
# NeoTreks iOS App

Repo for the NeoTreks Code Sample.

## Prerequisites for Development:##

* Mac Hardware
* Xcode Development IDE
* Cocoapods Installed (https://guides.cocoapods.org/using/getting-started.html)
* SourceTree app (https://www.sourcetreeapp.com/)

## Development Sandbox Setup

1. Download source code using SoureTree
2. Create local Git dev branch via SourceTree
3. Run Cocoapods from command line terminal: pod install (http://www.raywenderlich.com/97014)
4. Open Xcode workspace file (.xcworkspace) if it exists, if not open Xcode project file (.xcodeproj)
5. Follow GitFlow branching stategy (https://datasift.github.io/gitflow/IntroducingGitFlow.html)
6. Check in source code periodically to maintain code integrity and save changes
7. Access Git local and remote repos via SourceTree (creating branches as needed)

## Testing

* Test binaries for manual testing will be emailed out via BuddyBuild.
* Unit tests will be executed by BuddyBuild (https://dashboard.buddybuild.com/apps/)

### Target Environment Settings

The iOS app's target server-side environment is determined by a combination of both iOS configuration
settings in its info.plist and some environment Buddybuild settings. A Buddybuild shell script (found in the iOS projet: buddybuild_prebuild.sh) is also part of the process to insert values into the info.plist so that at runtime, the iOS app knows which server environment to point at. The app will default to the production server unless it sees the environment variable OVERRIDE_API_PATH in its info.plist. Then it will use that address and the name of that targeted server will be displayed in the bottom of the home view referenced by the environment variable TARGET_ENVIRONMENT_NAME. The target environment is displayed to the user (along with the app version and build number) at the bottom of the home view. 

The addresses of the different servers are defined in Buddybuild environment variables but the project also has a default production address for the cases where the developer is doing local builds.

Environment variables set in Buddybuild determine the target based upon the Bitbucket git repo's branch (following the Gitflow branching strategy):

Branch => Target Environment
=======================================
- master, release, hotfix => PRODUCTION
- develop => TEST
- feature => DEV

The following server environment variables are define in Buddybuild:

TARGET_ENVIRONMENT_NAME => PRODUCTION, TEST, DEV

USAC_API_PRODUCTION => https://api.unpluggedsystems.com

USAC_API_TEST => https://test-api.unpluggedsystems.com

USAC_API_DEV => https://dev-api.unpluggedsystems.com

This means that whenever an iOS app is built by Buddybuild, the app will point at that server environment designated by which branch the source code is in. 

If the developer so chooses, he or she can override the target by changing
the following environment variables in the project's info.plist file:

    <!-- Local testing
     <key>OVERRIDE_API_PATH</key>
     <string>https://api.unpluggedsystems.com</string>
     <key>TARGET_ENVIRONMENT_NAME</key>
     <string>PROD</string>
     -->

But the info.plist local setting will not take effect in a binary built by Buddybuild (even if the local setting settings are checked into the Bitbucket repo) Buddybuild will always override any values in the info.plist so that a Buddybuild built binary will take precedence.


## Deployment

1. Once local changes are completed, commit code to local dev branch
2. Push changes to remote GitLab's dev branch
3. BuddyBuild will start automated dev build because of webhook into GitLab repo
5. BuddyBuild will email out binary for testing
6. Once testing completed, merge dev branch into release branch
7. BuddyBuild will  increment build number and upload binary to iTunesConnect.
8. Inspect results of build via web (https://dashboard.buddybuild.com/apps/) or email.
9. Update app metadata in iTunes Connect (including images and text)
10. Submit to the Apple App Store

## Troubleshooting and Useful Tools


## Architecture and Implementation Notes




