//
//  Benefits.swift
//  Created by Brian Elliott on 8/11/17.
//

import Foundation

class Benefit {
    
    var title:String?
    var description:String?
    var offeredTo:[String]?
    var inAppLink:String?
    var externalLink:String?
    
    required init(title:String, description:String, offeredTo:[String], inAppLink:String, externalLink:String) {
        
        self.title = title
        self.description = description
        self.offeredTo = offeredTo
        self.inAppLink = inAppLink
        self.externalLink = externalLink
    }
    
}
