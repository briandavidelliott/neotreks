//
//  Benefits.swift
//  Created by Brian Elliott on 8/11/17.
//

import Foundation

class Partner {
    
    var partner:String?
    var imageName:String?
    var details:String?
    var offeredTo:[String]?
    
    required init(partner:String, imageName:String, details:String, offeredTo:[String]) {
        
        self.partner = partner
        self.imageName = imageName
        self.details = details
        self.offeredTo = offeredTo
    }
    
}
