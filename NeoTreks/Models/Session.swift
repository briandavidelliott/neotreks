//
//  Sesson.swift
//  Created by Brian Elliott on 9/28/17. 
//

import Foundation

import UIKit
import SwiftyJSON

class Session: NSObject, NSCoding {
    
    var access_token:        String?
    var refresh_token:       String?
    var token_expires:       Date?
    var scope:               DataAgent.UserRole?
    var licensesLastUpdated: Date?
    
    //
    // NSCoder unserializer
    //
    required init?(coder aDecoder: NSCoder) {
        self.access_token  = aDecoder.decodeObject(forKey: "access_token")  as? String
        self.refresh_token = aDecoder.decodeObject(forKey: "refresh_token") as? String
        self.token_expires = aDecoder.decodeObject(forKey: "token_expires") as? Date
        if let scope = aDecoder.decodeObject(forKey: "scope") as? String {
            switch scope {
            case "rd":       self.scope = DataAgent.UserRole.RaceDirector
            case "official": self.scope = DataAgent.UserRole.Official
            case "coach":    self.scope = DataAgent.UserRole.Coach
            case "comp":     fallthrough
            default:         self.scope = DataAgent.UserRole.Competitor
            }
        } else {
            self.scope = DataAgent.UserRole.Competitor
        }
        self.licensesLastUpdated = aDecoder.decodeObject(forKey: "licenses_last_updated") as? Date
        
    }
    
    init(_ accessToken:String, refreshToken:String, expires:Date, scope:DataAgent.UserRole, lastUpdated:Date?) {
        super.init()
        self.access_token = accessToken
        self.refresh_token = refreshToken
        self.token_expires = expires
        self.scope = scope
        self.licensesLastUpdated = lastUpdated
    }
    
    //
    // NSCoder serializer
    //
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.access_token,    forKey: "access_token")
        aCoder.encode(self.refresh_token,   forKey: "refresh_token")
        aCoder.encode(self.scope?.rawValue, forKey: "scope")
        aCoder.encode(self.token_expires,   forKey: "token_expires")
        aCoder.encode(self.token_expires,   forKey: "licenses_last_updated")
    }

}
