//
//  Waiver.swift
 //
// 
//
//  Created by Brian Elliott on 1/29/18.
 //
// 
//

import Foundation

class Waiver {
    
    var title:String?
    var content:String?
    var consent:String?
    
    required init(title:String?, content:String, consent:String) {
        
        self.title = title
        self.content = content
        self.consent = consent
    }
    
}
