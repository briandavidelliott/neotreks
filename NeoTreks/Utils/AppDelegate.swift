//
//  AppDelegate.swift
//  NeoTreks
//
//  Created by Brian Elliott on 2/24/18.
//  Copyright © 2018 Brian Elliott. All rights reserved.
//

import PushKit
import IQKeyboardManagerSwift
import Siren
import UserNotifications
import Timberjack


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    
    //
    // MARK: Properties
    //
    
    enum RefreshReturnValues {
        case Success
        case SuccessButNoDataFound
        case InvalidRefreshToken
        case GeneralApplicationError
        case NetworkError
        case InvalidUserNameOrPassword
        case APIDown
        case DeviceOffline
        case GeneralError
    }
    
    var window:             UIWindow?
    var storyboard:         UIStoryboard?
    var syncComplete:       Bool = false
    var navController:      UINavigationController?
    
    //
    // MARK: Delegate lifecycle
    //
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        BuddyBuildSDK.setup()
        
        let siren = Siren.shared
        siren.alertType = .option
        siren.showAlertAfterCurrentVersionHasBeenReleasedForDays = 1
        siren.checkVersion(checkType: .immediately)
        
        IQKeyboardManager.sharedManager().enable = true
        
        application.registerUserNotificationSettings(UIUserNotificationSettings(
            types: [.alert, .badge, .sound],
            categories: nil
        ))
        
        DataAgent.unserializeFromDisk()
        
        storyboard = UIStoryboard(name: "Main", bundle: nil)
        navController = self.window?.rootViewController as? UINavigationController
        // next line is needed to make sure window is not nil in alertview: let rv = UIApplication.shared.keyWindow! as UIWindow
        self.window?.makeKeyAndVisible()
        
        var viewName: String = "Login"
        
        if let nav = navController {
            
            let style = SCLAlertView.SCLAppearance(showCloseButton: false)
            let alert   = SCLAlertView(appearance: style)
            let wndWait = alert.showWait("Wait", subTitle: "Initializing ...", colorStyle: 0x041E42)
            
            AppUtils.checkAccess(nav, displayErrors:false, forceLoginOnError:true, displayLoginError:false) {
                (returnCode: AppUtils.CheckAccessReturnValues ) in
                // we have access, refresh data
                if returnCode == AppUtils.CheckAccessReturnValues.Success {
                    
                    wndWait.setSubTitle("Logging in ...")
                    
                    DataAgent.updateMemberData() { (success: Bool) in
                        
                        wndWait.close()
                        
                        if success {
                            viewName = self.getNextView()
                        }
                            // errors, then membership and license data not current but display because they have access but
                            // call to get current data did not work
                        else if DataAgent.hasTokens() {
                            viewName = self.getNextView()
                        }
                        else {
                            viewName = "Login"
                        }
                        
                        self.show(viewWithName: viewName)
                    }
                }
                else if returnCode == AppUtils.CheckAccessReturnValues.DeviceOffline {
                    wndWait.close()
                    viewName = self.getNextView()
                    _ = self.show(viewWithName: viewName)
                }
                else {
                    wndWait.close()
                    viewName = "Login"
                    self.show(viewWithName: viewName)
                }
            }
        }
        
        // Network monitoring
        if NetworkingConstants.networkingDebugFlag {
            Timberjack.logStyle = .verbose //Either .Verbose, or .Light
            Timberjack.register() //Register Timberjack to log all requests
        }
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
        // this call created a problem for the event search maps for the first time when it asked for permission,
        // then the applicationWillResignActive method (this one) was called when the dialog for permissions was displayed.
        // then this call would navigate to the menu view and the user would have to pick the event search button again.
        // I am not seeing a need for this call but we may need to revisit this later if it becomes and issue. So I will
        // leave it commented out for now.
        
        //        if DataAgent.hasTokens() {
        //            let _ = self.show(viewWithName: "Menu")
        //        }
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
        // Download (and cache) the login background image
        if DataAgent.getReachabilityStatus() != .notReachable {
            let url = URL(string: "https://s3.amazonaws.com/static/images/LoginBg-1200x1200.png")
            let img = UIImageView()
            img.kf.setImage(with: url)
        }
        
    }
    
    func refreshMemberLicenseData(_ nav:UINavigationController, completion: @escaping (_ completion: RefreshReturnValues) -> Void) {
        
        AppUtils.checkAccess(nav, displayErrors:false, forceLoginOnError:true, displayLoginError:true ) {
            (returnCode: AppUtils.CheckAccessReturnValues ) in
            
            // we have access, refresh data
            if returnCode == AppUtils.CheckAccessReturnValues.Success {
                DataAgent.updateMemberData() { (success: Bool) in
                    self.syncCompleted()
                    completion(RefreshReturnValues.Success)
                }
            }
            else if returnCode == AppUtils.CheckAccessReturnValues.DeviceOffline {
                completion(RefreshReturnValues.DeviceOffline)
            }
            else if returnCode == AppUtils.CheckAccessReturnValues.APIDown {
                completion(RefreshReturnValues.APIDown)
            }
            else if returnCode == AppUtils.CheckAccessReturnValues.NetworkError {
                completion(RefreshReturnValues.NetworkError)
            }
            else if returnCode == AppUtils.CheckAccessReturnValues.GeneralApplicationError {
                completion(RefreshReturnValues.GeneralApplicationError)
            }
            
        }
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        // for touch id case where device is offline and has no tokens
        if UserDefaults.standard.bool(forKey: "hasTouchIdLoginSet") {
            if DataAgent.getReachabilityStatus() == .notReachable {
                DataAgent.unserializeFromDisk()
                syncCompleted()
            }
        }
    }
    
    //
    // MARK: Updating utils
    //
    
    private func syncCompleted() {
        syncComplete = true
        NotificationCenter.default.post(name: NSNotification.Name("SyncComplete"),
                                        object: syncComplete)
    }
    
    //
    // MARK: Routing utils
    //
    
    func getNextView() -> String {
        
        var viewName = ""
        
        if GeneralConstants.FeatureToggles.licensesOnlyFlag {
            
            if AppUtils.isLookupUser() {
                viewName = "Menu"
            }
            else {
                if AppUtils.hasLicenses() {
                    viewName = "License"
                }
                else {
                    viewName = "LicenseError"
                }
            }
        }
        else {
            viewName = "Menu"
        }
        
        return viewName
        
    }
    
    func show(viewWithName viewName: String) {
        
        syncComplete = true
        
        // if going to license error (no licenses) or going to login, skip waiver check
        if viewName == "Menu" || viewName == "License" {
            
            let sharedWavierManager = WaiverManager.shared
            // if false, process without displaying waivers, else let waiver manager handle it
            if let nav = navController, let board = storyboard {
                sharedWavierManager.checkForWaivers(board, nav:nav, segueViewController:nil, viewNameForNonSegue:viewName, useSegue: false)
            }
        }
    }
    
}

