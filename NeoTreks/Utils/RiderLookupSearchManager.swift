//
//  RiderLookupSearchManager.swift
 //
// 
//
//  Created by Brian Elliott on 10/9/17.
//   //
// 
//

import Foundation
import SwiftyJSON

class RiderLookupSearchManager {
    
    //
    // MARK: Properties
    //
    
    private var parentViewController:UIViewController!
    private var alertViewResponder:SCLAlertViewResponder?
    
    private var spinnerIsDisplayed:Bool = false
    
    init(_ parent:UIViewController) {        
        self.parentViewController = parent
    }
    
    //
    // MARK: UI methods
    //
    
    func clearSpinner() {
        
        if spinnerIsDisplayed {
            
            if let alert = alertViewResponder {
                alert.close()
            }
        }
    }
    
    //
    // MARK: Search methods
    //
    
    func isNameSearch(_ terms:String) -> Bool {
        
        let term = terms.trimmingCharacters(in: NSCharacterSet.whitespaces)
        if AppUtils.isNumeric(term) {
            return false
        }
        else {
            return true
        }
    }
    
    func getLicenses(_ compid:String, completion: @escaping (_ results: [License]) -> Void) {
        
        var returnLicenseArray:[License] = []
        
        guard (compid.count > 0) else {
            SCLAlertView().showError("Error", subTitle: "System error - compid is blank")
            completion(returnLicenseArray)
            return
        }
        
        DataAgent.getLicenseData (compid, mock:false)
        { (data: JSON) in
            returnLicenseArray = self.processLicensesResults(data)
            completion(returnLicenseArray)
        }
    }
    
    func searchRiders(_ terms:String, pageNumber:Int, completion: @escaping (_ results: [Member], _ totalFound:Int) -> Void) {
        
        var returnLookupArray:[Member] = []
        
        guard (terms.count > 0) else {
            SCLAlertView().showError("Error", subTitle: "Search field is blank!")
            clearSpinner()
            completion(returnLookupArray, 0)
            return
        }
        
        if !spinnerIsDisplayed {
            
            // Show login progress popup
            let style = SCLAlertView.SCLAppearance(showCloseButton: false)
            let alert = SCLAlertView(appearance: style)
            alertViewResponder = alert.showWait("Searching ...", subTitle: "Looking for riders", colorStyle: 0x041E42)
            
            spinnerIsDisplayed = true
        }
        
        // determine search type
        if isNameSearch(terms) {
            
            DataAgent.getRidersByName (terms, pageNumber:pageNumber, mock:false)
            { (data: JSON) in
                
                let (riderData, totalFound) = self.parseRiderJsonData(data)
                returnLookupArray = self.processSearchRiderResults(riderData)
                self.clearSpinner()
                self.spinnerIsDisplayed = false
                
                completion(returnLookupArray, totalFound)
            }
        }
            // license id search
        else {
            
            let term = terms.trimmingCharacters(in: NSCharacterSet.whitespaces)
            DataAgent.getRidersByLicenseId (term, pageNumber:pageNumber, mock:false)
            { (data: JSON) in
                
                let (riderData, totalFound) = self.parseRiderJsonData(data)
                returnLookupArray = self.processSearchRiderResults(riderData)
                self.clearSpinner()
                self.spinnerIsDisplayed = false
                
                completion(returnLookupArray, totalFound)
            }
        }
    }
    
    func parseRiderJsonData(_ data: JSON) -> (data: JSON, totalFound:Int) {
        
        if let metadata = data["metadata"].dictionary {
            if let count = metadata["total_riders_found"] {
                return (data["data"], count.intValue)
            }
            else {
                return (data["data"], 0)
            }
        }
        else {
            print("no value for parseRiderJsondata")
            return (data["data"], 0)
        }
    }

    func processSearchRiderResults(_ data: JSON) -> [Member] {
        
        var returnLookupArray:[Member] = []
        
        let count: Int? = data.array?.count
        
        // No results returned
        if data["Error"] == 500 {
            SCLAlertView().showError("Search Error", subTitle: "Error: system is unavailable!")
        }
            // No riders found
        else if data.isEmpty || count == 0 {
            return returnLookupArray
        }
        else {

            data.forEach() { _, rider in
                let member = Member(initWithJSON: rider)
                returnLookupArray.append(member)
            }
        }
        
        return returnLookupArray
    }
    
    func processLicensesResults(_ data: JSON) -> [License] {
        
        var returnLicenseArray:[License] = []
        
        let count: Int? = data.array?.count
        
        // No results returned
        if data["Error"] == 500 {
            SCLAlertView().showError("Search Error", subTitle: "Error: system is unavailable!")
        }
        // No licenses returned
        else if data.isEmpty || count == 0 {
            return returnLicenseArray
        }
        else {
            
            data.forEach() { _, rider in
                
                let license = License(initWithJSON: rider)
                returnLicenseArray.append(license)
                
            }
        }
        
        return returnLicenseArray
    }
}
