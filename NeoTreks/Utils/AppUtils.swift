    //
//  AppUtils.swift
 //
// 
//
//  Created by Brian Elliott on 7/19/17.
//   //
// 
//

import Foundation
import SwiftyJSON

protocol NavBarBackProtocol {
    func backPicked()
}

class AppUtils {
    
    //
    // MARK: Properties
    //
    
    enum CheckAccessReturnValues {
        case Success
        case InvalidRefreshToken
        case GeneralApplicationError
        case NetworkError
        case APIDown
        case DeviceOffline
    }
    
    static var targetEnvironmentName:String?
    
    //
    // MARK: General App Utils
    //
    
    class func appVersion() -> String {
        
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        
        if targetEnvironmentName == nil {
            
            if let target = Bundle.main.infoDictionary?["TARGET_ENVIRONMENT_NAME"] as? String {
                
                targetEnvironmentName = target
            }
            else {
                
                targetEnvironmentName = "PRODUCTION"
            }
        }
        
        return "Version: \(version) (\(build)) - \(targetEnvironmentName ?? "PRODUCTION")"
    }
    
    //
    // MARK: UI utils
    //
    
    class func checkForWaiverErrors(_ data:JSON) -> Bool {
        
        if data["Error"] == 500 {
            return true
        }
        else {
            return false
        }
    }
    
    class func getLookupSection(_ license:License) -> String {
        
        if let header = license.section_header {
            if header.uppercased() == "TECHNICAL" {
                
                // Check for vehicle license
                if license.license_type == "V" {
                    return header.uppercased() + " - " + "DOM DRIVE"
                }
                else if let subtitle = license.section_subtitle {
                    return header.uppercased() + " - " + subtitle.uppercased()
                }
                else {
                    return header.uppercased()
                }
            }
            else {
                return header.uppercased()
            }
        }
        else {
            return ""
        }
    }
    
    class func getLicenseStatus(_ license:License) -> String {
        
        if license.license_status == "P" {
            return license.license_status
        }
        else if license.license_status == "S" {
            return license.license_status
        }
        else {
            return " "
        }
        
    }
    
    class func navigateToViewController (_ storyboard:UIStoryboard, navController:UINavigationController, viewName:String) {

        let vc = storyboard.instantiateViewController(withIdentifier: viewName)

        if viewName == "Waivers" {
            if let waiverVC = vc as? WaiverViewController {
                let sharedWavierManager = WaiverManager.shared
                waiverVC.waivers = sharedWavierManager.waivers
                waiverVC.waiverId = sharedWavierManager.waiverId

                navController.pushViewController(waiverVC, animated: true)
            }
        }
        else {
            navController.pushViewController(vc, animated: true)
        }

    }
    
    class func navigateToView(_ controller:UIViewController, includeErrorPath:Bool) {
        
        // these segue are only guaranteed to work with the login view controller
        if controller is LoginController {
            if GeneralConstants.FeatureToggles.licensesOnlyFlag {
                
                if AppUtils.isLookupUser() {
                    // Trigger login segue (to main/home menu)
                    controller.performSegue(withIdentifier: "LoginSegue", sender: nil)
                }
                else {
                    // still in license features toggle mode for non-officals
                    if hasLicenses() {
                        // Trigger login segue (to license menu)
                        controller.performSegue(withIdentifier: "LicenseSegue", sender: nil)
                    }
                    else if includeErrorPath {
                        controller.performSegue(withIdentifier: "LicenseErrorSegue", sender: nil)
                    }
                }
            }
            else {
                // Trigger login segue (to main/home menu)
                controller.performSegue(withIdentifier: "LoginSegue", sender: nil)
            }
        }
        
    }
    
    class func createAttributedStringFromMarkdown(_ text:String) -> NSAttributedString {
        
        let documentAttributes = [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType]
        return NSAttributedString(string: text, attributes: documentAttributes)
        
    }
    
    
    class func enumerateFonts()
    {
        for fontFamily in UIFont.familyNames
        {
            print("Font family name = \(fontFamily as String)")
            for fontName in UIFont.fontNames(forFamilyName: fontFamily as String)
            {
                print("- Font name = \(fontName)")
            }
        }
    }
    
    class func buildRaceTypeArray() -> [String] {
        
        var raceTypeArray:[String] = []
        
        for value in GeneralConstants.EventSearch.RaceType.allValues {
            raceTypeArray.append(value.rawValue)
        }
        
        return raceTypeArray
    }
    
    class func getBirthdate(_ dob:Date) -> String {
        
        // flag to indicate a bonus date from the API - a zero date
        if AppUtils.isDateInTheFuture(dob) {
            return "N/A"
        }
        else {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy"
            return dateFormatter.string(from: dob)
        }
    }
    
    class func getMembership() -> GeneralConstants.Benefits.MembershipTypes {
        
        return GeneralConstants.Benefits.MembershipTypes.Ride
    }
    
    class func isBenefitApplicable(_ offeredTo:[String], currentMembershipLevel:GeneralConstants.Benefits.MembershipTypes) -> Bool {
        
        return offeredTo.contains(currentMembershipLevel.rawValue)
    }
    
    class func setAccountInfo(_ member:Member) -> [GeneralConstants.AccountViewNames:String] {
        
        var valuesDictionary:[GeneralConstants.AccountViewNames:String] = [:]
        
        valuesDictionary[GeneralConstants.AccountViewNames.Name] = member.first_name + " " + member.last_name
        valuesDictionary[GeneralConstants.AccountViewNames.MemberNumber] = String(member.comp_id)
        valuesDictionary[GeneralConstants.AccountViewNames.DateOfBirth] = member.birth_date
        
        if let racingAge = AppUtils.calcRacingAge(member) {
            valuesDictionary[GeneralConstants.AccountViewNames.RacingAge] = String(racingAge)
        }
        else {
            valuesDictionary[GeneralConstants.AccountViewNames.RacingAge] = ""
        }
        
        valuesDictionary[GeneralConstants.AccountViewNames.Gender] = member.gender.rawValue
        valuesDictionary[GeneralConstants.AccountViewNames.PrimaryEmail] = member.email
        valuesDictionary[GeneralConstants.AccountViewNames.Username] = member.username
        
        var addressDictionary:[String:String] = [:]
        
        addressDictionary["address"] = member.address
        addressDictionary["city"] = member.city
        addressDictionary["state"] = member.state
        addressDictionary["zip"] = member.zip
        
        let address = addressDictionary["address"] ?? ""
        let city = addressDictionary["city"] ?? ""
        let state = addressDictionary["state"] ?? ""
        let zip = addressDictionary["zip"] ?? ""
        
        // to get by compiler too complex error
        // see: https://stackoverflow.com/questions/29707622/bizarre-swift-compiler-error-expression-too-complex-on-a-string-concatenation
        
        let addressString1 = address + "\n" + city
        let addressString2 = ", " + state + " " + zip
        // let testThirdLine = "\nThis is my third line\nAnd forth!"
        
        // future
        valuesDictionary[GeneralConstants.AccountViewNames.UCICode] = ""
        valuesDictionary[GeneralConstants.AccountViewNames.UCIID] = ""
        valuesDictionary[GeneralConstants.AccountViewNames.Citizen] = ""
        valuesDictionary[GeneralConstants.AccountViewNames.MemberSince] = ""
        valuesDictionary[GeneralConstants.AccountViewNames.EmergencyContact] = ""
        valuesDictionary[GeneralConstants.AccountViewNames.PreferredPhone] = ""
        
        valuesDictionary[GeneralConstants.AccountViewNames.Address] = addressString1 + addressString2
        
        return valuesDictionary
    }
    
    class func addNavBar(_ label:String, viewController:UIViewController, navigationController:UINavigationController?, delegate:NavBarBackProtocol) {
        
        navigationController?.isNavigationBarHidden = false
        
        let img = UIImage(named: "SmallLogo.png")
        viewController.navigationItem.titleView = UIImageView(image: img)
        
        let backString:String = "\u{25C0} " + label
        
        let backButton = BackBarButtonItem.init(
            title: backString, style: UIBarButtonItemStyle.plain,
            delegate:delegate)

        viewController.navigationItem.leftBarButtonItem = backButton
    }
    
    class func setLocalAssociationStatusIndicator(_ label:UILabel, localAssociation:String, status:String) {
    
        if status == "Y" {
            label.backgroundColor = AppColors.RiderLookupColors.laStatusActive
            label.text = "A"
            label.isHidden = false
        }
        else if status == "N" {
            label.backgroundColor = AppColors.RiderLookupColors.laStatusInactive
            label.text = "I"
            label.isHidden = false
        }
        else {
            label.isHidden = true
            label.text = " "
        }
    
    }
    
    class func pinBackground(_ view: UIView, to stackView: UIStackView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        stackView.insertSubview(view, at: 0)
        view.pin(to: stackView)
    }
    
    class func convertColorToHtmlBackground(_ hex:String, alpha:Float) -> String {

        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
             return "rgba(127, 127, 127, \(alpha))"
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        let r, g, b: UInt32
        (r, g, b) = (rgbValue >> 16, rgbValue >> 8 & 0xFF, rgbValue & 0xFF)
        
        return "rgba(\(r), \(g), \(b), \(alpha))"

    }
    
    class func hexStringToUIColor (_ hex:String, alpha:Float) -> UIColor {

        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(alpha)
        )
    }
    
    //
    // MARK: Date utils
    //
    
    class func getCurrentYear () -> String {
        
        let date = Date()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        
        return String(year)
        
    }
    
    class func getYearlyLicenseColor(_ alpha:Float) -> UIColor {
        
        var returnColor = AppUtils.hexStringToUIColor(AppColors.LicenseColors.defaultBannerBackgroundColor, alpha: alpha)
        
        if let licenses = DataAgent.getMemberLicenses() {

            if licenses.count <= 0 {
                return returnColor
            }
            else {

                for license in licenses {
                    if let bgColor = license.bg_color {
                        returnColor = AppUtils.hexStringToUIColor(bgColor, alpha: alpha)
                        break
                    }
                }

                return returnColor
            }
        }
        else {
            return returnColor
        }
    }
    
    static func shortDateTime (_ date:Date) -> String {
        
        return DateFormatter.localizedString(
            from: date,
            dateStyle: .short,
            timeStyle: .short)
    }
    
    static func formatDate(_ input:Date) -> String? {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"

        return formatter.string(from: input)
        
    }
    
    static func getWaiverExpiration() -> Date {
        // interval is in seconds so multiple by 60 to set number of minutes
        return Date().addingTimeInterval(GeneralConstants.Waivers.waiverWaitTimeMinutes * 60.0)
    }
    
    //
    // MARK: Format Data Utils
    //
    
    class func isNumeric(_ string: String) -> Bool
    {
        let number = Int(string)
        return number != nil
    }
    
    class func isDateInTheFuture(_ input:Date) -> Bool {
        
        if input > Date() {
            return true
        }
        else {
            return false
        }
    }
    
    class func isDateExpired(_ expirationDate:Date) -> Bool {
        
        let now = Date()
        let order = Calendar.current.compare(now as Date, to: expirationDate as Date, toGranularity: .day)
        
        switch order {
        case .orderedAscending:
            // print("\(expirationDate) is after \(now)")
            return false
        case .orderedDescending:
            // print("\(expirationDate) is before \(now)")
            return true
        default:
            // print("\(expirationDate) is the same as \(now)")
            return false
        }
    }
    
    class func formatUciId(_ uciLicenseId:String) -> String {
        
        if uciLicenseId.count > 0  && !uciLicenseId.contains(" ") {
            
            let pattern = "([0-9]{3})([0-9]{3})([0-9]{3})([0-9]{2})"
            
            let regex = try! NSRegularExpression(pattern: pattern)
            let result = regex.matches(in:uciLicenseId, range:NSMakeRange(0, uciLicenseId.utf16.count))
            
            let nsrangeOne = result[0].rangeAt(1)
            let nsrangeTwo = result[0].rangeAt(2)
            let nsrangeThree = result[0].rangeAt(3)
            let nsrangeFour = result[0].rangeAt(4)
            
            if let rangeOne = uciLicenseId.range(from: nsrangeOne),
                let rangeTwo = uciLicenseId.range(from: nsrangeTwo),
                let rangeThree = uciLicenseId.range(from: nsrangeThree),
                let rangeFour = uciLicenseId.range(from: nsrangeFour) {
                
                let first = uciLicenseId.substring(with: rangeOne) + " "
                let second = uciLicenseId.substring(with: rangeTwo) + " "
                let third = uciLicenseId.substring(with: rangeThree)  + " "
                let fourth = uciLicenseId.substring(with: rangeFour)
                
                return first + second + third + fourth
            }
            else {
                return uciLicenseId
            }
        }
        else {
            return uciLicenseId
        }
    }
    
    class func prepareHtmlWaiver(_ htmlString:String) -> String {
        
        // trim CRLF from end of content
        var returnString = htmlString.replacingOccurrences(of: "\\r\\n+$", with: "", options: .regularExpression)
        returnString = returnString.replacingOccurrences(of: "\r\n", with: "<br/>")
        returnString = returnString.replacingOccurrences(of: "\u{20AC}", with: "&nbsp;")
        
        return returnString
    }
    
    class func formatPhone(_ phone:String) -> String {
        
        if phone.count > 0 && !phone.contains(" ") && AppUtils.isNumeric(phone) {
            
            let pattern = "(.*)([0-9]{3})([0-9]{3})([0-9]{4})"
            
            let regex = try! NSRegularExpression(pattern: pattern)
            let result = regex.matches(in:phone, range:NSMakeRange(0, phone.utf16.count))
            
            if result.count > 0 {
                
                let nsrangeOne = result[0].rangeAt(1)
                let nsrangeTwo = result[0].rangeAt(2)
                let nsrangeThree = result[0].rangeAt(3)
                let nsrangeFour = result[0].rangeAt(4)
            
                if let rangeOne = phone.range(from: nsrangeOne),
                    let rangeTwo = phone.range(from: nsrangeTwo),
                    let rangeThree = phone.range(from: nsrangeThree),
                    let rangeFour = phone.range(from: nsrangeFour) {
                
                    var first = phone.substring(with: rangeOne) + "-"
                    // if no country code
                    if first.count == 1 {
                        first = ""
                    }
                    let second = phone.substring(with: rangeTwo) + "-"
                    let third = phone.substring(with: rangeThree)  + "-"
                    let fourth = phone.substring(with: rangeFour)
                
                    return first + second + third + fourth
                }
                else {
                    return phone
                }
            }
            else {
                return phone
            }
        }
        else {
            return phone
        }
    }
    
    //
    // MARK: Calculation Utils
    //
    
    // The racing age is the age of the rider at the end of the year
    class func calcRacingAge(_ member:Member) -> Int? {
    
        let dob = member.dob
        return dob.racingAge
    }
    
    class func calcRacingAgeInYear(_ member:Member) -> String? {
        
        let dob = member.dob
        let date = Date()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        if let age = dob.racingAge {
            return String(describing: age) + " in " + String(year)
        }
        else {
            return " "
        }
    }
    
    //
    // MARK: JSON related utils
    //
    
    class func readJson(_ fileName:String) -> JSON? {
        
        do {
            if let file = Bundle.main.url(forResource: fileName, withExtension: "json") {
                let data = try Data(contentsOf: file)
                
                let json = JSON(data: data)
                return json
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        
        return nil
    }
    
    //
    // MARK: User utils
    //
    
    // Has access to do rider lookup
    class func isLookupUser() -> Bool {
        
        // check for admin status first
        if let member = DataAgent.getMemberProfile() {
            if member.has_lookup == "Y" {
                return true
            }
        }
        
        // now check licenses
        if let licenses = DataAgent.getMemberLicenses() {
            
            if licenses.count <= 0 {
                return false
            }
            else {
                // loop thru all licenses looking for one officals license
                var officalsLicenseFound = false
                var suspendedOrPendingFound = false
                licenses.forEach() { (license: License) in
                    
                    if !suspendedOrPendingFound {
                        
                        // allow access for officials, directors and promoters access
                        if license.license_type == "O"  || license.license_type == "D" || license.license_type == "P" {
                        
                            // check if license is expired
                            if !isDateExpired(license.expiration_date) {
                                // Check if license is suspended or pending
                                if license.license_status.uppercased() == "S" || license.license_status.uppercased() == "P" {
                                    suspendedOrPendingFound = true
                                    officalsLicenseFound = false
                                }
                                else {
                                    officalsLicenseFound = true
                                }
                            }
                        }
                    }
                }
                
                if officalsLicenseFound {
                    return true
                }
                else {
                    return false
                }
            }
        }
        else {
            return false
        }
        
    }
    
    class func isUserAnOfficial() -> Bool {
        
        // check for admin status first
        if let member = DataAgent.getMemberProfile() {
            if member.has_lookup == "Y" {
                return true
            }
        }
        
        // now check licenses
        if let licenses = DataAgent.getMemberLicenses() {
            
            if licenses.count <= 0 {
                return false
            }
            else {
                // loop thru all licenses looking for one officals license
                var officalsLicenseFound = false
                licenses.forEach() { (license: License) in
                    // allow access for officials, directors and promoters access
                    if license.license_type == "O" {
                        officalsLicenseFound = true
                    }
                }
                
                if officalsLicenseFound {
                    return true
                }
                else {
                    return false
                }
            }
        }
        else {
            return false
        }
        
    }
    
    class func hasLicenses() -> Bool {
        
        if let licenses = DataAgent.getMemberLicenses() {
            
            if licenses.count == 0 {
                return false
            }
            else {
                return true
            }
        }
        else {
            return false
        }
    }
    
    class func getWaiverId(_ data:JSON) -> Int? {
        
        var waiverId:Int?
        
        for item in data.arrayValue {
            waiverId =  item["id"].intValue
            break
        }
        
        return waiverId
    }
    
    class func hasJuniorWaiver(_ data:JSON) -> (isJuniorWavier:Bool, parentName:String, parentEmail:String) {
        
        // these value will be a empty string if JSON is null so that is why they aren't optionals
        var guardianName:String = ""
        var guardianEmail:String = ""
        
        for item in data.arrayValue {
            
            guardianName =  item["guardian_name"].stringValue
            guardianEmail =  item["guardian_email"].stringValue

        }
        
        if !guardianEmail.isEmpty {
            if !guardianName.isEmpty {
                return (true, guardianName, guardianEmail)
            }
            else {
                return (true, " ", guardianEmail)
            }
        }
        else {
            return (false, " ", " ")
        }
        
    }
    
    class func needsWavierSigned() -> Bool {
        
        // check licenses
        if let licenses = DataAgent.getMemberLicenses() {
            
            if licenses.count <= 0 {
                return false
            }
            else {
                // loop thru all licenses looking for one waiver pending
                var waiverPendingFound = false
                licenses.forEach() { (license: License) in
                    
                    if !waiverPendingFound {
                        
                        // allow access for officials, directors and promoters access
                        if let reason = license.pend_reason {
                            
                            // check if license is expired
                            if !isDateExpired(license.expiration_date) {
                                // Check if pending reason is waiver
                                if reason.lowercased().contains("waiver") {
                                    waiverPendingFound = true
                                }
                            }
                        }
                    }
                }
                
                if waiverPendingFound {
                    return true
                }
                else {
                    return false
                }
            }
        }
        else {
            return false
        }
    }
    
    //
    // MARK: Network utils
    //
    
    class func displayCheckAccessError(_ nav:UINavigationController, inputValue:AppUtils.CheckAccessReturnValues) {
    
        if inputValue == AppUtils.CheckAccessReturnValues.Success {
            return
        }
        else if inputValue == AppUtils.CheckAccessReturnValues.DeviceOffline {
            return
        }
        else if inputValue == CheckAccessReturnValues.NetworkError {
            SCLAlertView().showError("Network Error", subTitle: "Network error. Please try again later with a better Internet connection.")
        }
        else if inputValue == CheckAccessReturnValues.APIDown {
            SCLAlertView().showError("Server Error", subTitle: "Server error. Please try again later.")
        }
        else if inputValue == CheckAccessReturnValues.GeneralApplicationError {
            SCLAlertView().showError("Server Error", subTitle: "System error. Please try again later.")
        }
        // force login
        else if inputValue == CheckAccessReturnValues.InvalidRefreshToken {
            
            SCLAlertView().showError("Login Error", subTitle: "Session expired. Please login.")
        }
    }
    
    class func goToLogin(_ nav:UINavigationController) {
        
        // clear out auth, member and license data
        DataAgent.reset()
        
        // go to login
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Login")
        nav.pushViewController(vc, animated: true)
    }
    
    class func checkAccess(_ nav:UINavigationController, displayErrors:Bool, forceLoginOnError:Bool, displayLoginError:Bool, accessReturnCode: @escaping (AppUtils.CheckAccessReturnValues) -> Void) {
        
        // has refresh token
        if DataAgent.hasTokens() {
            
            if DataAgent.getReachabilityStatus() == .notReachable {
                accessReturnCode(AppUtils.CheckAccessReturnValues.DeviceOffline)
            } else {
                
                // is access token expired?
                if DataAgent.isAuthenticated() {
                    // access token is not expired, access is good
                    accessReturnCode(AppUtils.CheckAccessReturnValues.Success)
                } else {
                    // network call to get a new access token from the refresh token
                    DataAgent.refreshToken() { (returnCode: DataAgent.APIReturnValues) in
 
                        if returnCode == DataAgent.APIReturnValues.Success {
                            // access token has been refreshed successfully, access is good
                            accessReturnCode(AppUtils.CheckAccessReturnValues.Success)
                        } else {
                            // refresh fails, force login
                            if returnCode == DataAgent.APIReturnValues.SuccessButNoDataFound ||
                                returnCode == DataAgent.APIReturnValues.InvalidRefreshToken {
                                        
                                if displayErrors {
                                    self.displayCheckAccessError(nav, inputValue:CheckAccessReturnValues.InvalidRefreshToken)
                                }
                                        
                                if forceLoginOnError {
                                    // clear out auth, member and license data
                                    self.goToLogin(nav)
                                }
                                        
                                accessReturnCode(AppUtils.CheckAccessReturnValues.InvalidRefreshToken)
                            }
                            else if returnCode == DataAgent.APIReturnValues.NetworkError {
                                    if displayErrors {
                                        self.displayCheckAccessError(nav, inputValue:CheckAccessReturnValues.NetworkError)
                                    }
                                    accessReturnCode(AppUtils.CheckAccessReturnValues.NetworkError)
                            }
                            else if returnCode == DataAgent.APIReturnValues.APIDown {
                                    if displayErrors {
                                        self.displayCheckAccessError(nav, inputValue:CheckAccessReturnValues.APIDown)
                                    }
                                    accessReturnCode(AppUtils.CheckAccessReturnValues.APIDown)
                            }
                            else {
                                    if displayErrors {
                                        self.displayCheckAccessError(nav, inputValue:CheckAccessReturnValues.GeneralApplicationError)
                                    }
                                    accessReturnCode(AppUtils.CheckAccessReturnValues.GeneralApplicationError)
                            }
                        }
                    }
                }
            }
        }
        else {
            // refresh token is invalid
            if displayLoginError {
                self.displayCheckAccessError(nav, inputValue:CheckAccessReturnValues.InvalidRefreshToken)
            }
            
            if forceLoginOnError {
                self.goToLogin(nav)
            }
            
            accessReturnCode(AppUtils.CheckAccessReturnValues.InvalidRefreshToken)
        }
    }
    
}
