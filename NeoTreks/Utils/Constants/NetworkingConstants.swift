//
//  Created by Brian Elliott on 7/17/17.
//   //
// 
//

import Foundation

struct NetworkingConstants {
    
    static let networkingDebugFlag = true
        
    static let productionUrl = "http://unpluggedsystems.com"
    static let updateAccountUrl = "/mypage/index.php?pagename=editaccount"
    
    static let licenseErrorViewOptionsUrl = "https://register.unpluggedsystems.com/#!/c/race"
    
    static let clientID = "APPAPPLE"
    static let clientSecret = "**************************"
    
    enum HTTPMethod: String {
        case options = "OPTIONS"
        case get     = "GET"
        case head    = "HEAD"
        case post    = "POST"
        case put     = "PUT"
        case patch   = "PATCH"
        case delete  = "DELETE"
        case trace   = "TRACE"
        case connect = "CONNECT"
    }
    
}
