//
//  Created by Brian Elliott on 7/17/17.
//   //
// 
//

import Foundation
import UIKit

struct GeneralConstants {
    
    static let generalDebugFlag = false
    
    struct FeatureToggles {
        
        static let licensesOnlyFlag = true
        static let officialsVersionFlag = true
        
    }

    struct iconButton {
        
        static let iconButtonImageHeightWidth:CGFloat = 25.0
        static let iconButtonPadding:CGFloat = 10.0

    }
    
    enum UserRole: String {
        case Competitor   = "comp"
        case RaceDirector = "rd"
        case Coach        = "coach"
        case Official     = "official"
    }
    
    enum AccountViewNames: String {
        case Name = "Name"
        case MemberNumber = "Member #"
        case DateOfBirth = "Date of Birth"
        case UCICode = "UCI Code"
        case UCIID = "UCI ID"
        case Citizen = "Citizen"
        case RacingAge = "Racing Age"
        case MemberSince = "Member Since"
        case Gender = "Gender"
        case Address = "Address"
        case PreferredPhone = "Preferred Phone"
        case PrimaryEmail = "Primary Email"
        case Username = "Username"
        case EmergencyContact = "Emergency Contact"
    }
    
    static let accountViewNamesDisplayOrder: [AccountViewNames:Int] = [
        AccountViewNames.Name : 1,
        AccountViewNames.MemberNumber : 2,
        AccountViewNames.DateOfBirth : 3,
        AccountViewNames.UCICode : 4,
        AccountViewNames.UCIID : 5,
        AccountViewNames.Citizen : 6,
        AccountViewNames.RacingAge : 7,
        AccountViewNames.MemberSince : 8,
        AccountViewNames.Gender : 9,
        AccountViewNames.Address : 10,
        AccountViewNames.PreferredPhone : 11,
        AccountViewNames.PrimaryEmail : 12,
        AccountViewNames.Username : 13,
        AccountViewNames.EmergencyContact : 14
    ]
    
    static let accountViewNamesDisplayOn: [AccountViewNames:Bool] = [
        AccountViewNames.Name : true,
        AccountViewNames.MemberNumber : true,
        AccountViewNames.DateOfBirth : true,
        AccountViewNames.UCICode : false,
        AccountViewNames.UCIID : false,
        AccountViewNames.Citizen : false,
        AccountViewNames.RacingAge : true,
        AccountViewNames.MemberSince : false,
        AccountViewNames.Gender : true,
        AccountViewNames.Address : true,
        AccountViewNames.PreferredPhone : false,
        AccountViewNames.PrimaryEmail : true,
        AccountViewNames.Username : true,
        AccountViewNames.EmergencyContact : false
    ]
    
    struct AccountViewDisplay {
        
        // static let listShiftLeft:CGFloat = -200.0
        
    }
    
    struct LicenseView {
        
        static let smallLogoScreenHeightThershold:CGFloat = 667.0
        static let smallLogoScreenHeightResize:CGFloat = 70.0
        static let licenseBackgroundColorAlpha:Float = 0.4
        
    }
    
    struct Benefits {
        static let benefitsJsonFile = "member_benefits"
        static let partnerJsonFile = "partner_benefits"
        static let membershipExplaination:String = "Below are the benefits you receive for being a %membership_type% Member."
        static let partnerExplaination:String = "Below are the benefits from our partners that we extend to you as a member of Team Sport."
        
        enum MembershipTypes: String {
            case Ride = "Ride"
            case RidePlus = "Ride Plus"
            case Race = "Race"
            case RacePodium = "Race Podium"
            case RacePodiumPlus = "Race Podium Plus"
        }
        
        static let partnerImageWidth:CGFloat = 300.0
        static let partnerImageHeight:CGFloat = 300.0
        static let partnerImageSpacing:CGFloat = 30.0
        
        static let collaspedCellIcon = "Minus"
        static let expandedCellIcon = "Plus"
        static let externalLinkCellIcon = "LinkArrow"
        static let internalLinkCellIcon = "InternalLink"
        
    }
    
    struct Waivers {
        static let collaspedCellIcon = "Minus"
        static let expandedCellIcon = "Plus"
        static let greenCheckBoxIcon = "GreenCheckBox"
        static let cornerRadius:CGFloat = 5.0
        static let sectionHeaderHeight:CGFloat = 65.0
        static let waiverWaitTimeMinutes = 1.0
        static let membershipContactEmailAddress = "mailto:membership@unpluggedsystems.com"
        static let membershipContactEmailDisplay = "membership@unpluggedsystems.com"
        static let membershipContactPhoneNumber = "tel:1-719-434-4200"
        static let membershipContactPhoneNumberDislay = "(719) 434-4200"
        static let adultWaiverSearchString = "ACKNOWLEDGMENT OF RISK, RELEASE OF LIABILITY, INDEMNIFICATION AGREEMENT AND COVENANT NOT TO SUE"
        static let adultWaiverTitle = "ADULT LICENSE WAIVER"
        static let childWaiverSearchString = "I give permission for my Child"
        static let childWaiverTitle = "CHILD WAIVER"
        static let liabilityWaiverSearchString = "release by me and my Child of all liability"
        static let liabilityWaiverTitle = "LIABILITY RELEASE"
        static let mediaGrantWaiverSearchString = "MEDIA GRANT"
        static let mediaGrantWaiverTitle = "MEDIA GRANT AGREEMENT"
        static let codeOfConductWaiverSearchString = "Team Sport Code of Conduct"
        static let codeOfConductWaiverTitle = "CODE OF CONDUCT AGREEMENT"
    }
    
    struct EventSearch {
        
        static let defaultRadius:Int = 15
        
        enum SearchType {
            case location
            case name
        }
        
        enum LocationType {
            case current
            case zip
            case state
        }
        
        enum RaceType: String {
            case all   = "All"
            case road = "Road"
            case track = "Track"
            case mountain = "Mountain"
            case cycloCross = "Cyclo-cross"
            case collegiate = "Collegiate"
            case bmx = "BMX"
            
            static let allValues = [all, road, track, mountain, cycloCross, collegiate, bmx]
            
            init?(id : Int) {
                switch id {
                case 1:
                    self = .all
                case 2:
                    self = .road
                case 3:
                    self = .track
                case 4:
                    self = .mountain
                case 5:
                    self = .cycloCross
                case 6:
                    self = .collegiate
                case 7:
                    self = .bmx
                default:
                    return nil
                }
            }
        }
        
        static let separatorLineThickness:CGFloat = 2.0
        static let controlsViewOffsetStandard:CGFloat = 667.0
        static let controlsViewPaddingStandard:CGFloat = 75.0 
    }
    
    struct RiderLookup {
        static let resultsMockResultsListJsonFile = "sample_rider_lookup_list"
        static let resultsMockLicensesJsonFile = "sample_rider_licenses"
        static let popupHeightNormal:CGFloat = 398.0
        static let popupHeightTechOfficial:CGFloat = 365.0
        static let popupHeightTechCoach:CGFloat = 250.0
        static let popupHeightGenericShort:CGFloat = 250.0
        static let popupHeightGenericMedium:CGFloat = 325.0
        static let popupHeightProLicense:CGFloat = 315.0
        
        static let searchItemsPerPage:Int = 50
        static let searchMoreViewTag:Int = 5000
        static let searchMaxAllowed:Int = 500
        
        static let defaultCornerRadius:CGFloat = 10.0
        static let popupMargin:CGFloat = 10.0
    }
    
    enum APIErrorHandlingMessages: String {
        case invalidRefreshToken = "Invalid refresh_token"
        case failedLogin         = "Invalid username or password"
    }
    
}

