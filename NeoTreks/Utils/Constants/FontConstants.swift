//
//  Created by Brian Elliott on 7/17/17.
//   //
// 
//

import Foundation
import UIKit

struct FontNames {
    
    static let bodyTextFont:UIFont = UIFont.systemFont(ofSize: 14.0, weight: UIFontWeightSemibold)
    
    struct SpecialFonts {
        
        static let bodyTextFont:UIFont = UIFont.systemFont(ofSize: 17.0, weight: UIFontWeightSemibold)
        
    }
    
}
