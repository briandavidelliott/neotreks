//
//  Created by Brian Elliott on 7/17/17.
//   //
// 
//

import Foundation
import UIKit

struct AppColors {
    
    struct BackgroundColors {
        
        static let generalAppBackgroundColor = UIColor(red: 4/255, green: 30/255, blue: 66/255, alpha: 1.0)
        static let generalAppRedColor = UIColor(red: 166/255, green: 25/255, blue: 46/255, alpha: 1.0)
        static let generalAppYellowColor = UIColor(red: 198/255, green: 146/255, blue: 20/255, alpha: 1.0)
    }
    
    struct HomeButtonColors {
        
        static let highlightColor = UIColor.lightGray
    }
    
    struct SpecialColors {
        
        static let textColor = UIColor.black
        static let backgroundColor = UIColor.white
        static let borderColor = UIColor(red: 37.0/255.0, green: 171.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    }
    
    struct BenefitColors {
        static let tabOnBackgroundColor = UIColor.white
        static let tabOffBackgroundColor = UIColor.gray
        static let tabOnTextColor = UIColor.black
        static let tabSelectedTextColor = UIColor.black
        static let tabOffTextColor = UIColor.white
    }
    
    struct EventSearchColors {
        static let eventSearchButtonOffTextColor = UIColor.white
        static let eventSearchButtonSelectedTextColor = UIColor.gray
        static let eventSearchButtoOnTextColor = UIColor.gray
        static let eventViewSeparatorColor = UIColor.black
    }
    
    struct RiderLookupColors {
        static let laStatusActive = UIColor(red: 0.0/255.0, green: 128.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        static let laStatusInactive = UIColor(red: 166.0/255.0, green: 25.0/255.0, blue: 46.0/255.0, alpha: 1.0)
        static let borderColor = UIColor(red: 4/255, green: 30/255, blue: 66/255, alpha: 1.0)
        static let sectionBackgroundColor = UIColor(red: 223/255, green: 229/255, blue: 229/255, alpha: 1.0)
        static let normalCellBackgroundColor = UIColor.white
        static let errorMessageFontColor = UIColor(red: 184/255, green: 20/255, blue: 44/255, alpha: 1.0)
        static let normalFontColor = UIColor.black
        // static let moreRiderInfoPopupBackgroundFontColor = UIColor(red: 210/255, green: 144/255, blue: 45/255, alpha: 1.0)
        static let moreRiderInfoPopupBackgroundFontColor = UIColor(red: 184/255, green: 20/255, blue: 44/255, alpha: 1.0)
    }
    
    struct LicenseColors {
        static let defaultBannerBackgroundColor = "#ffffff"
        static let defaultBodyBackgroundColor = "#ebeff1"
    }
    
    struct WaiverColors {
        static let consentSectionBackgroundColor = UIColor(red: 4/255, green: 30/255, blue: 66/255, alpha: 1.0)
        static let sectionTitleNormalColor = UIColor.black
        static let sectionTitleHighlightColor = UIColor.lightGray
        static let submitButtonInactiveColor = UIColor.gray
        static let submitButtonActiveColor = UIColor(red: 4/255, green: 30/255, blue: 66/255, alpha: 1.0)
        
    }
    
}

