//
//  HTTPManager.swift
// 
//
//  Created by Brian Elliott on 7/15/17.
//   //
//
//

import Foundation
import Alamofire
import Timberjack

class HTTPManager: Alamofire.SessionManager {
    
    static let shared: HTTPManager = {
        let configuration = Timberjack.defaultSessionConfiguration()
        let manager = HTTPManager(configuration: configuration)
        return manager
    }()
}
