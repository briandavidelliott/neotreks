//
//  DataAgent.swift
//
//  Brian Elliott revisions based on original by Patrik DeLoughry
//
//

import Alamofire
import SwiftyJSON
import SystemConfiguration
import UIKit

class DataAgent {
    
    //
    // MARK: Properties
    //
    
    enum APIReturnValues {
        case Success
        case SuccessButNoDataFound
        case InvalidRefreshToken
        case GeneralApplicationError
        case NetworkError
        case InvalidUserNameOrPassword
        case APIDown
        case DeviceOffline
        case GeneralError
    }
    
    enum APIReturnCase {
        case InvalidRefreshToken
        case InvalidUserNameOrPassword
        case None
    }

    enum UserRole: String {
        case Competitor   = "comp"
        case RaceDirector = "rd"
        case Coach        = "coach"
        case Official     = "official"
    }
        
    private static let client_id     = "client-id-goes-here"
    private static let client_secret = "**************************"
    
    private static var token_expires: Date?
    private static var access_token:  String?
    private static var refresh_token: String?
    private static var scope:         UserRole?
    private static var authHeader:    HTTPHeaders?
    
    private static var member:        Member?
    private static var licenses:      [License]?
    
    private static var licensesLastUpdated: Date?
    
    public  static var manager:NetworkManager = NetworkManager()
    
    //
    // MARK: Offline archive methods
    //
    
    static var sessionFilePath: String {
        let manager = FileManager.default
        let url = manager.urls(for: .documentDirectory, in: .userDomainMask).first
        return (url!.appendingPathComponent("SessionData").path)
    }
    
    static var memberDataFilePath: String {
        let manager = FileManager.default
        let url = manager.urls(for: .documentDirectory, in: .userDomainMask).first
        return (url!.appendingPathComponent("MemberData").path)
    }
    
    static var licenseFilePath: String {
        let manager = FileManager.default
        let url = manager.urls(for: .documentDirectory, in: .userDomainMask).first
        return (url!.appendingPathComponent("LicenseData").path)
    }
    
    static func setLastLicenseUpdate(_ inputDate:Date){
        
        self.licensesLastUpdated = inputDate
    }
    
    private static func clearOfflineData() {
        
        let manager = FileManager.default

        do {
            try manager.removeItem(atPath: sessionFilePath)
        }
        catch let error as NSError {
            print("System error - session files not cleared: \(error)")
        }
        
    }
    
    private static func saveSessionData () {
        
        if let access = DataAgent.access_token, let refresh = DataAgent.refresh_token, let expires = DataAgent.token_expires,
            let scope = DataAgent.scope {
            
            let session = Session(access, refreshToken:refresh, expires:expires, scope:scope, lastUpdated:licensesLastUpdated)
            
            NSKeyedArchiver.archiveRootObject(session, toFile: sessionFilePath)
        }

    }
    
    private static func loadSessionData() {
        
        if let session = NSKeyedUnarchiver.unarchiveObject(withFile: sessionFilePath) as? Session {
            
            if let token = session.access_token {
                
                DataAgent.access_token = token
                // need to reinitialize so it isn't nil
                DataAgent.authHeader    = [
                    "Authorization": "Bearer " + token
                ]
            }
            
            if let refresh = session.refresh_token {
                DataAgent.refresh_token = refresh
            }
            
            if let expires = session.token_expires {
                DataAgent.token_expires = expires
            }
            
            if let scope_value = session.scope {
                DataAgent.scope = scope_value
            }
            
            if let last_updated = session.licensesLastUpdated {
                DataAgent.licensesLastUpdated = last_updated
            }
        }
    }
    
    private static func saveMemberData (_ item: Member) {
        NSKeyedArchiver.archiveRootObject(item, toFile: memberDataFilePath)
    }
    
    private static func loadMemberData() {
        if let ourData = NSKeyedUnarchiver.unarchiveObject(withFile: memberDataFilePath) as? Member {
            DataAgent.member = ourData
        }
    }
    
    private static func saveLicenses(_ items: [License]) {
        NSKeyedArchiver.archiveRootObject(items, toFile: licenseFilePath)
    }

    private static func loadLicenses() {
        if let ourData = NSKeyedUnarchiver.unarchiveObject(withFile: licenseFilePath) as? [License] {
            DataAgent.licenses = ourData
        }
    }
    
    //
    // Cache data from memory onto disk
    //
    static func serializeToDisk() {
        
        saveSessionData()
        
        if let memberObject = DataAgent.member {
            saveMemberData(memberObject)
        }

        if let licensesArray = DataAgent.licenses {
            DataAgent.saveLicenses(licensesArray)
        }
        
    }
    
    //
    // Load cached data into memory from disk
    //
    static func unserializeFromDisk() {
        
        DataAgent.loadSessionData()
        DataAgent.loadMemberData()
        DataAgent.loadLicenses()
    }
    
    //
    // Delete all cached member data
    //
    static func reset() {

        token_expires = nil
        access_token  = nil
        refresh_token = nil
        scope         = nil
        authHeader    = nil
        member        = nil
        licenses      = nil
        
        DataAgent.clearOfflineData()
    }
    
    //
    // MARK: Network calls
    //
    
    //
    // Authenticate user login and cache tokens
    //
    static func authenticate(userRole role: UserRole, withUsername username: String, andPassword password: String, _ completion: @escaping (_ successful: APIReturnValues) -> Void) {

        let params: Parameters = [
            "grant_type":    "password",
            "client_id":     DataAgent.client_id,
            "client_secret": DataAgent.client_secret,
            "username":      username,
            "password":      password,
            "scope":         role.rawValue
        ]

        if let apiUrl = manager.apiUrl {
            
            let request:DataRequest = manager.networkRequest("\(apiUrl)/oauth/v2/token", method:NetworkingConstants.HTTPMethod.post, params: params)
            request.validate(contentType: ["application/json"])
                .responseJSON { response in
                    
                    let returnCode = processServerResponse(response, returnCase: GeneralConstants.APIErrorHandlingMessages.failedLogin)
                    if returnCode == APIReturnValues.Success {
                        
                        if let data = response.data {
                            DataAgent.tokenCallback(json: JSON(data))
                            DataAgent.scope = role
                            print("Login Successful! \(DataAgent.access_token ?? "UNDEFINED")")
                            completion(returnCode)
                        }
                    }
                    else {
                        print("Login Failed: \(returnCode)")
                        completion(returnCode)
                    }
                    
            }
        }
    }
    
    //
    // Get new access token using refresh token
    //
    static func refreshToken(_ completion: @escaping (APIReturnValues) -> Void) {
        
        var params: Parameters = [
            "grant_type":    "refresh_token",
            "client_id":     DataAgent.client_id,
            "client_secret": DataAgent.client_secret,
            "refresh_token": "",
            "scope":         "comp"
        ]
        
        if let token = DataAgent.refresh_token, let scope = DataAgent.scope {
            
            params = [
                "grant_type":    "refresh_token",
                "client_id":     DataAgent.client_id,
                "client_secret": DataAgent.client_secret,
                "refresh_token": token,
                "scope":         scope.rawValue
            ]
            
            if NetworkingConstants.networkingDebugFlag {
                print("refreshToken .... token: \(token)")
            }
            
        }
        
        if let apiUrl = manager.apiUrl {
            
            let request:DataRequest = manager.networkRequest("\(apiUrl)/oauth/v2/token", method:NetworkingConstants.HTTPMethod.post, params: params)
                request.validate(contentType: ["application/json"])
                .responseJSON { response in
                    
                    let returnCode = processServerResponse(response, returnCase: GeneralConstants.APIErrorHandlingMessages.invalidRefreshToken)
                    if returnCode == APIReturnValues.Success {

                        if let data = response.data {
                            DataAgent.tokenCallback(json: JSON(data))
                            print("Refresh Successful! \(DataAgent.access_token ?? "UNDEFINED")")
                            completion(APIReturnValues.Success)
                        }
                    }
                    else {
                        completion(returnCode)
                    }
                
            }
        }
    }
    
    static func containsTokenString(_ tokenArray:[String], message:String) -> Bool {

        for item in tokenArray {
            if message.lowercased().contains(item.lowercased()) {
                return true
            }
        }

        return false
        
    }

    static func processServerResponse(_ response:DataResponse<Any>?, returnCase:GeneralConstants.APIErrorHandlingMessages) -> APIReturnValues {
        
        if let statusCode = response?.response?.statusCode {

            // check for success
            if 200..<300 ~= statusCode {
                
                // check that although successful that there is data returned
                if let _ = response?.data {
                    return APIReturnValues.Success
                }
                    // blank JSON data
                else {
                    return APIReturnValues.SuccessButNoDataFound
                }
            }
            // check for API status of 400 or 401 and invalid token JSON message
            else if statusCode == 400 || statusCode == 401 {

                if let value = response?.result.value {
                    let error = value as! NSDictionary
                    let errorMessage = error.object(forKey: "message") as! String
                  
                    let parsedArray = returnCase.rawValue.components(separatedBy: " ")
                    if containsTokenString(parsedArray, message:errorMessage) {

                        if returnCase == GeneralConstants.APIErrorHandlingMessages.invalidRefreshToken {
                            return APIReturnValues.InvalidRefreshToken
                        }
                        else if returnCase == GeneralConstants.APIErrorHandlingMessages.failedLogin {
                            return APIReturnValues.InvalidUserNameOrPassword
                        }
                        else {
                            return APIReturnValues.GeneralError
                        }
                    }
                    else {
                        print(errorMessage)
                        return APIReturnValues.NetworkError
                    }
                }
            }
                // check for API status of 500 for API down or erroring out
            else if statusCode == 500 {
                return APIReturnValues.APIDown
            }
            else {
                return APIReturnValues.NetworkError
            }
            
        }
        
        return APIReturnValues.GeneralError
    }
    
    //
    // New token(s) handler
    //
    static private func tokenCallback(json: JSON) {
        
        DataAgent.access_token  = json["access_token"].stringValue
        DataAgent.refresh_token = json["refresh_token"].stringValue
        DataAgent.authHeader    = [
            "Authorization": "Bearer " + DataAgent.access_token!
        ]
        
        // set date when access token expires
        let expires_in = TimeInterval(json["expires_in"].intValue)
        DataAgent.token_expires = Date()
        DataAgent.token_expires?.addTimeInterval(expires_in)
        
        // store new tokens to device
        DataAgent.serializeToDisk()
    }
    
    //
    // Do we have a valid access token for making requests?
    //
    static func isAuthenticated() -> Bool {

        // check date when access token expires
        if let expires = DataAgent.token_expires {
            if expires > Date() {
                return true
            }
        }
        
        return false
    }
    
    //
    // MARK: Data utility methods
    //
    
    //
    // Did we have a valid token at some point?
    //
    static func hasTokens() -> Bool {
        return DataAgent.refresh_token != nil
    }
    
    //
    // Get the cached access token
    //
    static func getAccessToken() -> String {
        if let token = DataAgent.access_token {
            return token
        }
        else {
            return ""
        }
    }
    
    //
    // Get the cached license last updated date
    //
    static func getLastUpdated() -> Date? {
        
        if let lastUpdate = licensesLastUpdated {
            return lastUpdate
        }
        else {
            return nil
        }
    }
    
    //
    // Get the cached member profile
    //
    static func getMemberProfile() -> Member? {
        
        if let member = member {
            return member
        }
        else {
            return nil
        }
    }
    
    //
    // Get the cached member licenses
    //
    static func getMemberLicenses() -> [License]? {
        
        if let licenses = licenses {
            return licenses
        }
        else {
            return nil
        }
    }
    
    static func getMemberLicense(_ id:Int) -> License? {
        
        if let licenseArray = licenses {
            for license in licenseArray {
                if license.license_id == id {
                    return license
                }
            }
        }
        
        return nil
    }
    
    //
    // MARK: refresh data networking methods
    //
    
    //
    // Update the cached member data (account and license info)
    //
    static func updateMemberData(completion: @escaping (_ success: Bool) -> Void) {
        
        // Create threading group
        let taskGroup = DispatchGroup()
        
        // Dispatch API call on new thread
        taskGroup.enter()
        
        // network call for profile
        if let headers = authHeader, let apiUrl = manager.apiUrl {
            
            let profileRequest:DataRequest = manager.networkRequest("\(apiUrl)/profile", method:NetworkingConstants.HTTPMethod.get, headers: headers)
        
            profileRequest.validate()
                .responseJSON { response in
                
                // Always term the thread when done
                defer {
                    taskGroup.leave()
                }
                
                switch response.result {
                    case .success(let data):
                        self.member = Member(initWithJSON: JSON(data))
                    
                    case .failure:
                        print("Could not update member profile")
                    }
                }
            
            // Another concurrent API call on new thread
            taskGroup.enter()
            
            let licenseRequest:DataRequest = manager.networkRequest("\(apiUrl)/licenses", method:NetworkingConstants.HTTPMethod.get, headers: headers)
            
            licenseRequest.validate()
                .responseJSON { response in
                    
                    // Always term the thread when done
                    defer {
                        taskGroup.leave()
                    }
                    
                    switch response.result {
                    case .success(let data):
                        let json = JSON(data)
                        self.licenses = [License]()
                        json.forEach() { _, license in
                            print("DataAgent ... updateMemberData ... license json: \(license)")
                            self.licenses?.append(License(initWithJSON: license))
                        }
                        DataAgent.setLastLicenseUpdate(Date())
                        
                    case .failure:
                        print("Could not update member licenses")
                    }
            }
            
            // Threading group completion handler
            // (fires when all threads have completed)
            taskGroup.notify(queue: .main, work: DispatchWorkItem(block: {
                
                // network call for Official licenses (workaround for now)
                if let compId = member?.comp_id {
                    DataAgent.updateOfficialLicenses(String(compId), mock:false) {
                        (returnValue) in
                        
                        // write to disk even if consolidate fails because backup licenses are populated
                        DataAgent.serializeToDisk()
                        completion(self.member != nil && self.licenses != nil)
                    }
                }
                else {
                    DataAgent.serializeToDisk()
                    completion(self.member != nil && self.licenses != nil)
                }
            }))
        }
        
    }
    
    // Work-around for Official licenses
    static func updateOfficialLicenses(_ compid:String, mock:Bool, completion: @escaping (APIReturnValues) -> Void) {
        
        DataAgent.getLicenseData (compid, mock:mock)
        { (data: JSON) in
            
            var tempLicense:License?
            
            let count: Int? = data.array?.count
            
            // No results returned
            if data["Error"] == 500 {
                completion(APIReturnValues.GeneralApplicationError)
            }
            // No licenses returned
            else if data.isEmpty || count == 0 {
                completion(APIReturnValues.SuccessButNoDataFound)
            }
            else {
                
                data.forEach() { _, eachLicense in
                    
                    let license = License(initWithJSON: eachLicense)
                    
                    // only grab official license
                    if let header = license.section_header, let subtitle = license.section_subtitle {
                        if header == "TECHNICAL" && subtitle == "OFFICIAL" {
                            // check expiration date
                            if !AppUtils.isDateExpired(license.expiration_date) {
                                tempLicense = license
                            }
                        }
                    }
                }
                
                if let officialLicense = tempLicense {
                    DataAgent.replaceOfficialLicenses(officialLicense)
                }
                
                completion(APIReturnValues.Success)
            }
        }
        
    }
        
    //
    // Replaces all the official licenses with the consolidate license info
    //
    static func replaceOfficialLicenses (_ consolidatedLicense:License) {
        
        var finalLicenseArray:[License] = []
        
        if let licenseArray = DataAgent.licenses {
            for license in licenseArray {
                
                if license.license_type.uppercased() != "O" {
                    finalLicenseArray.append(license)
                }

            }
            
            // add consolidated official licenses
            finalLicenseArray.append(consolidatedLicense)
        }
        
        if finalLicenseArray.count > 0 {
            DataAgent.licenses = finalLicenseArray
        }
    }
    
    //
    // Retrieve license data
    //
    static func getLicenseData (_ compid:String, mock:Bool, completion: @escaping (_ results: JSON) -> Void) {
        
        if let apiUrl = manager.apiUrl {
            
            let address = "\(apiUrl)/licenses?" + "comp_id=\(compid)"
            
            if mock {
                
                // no results - use this line
                // completion("")
                
                // system error - use this line
                //  completion(["Error":500, "Message":"Error: No riders found!"])
                
                if let json = AppUtils.readJson(GeneralConstants.RiderLookup.resultsMockLicensesJsonFile) {
                    completion(json)
                }
                else {
                    completion(["Error":500, "Message":"Mock license data not read!"])
                    print("Mock license data not read.")
                }
            }
            else {
                
                if let headers = authHeader {
                    
                    let ridersRequest:DataRequest = manager.networkRequest(address, method:NetworkingConstants.HTTPMethod.get, headers: headers, mock:false )
                    ridersRequest.validate()
                        .responseJSON { response in
                            switch response.result {
                            case .success(let data):
                                completion(JSON(data))
                                
                            case .failure:
                                
                                completion(["Error":500, "Message":"Error: Could not retreive license data!"])
                            }
                    }
                }
                else {
                    
                    if GeneralConstants.generalDebugFlag {
                        SCLAlertView().showError("getLicenseData Error", subTitle: "authHeader is nil")
                    }
                }
            }
        }
    }
    
    //
    // Signoff on waiver 
    //
    static func acceptWaiver (_ mock:Bool, waiverId:Int, signature:String, completion: @escaping (_ results: JSON) -> Void) {
        
        if let ccnApiUrl = manager.ccnApiUrl, let member = DataAgent.getMemberProfile() {
            
            guard let identityId = member.ccn_identity_id else {
                completion(["Error":500, "Message":"Error: Could not approve waiver!!"])
                return
            }
            
            let address = "\(ccnApiUrl)/rest/v2/waiver-signature-requests/\(waiverId)/accept/"
            
            var params: Parameters = [
                "identity_id": identityId,
                "signature":   "\(signature)"
            ]
            
            if mock {
                
                // no results - use this line
                // completion("")
                
                // system error - use this line
                //  completion(["Error":500, "Message":"Error: Mock waiver data not read!"])
                
                if let json = AppUtils.readJson(GeneralConstants.RiderLookup.resultsMockLicensesJsonFile) {
                    completion(json)
                    return
                }
                else {
                    completion(["Error":400, "Message":"Mock Could not approve waiver!"])
                    print("Mock - Could not approve waiver.")
                }
            }
            else {
                
                var authHeader:HTTPHeaders = HTTPHeaders()
                
                if let targetEnv = Bundle.main.infoDictionary?["TARGET_ENVIRONMENT_NAME"] as? String {
                    if !targetEnv.contains("PROD") {
                        authHeader["Authorization"] = "Basic Y2NuOndoYW1tb2JsYW1tbw=="
                    }
                }
                
                let cookieString = buildCcnCookie()
                if  cookieString.count > 0 {
                    
                    if let cookie = cookieString.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) {

                        authHeader["Cookie"] = "TOKEN=" + cookie
                        
                        authHeader["Content-Type"] = "application/json"
                        authHeader["Accept"] = "application/json"
                        
                        if let targetEnv = Bundle.main.infoDictionary?["TARGET_ENVIRONMENT_NAME"] as? String {
                            if !targetEnv.contains("PROD") {
                                authHeader["Authorization"] = "Basic Y2NuOndoYW1tb2JsYW1tbw=="
                            }
                        }

                        let approveRequest:DataRequest = manager.networkRequest(address, method:NetworkingConstants.HTTPMethod.put, headers:authHeader, params: params, mock:false)
                        approveRequest.validate()
                            .responseJSON { response in
                                switch response.result {
                                case .success(let data):
                                    completion(JSON(data))
                                    return
                                case .failure:
                                    completion(["Error":400, "Message":"Error: Could not approve waiver!"])
                                    return
                                }
                        }
                    }
                    else if GeneralConstants.generalDebugFlag {
                        print("Get Waiver Info Error")
                        return
                    }
                    else {
                        completion(["Error":400, "Message":"Error: Could not approve waiver!"])
                        return
                    }
                    return
                }
                else {
                    
                    if GeneralConstants.generalDebugFlag {
                        print("Get Waiver Info Error")
                    }
                    completion(["Error":400, "Message":"Error: Could not approve waiver!"])
                    return
                }
            }
        }
        else {
            completion(["Error":400, "Message":"Error: Could not approve waiver!"])
            return
        }
    }
    
    //
    // Retrieve waiver data
    //
    static func getWaiverInfo (_ mock:Bool, completion: @escaping (_ results: JSON) -> Void) {

        if let ccnApiUrl = manager.ccnApiUrl, let member = DataAgent.getMemberProfile() {
            
            guard let identityId = member.ccn_identity_id else {
                completion(["Error":500, "Message":"Error: Could not retreive waiver data!"])
                return
            }
            
            let address = "\(ccnApiUrl)/rest/v2/waiver-signature-requests/?" + "identity_id=\(identityId)"
            
            if mock {
                
                // no results - use this line
                // completion("")
                
                // system error - use this line
                //  completion(["Error":500, "Message":"Error: Mock waiver data not read!"])
                
                if let json = AppUtils.readJson(GeneralConstants.RiderLookup.resultsMockLicensesJsonFile) {
                    completion(json)
                }
                else {
                    completion(["Error":500, "Message":"Mock waiver data not read!"])
                    print("Mock license data not read.")
                }
            }
            else {
                
                var authHeader:HTTPHeaders = HTTPHeaders()
                
                if let targetEnv = Bundle.main.infoDictionary?["TARGET_ENVIRONMENT_NAME"] as? String {
                    if !targetEnv.contains("PROD") {
                        authHeader["Authorization"] = "Basic Y2NuOndoYW1tb2JsYW1tbw=="
                    }
                }
                
                let cookieString = buildCcnCookie()
                if  cookieString.count > 0 {
                    
                    if let cookie = cookieString.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) {
                        
                        print("DataAgent .... cookie: \(cookie)")
                        authHeader["Cookie"] = "TOKEN=" + cookie
                    
                        let ridersRequest:DataRequest = manager.networkRequest(address, method:NetworkingConstants.HTTPMethod.get, headers: authHeader, mock:false )
                            ridersRequest.validate()
                                .responseJSON { response in
                                    switch response.result {
                                    case .success(let data):
                                        completion(JSON(data))
                                
                                    case .failure:
                                
                                        completion(["Error":500, "Message":"Error: Could not retreive waiver data!"])
                                    }
                        }
                    }
                    else if GeneralConstants.generalDebugFlag {
                        print("Get Waiver Info Error")
                    }
                }
                else {
                    
                    if GeneralConstants.generalDebugFlag {
                        print("Get Waiver Info Error")
                    }
                }
            }
        }
        else {
            completion(["Error":500, "Message":"Error: Could not retreive waiver data!"])
        }
    }
    
    static func buildCcnCookie() -> String {
        
        if let accessToken = DataAgent.access_token, let refreshToken = DataAgent.refresh_token, let expiresDate = DataAgent.token_expires {
            
            let now = Date()
            let calendar = NSCalendar.current
            var compos:Set<Calendar.Component> = Set<Calendar.Component>()
            compos.insert(.second)
            let difference = calendar.dateComponents(compos, from: now, to: expiresDate)
            
            if let seconds = difference.second {
            
//                let cookie: [String: String] = [
//                    "token_type" : "Bearer",
//                    "access_token" : accessToken,
//                    "expires_in" : String(seconds),
//                    "refresh_token" : refreshToken
//                ]
                
                let cookie: [String: String] = [
                    "token_type" : "Bearer",
                    "access_token" : accessToken,
                    "expires_in" : String(seconds),
                    "refresh_token" : refreshToken
                ]
                
                let jsonObj = JSON(cookie)
                if let stringJSON = jsonObj.rawString(String.Encoding.utf8, options: []) {
                    return stringJSON
                }
                
                return ""
            }

        }

        return ""

    }
    
    // search by name
    static func getEventsByName (_ name:String, completion: @escaping (_ results: JSON) -> Void) {
        
        if let apiUrl = manager.apiUrl {
            let address = "\(apiUrl)/events?" + "name=\(name)".addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
            getEvents(address: address, completion: completion)
        }
    }
    
    // search by state
    static func getEventsByState (_ state:String, withinRadius radius: Int, betweenDates fromDate: Date?, and toDate: Date?, raceType:GeneralConstants.EventSearch.RaceType?, completion: @escaping (_ results: JSON) -> Void) {
        
        var dateString = ""
        
        if let fromDate = fromDate, let toDate = toDate {
            
            if let fromDateString:String = formatDate(fromDate), let toDateString:String = formatDate(toDate) {
                dateString = "&from=\(fromDateString)&to=\(toDateString)"
            }
        }
        
        var eventTypeString = ""
        if let type = raceType {
            if type != GeneralConstants.EventSearch.RaceType.all {
                eventTypeString = "&type=\(type.rawValue)"
            }
        }
        
        if let apiUrl = manager.apiUrl {
            let address = "\(apiUrl)/events?" + "state=\(state)" + eventTypeString + dateString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        
            getEvents(address: address, completion: completion)
        }
    }
    
    // search by zip (includes device current location and zip search
    static func getEventsByZip (_ zip:String, withinRadius radius: Int, betweenDates fromDate: Date?, and toDate: Date?, raceType:GeneralConstants.EventSearch.RaceType?, completion: @escaping (_ results: JSON) -> Void) {
        
        var dateString = ""
        
        if let fromDate = fromDate, let toDate = toDate {
            
            if let fromDateString:String = formatDate(fromDate), let toDateString:String = formatDate(toDate) {
                dateString = "&from=\(fromDateString)&to=\(toDateString)"
            }
        }
        
        var eventTypeString = ""
        if let type = raceType {
            if type != GeneralConstants.EventSearch.RaceType.all {
                eventTypeString = "&type=\(type.rawValue)"
            }
        }
        
        if let apiUrl = manager.apiUrl {
            let address = "\(apiUrl)/events?" + "zip=\(zip)&radius=\(radius)" + eventTypeString + dateString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        
            getEvents(address: address, completion: completion)
        }
    }
    
    static func formatDate(_ input:Date) -> String? {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        if let dateString = formatter.string(from: input).stringByAddingPercentEncodingToData() {
            return dateString
        }
        else {
            return nil
        }
        
    }
    
    static func getEvents(address:String, completion: @escaping (_ results: JSON) -> Void) {
        
        if let headers = authHeader {
            
            let eventsRequest:DataRequest = manager.networkRequest(address, method:NetworkingConstants.HTTPMethod.get, headers: headers )
            eventsRequest.validate()
                .responseJSON { response in
                    
                    switch response.result {
                    case .success(let data):
                        completion(JSON(data))
                        
                    case .failure:
                        
                        completion(["Error":500, "Message":"Error: Could not search events!"])
                        print("Could not search events")
                    }
            }
        }
        else {
            
            if GeneralConstants.generalDebugFlag {
                SCLAlertView().showError("getEvents Error", subTitle: "authHeader is nil")
            }
        }
        
    }
    
    static func getRidersByName (_ name:String, pageNumber:Int, mock:Bool, completion: @escaping (_ results: JSON) -> Void) {
        
        if let apiUrl = manager.apiUrl {
            
            if let searchName = name.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
                let pagingString = getRiderLookupPagingString(pageNumber)
                let address = "\(apiUrl)/rider_lookup?name=\(searchName)\(pagingString)"
                getRiders(address: address, mock:mock, completion: completion)
            }
            else {
                // gives nothing but shows no results
                let address = "\(apiUrl)/rider_lookup?name="
                getRiders(address: address, mock:mock, completion: completion)
            }
            
        }
    }
    
    static func getRidersByLicenseId (_ licenseId:String, pageNumber:Int, mock:Bool, completion: @escaping (_ results: JSON) -> Void) {
        
        if let apiUrl = manager.apiUrl {
            
            if let compId = licenseId.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
                let pagingString = getRiderLookupPagingString(pageNumber)
                let address = "\(apiUrl)/rider_lookup?comp_id=\(compId)\(pagingString)"
                getRiders(address: address, mock:mock, completion: completion)
            }
            else {
                // gives nothing but shows no results
                let address = "\(apiUrl)/rider_lookup?comp_id="
                getRiders(address: address, mock:mock, completion: completion)
            }

        }
    }
    
    static func getRiderLookupPagingString(_ pageNumber:Int) -> String {
        
        var returnString = ""
        
        let itemsPerPage = GeneralConstants.RiderLookup.searchItemsPerPage
        let pageStartIndex = (pageNumber - 1) * itemsPerPage
        
        returnString = "&page_start=\(pageStartIndex)&page_limit=\(itemsPerPage)"
        
        return returnString
    }
    
    static func getRiders(address:String, mock:Bool, completion: @escaping (_ results: JSON) -> Void) {
        
        if mock {
            
            // no results - use this line
            // completion("")
            
            // system error - use this line
            //  completion(["Error":500, "Message":"Error: No riders found!"])

            if let json = AppUtils.readJson(GeneralConstants.RiderLookup.resultsMockResultsListJsonFile) {
                completion(json)
            }
            else {
                completion(["Error":500, "Message":"Mock rider lookup list data not read!"])
                print("Mock rider lookup list data not read.")
            }
        }
        else {
        
            if let headers = authHeader {
            
                let ridersRequest:DataRequest = manager.networkRequest(address, method:NetworkingConstants.HTTPMethod.get, headers: headers, mock:false )
                    ridersRequest.validate()
                        .responseJSON { response in
                            switch response.result {
                            case .success(let data):
                                completion(JSON(data))
                        
                            case .failure:
                                
                                completion(["Error":500, "Message":"Error: Could not search riders!"])
                                print("Could not search riders")
                            }
                    }
            }
            else {
            
                if GeneralConstants.generalDebugFlag {
                    SCLAlertView().showError("getRiders Error", subTitle: "authHeader is nil")
                }
            }
        }
        
    }
    
    //
    // MARK: Debugging
    //
    
    public static func dumpDataAgent() {
        
        print("\n\n observeValue ... dump DataAgent  ... ")
        
        print(" observeValue - access_token: |\(String(describing: DataAgent.access_token))|")
        print(" observeValue - refresh_token: |\(String(describing: DataAgent.refresh_token))|")
        print(" observeValue - token_expires: |\(String(describing: DataAgent.token_expires))|")
        print(" observeValue - scope: |\(String(describing: DataAgent.scope))|")
        
        print(" observeValue - member: compid: |\(String(describing: DataAgent.member?.comp_id))|, username: \(String(describing: DataAgent.member?.username))")
        print(" observeValue - licenses: count: |\(String(describing: DataAgent.licenses?.count))|")
        
        print("observeValue ... dump DataAgent  ... \n\n ")
    }
    
}

protocol Utilities {
}

//
// MARK: Reachability
//
extension DataAgent: Utilities {
    enum ReachabilityStatus {
        case notReachable
        case reachableViaWWAN
        case reachableViaWiFi
    }
    
    static func getReachabilityStatus() -> ReachabilityStatus {
        var zeroAddress        = sockaddr_in()
        zeroAddress.sin_len    = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return .notReachable
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return .notReachable
        }
        
        if flags.contains(.reachable) == false {
            // The target host is not reachable.
            return .notReachable
        }
        else if flags.contains(.isWWAN) == true {
            // WWAN connections are OK if the calling application is using the CFNetwork APIs.
            return .reachableViaWWAN
        }
        else if flags.contains(.connectionRequired) == false {
            // If the target host is reachable and no connection is required then we'll assume that you're on Wi-Fi...
            return .reachableViaWiFi
        }
        else if (flags.contains(.connectionOnDemand) == true || flags.contains(.connectionOnTraffic) == true) && flags.contains(.interventionRequired) == false {
            // The connection is on-demand (or on-traffic) if the calling application is using the CFSocketStream or higher APIs and no [user] intervention is needed
            return .reachableViaWiFi
        }
        else {
            return .notReachable
        }
    }
}
