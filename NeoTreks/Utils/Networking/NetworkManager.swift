//
//  NetworkManager.swift
 //
// 
//
//  Created by Brian Elliott on 7/18/17.
//   //
// 
//

import Foundation
import Alamofire

class NetworkManager {
    
    var apiUrl:String?
    var ccnApiUrl:String?
    
    func sendNetworkRequest() -> HTTPURLResponse? {
       
        if NetworkingConstants.networkingDebugFlag {
            
        }
        
        return nil
    }
    
    func networkRequest<T> (_ url:String, method:NetworkingConstants.HTTPMethod, headers:[String: String], params:[String : Any], mock:Bool = false) -> T  {
        
        // mock network call but not networking debugging
        if (mock && !NetworkingConstants.networkingDebugFlag) {
            
            let returnRequest:MockRequest = MockRequest(url, method:method, params:params)
            return returnRequest as! T
            
        }
        // networking debugging
        else if NetworkingConstants.networkingDebugFlag {
            
            if let urlObject = URL(string: url) {
                
                var urlRequest = URLRequest(url: urlObject)
                urlRequest.httpMethod = getAlamofireMethod(method).rawValue
                urlRequest.allHTTPHeaderFields = headers

                urlRequest.httpBody = try? JSONSerialization.data(withJSONObject: params)
                urlRequest.allHTTPHeaderFields = headers
                
                return HTTPManager.shared.request(urlRequest) as! T
            }
            else {
                return HTTPManager.shared.request(url, method: getAlamofireMethod(method), parameters: params)  as! T
            }
            
        }
        // normal path
        else {
            
            if let urlObject = URL(string: url) {
                
                var urlRequest = URLRequest(url: urlObject)
                urlRequest.httpMethod = getAlamofireMethod(method).rawValue
                urlRequest.allHTTPHeaderFields = headers
                urlRequest.httpBody = try? JSONSerialization.data(withJSONObject: params)
                urlRequest.allHTTPHeaderFields = headers
                
                return Alamofire.request(urlRequest) as! T
            }
            else {
                return Alamofire.request(url, method: getAlamofireMethod(method), parameters: params)  as! T
            }
        }
        
    }
    
    func networkRequest<T> (_ url:String, method:NetworkingConstants.HTTPMethod, params:[String : Any], mock:Bool = false) -> T  {
        
        // mock network call but not networking debugging
        if (mock && !NetworkingConstants.networkingDebugFlag) {
            
            let returnRequest:MockRequest = MockRequest(url, method:method, params:params)
            return returnRequest as! T
            
        }
        // networking debugging
        else if NetworkingConstants.networkingDebugFlag {
                        
            let parameters:Parameters = params
            let returnRequest:DataRequest = HTTPManager.shared.request(url, method: getAlamofireMethod(method), parameters: parameters)
            return returnRequest as! T
            
        }
        // normal path
        else {
            
            let parameters:Parameters = params
            let returnRequest:DataRequest = Alamofire.request(url, method: getAlamofireMethod(method), parameters: parameters)
            return returnRequest as! T

        }
        
    }
    
    func networkRequest<T> (_ url:String, method:NetworkingConstants.HTTPMethod, headers:[String: String], mock:Bool = false) -> T  {
        
        // mock network call but not networking debugging
        if (mock && !NetworkingConstants.networkingDebugFlag) {
            
            let headerValues:HTTPHeaders = headers
            let returnRequest:MockRequest = MockRequest(url, method:method, headers:headerValues)
            return returnRequest as! T
            
        }
        // networking debugging
        else if NetworkingConstants.networkingDebugFlag {
            
            let headerValues:HTTPHeaders = headers
            let returnRequest:DataRequest = HTTPManager.shared.request(url, method: getAlamofireMethod(method), headers:headerValues)
            return returnRequest as! T
            
        }
        // normal path
        else {
            
            let headerValues:HTTPHeaders = headers
            let returnRequest:DataRequest = Alamofire.request(url, method: getAlamofireMethod(method), headers:headerValues)
            return returnRequest as! T
            
        }
        
    }
    
    func getAlamofireMethod(_ method:NetworkingConstants.HTTPMethod) -> Alamofire.HTTPMethod {
    
        switch method
        {
        case NetworkingConstants.HTTPMethod.options:
            return Alamofire.HTTPMethod.options
        case NetworkingConstants.HTTPMethod.get:
            return Alamofire.HTTPMethod.get
        case NetworkingConstants.HTTPMethod.head:
            return Alamofire.HTTPMethod.head
        case NetworkingConstants.HTTPMethod.post:
            return Alamofire.HTTPMethod.post
        case NetworkingConstants.HTTPMethod.put:
            return Alamofire.HTTPMethod.put
        case NetworkingConstants.HTTPMethod.patch:
            return Alamofire.HTTPMethod.patch
        case NetworkingConstants.HTTPMethod.delete:
            return Alamofire.HTTPMethod.delete
        case NetworkingConstants.HTTPMethod.trace:
            return Alamofire.HTTPMethod.trace
        case NetworkingConstants.HTTPMethod.connect:
            return Alamofire.HTTPMethod.connect
//        default:
//            return Alamofire.HTTPMethod.get
        }
    }
    
    init() {
        
        if let url = Bundle.main.infoDictionary?["DEFAULT_SERVER_PRODUCTION"] as? String {
            
            apiUrl = url
            
            if let overridePath = Bundle.main.infoDictionary?["OVERRIDE_API_PATH"] as? String {
                print("override api papth: \(overridePath)")
                apiUrl = overridePath
            }
        }
        
        if let ccnUrl = Bundle.main.infoDictionary?["DEFAULT_CCN_SERVER_PRODUCTION"] as? String {
            
            ccnApiUrl = ccnUrl
            
            if let overrideCCNPath = Bundle.main.infoDictionary?["OVERRIDE_CCN_API_PATH"] as? String {
                print("override CCN api papth: \(overrideCCNPath)")
                ccnApiUrl = overrideCCNPath
            }
        }

        
    }

}
