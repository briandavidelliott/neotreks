//
//  MockRequest.swift
 //
// 
//
//  Created by Brian Elliott on 7/24/17.
//   //
// 
//

import Foundation
import Alamofire

// class MockRequest:Alamofire.DataRequest { // problems subclassing Alamofire
    
class MockRequest {
    
    var url:String?
    var method:NetworkingConstants.HTTPMethod = NetworkingConstants.HTTPMethod.post
    var parameters:[String:Any]?
    var headers:[String: String]?

    struct response{
        static var data:HTTPURLResponse?
        static var json:AnyObject?
        static var error:NSError?
    }
    
    static let caseRiderLookupByName = "/riderlookup?name="
    static let caseRiderLookupByLicenseId = "/riderlookup?comp_id="
    
    init(_ url:String, method:NetworkingConstants.HTTPMethod, params:[String : Any]) {
        self.url = url
        self.method = method
        self.parameters = params
    }
    
    convenience init(_ url:String, method:NetworkingConstants.HTTPMethod, headers:[String: String]) {
        
        let parameters = ["":""]
        self.init(url, method:method, params:parameters)
        self.headers = headers

    }
    
    func validate() -> Bool {
        return true
    }
    
    public func responseJSON(options: JSONSerialization.ReadingOptions = .allowFragments, completionHandler: (URLRequest, HTTPURLResponse?, AnyObject?, NSError?) -> Void) -> Self {
        
        print("responseJSON ... ")
        
        completionHandler(NSURLRequest(url: NSURL(string:self.url!)! as URL) as URLRequest, MockRequest.response.data, MockRequest.response.json, MockRequest.response.error)
        return self
    }
    
    
}
