//
//  EventSearchManager.swift
 //
// 
//
//  Created by Brian Elliott on 8/21/17.
//   //
// 
//

import Foundation
import MapKit
import SwiftyJSON
import CoreLocation

class EventSearchManager {
    
    //
    // MARK: Properties
    //
    
    private var mapView:MKMapView!
    private var parentViewController:UIViewController!
    private var alertViewResponder:SCLAlertViewResponder?
    
    private var deviceZipCode:String?
    public let defaultUserZipCode:String?
    private var deviceCurrentLocation:CLLocation?
    
    private let defaultRadius:Int!
    private let defaultFromDate:Date!
    private let defaultToDate:Date!
    private let defaultZip:String
    
    private var spinnerIsDisplayed:Bool = false
    private var noDataFoundIsDisplayed:Bool = false
    private var mapZoomUpdatedOnce = false
    
    init(_ parent:UIViewController, mapView:MKMapView) {
        
        self.mapView = mapView
        self.parentViewController = parent
        
        defaultRadius = GeneralConstants.EventSearch.defaultRadius
        defaultFromDate = Date()
        defaultToDate = defaultFromDate.aYearFromNow()
        defaultZip = "80919"
        
        if let member: Member = DataAgent.getMemberProfile() {
            self.defaultUserZipCode = member.zip
        }
        else {
            self.defaultUserZipCode = defaultZip
        }
        
    }
    
    //
    // MARK: Search Utils
    //
    
    func processSearchEventsResults(_ data: JSON, locationType:GeneralConstants.EventSearch.LocationType, searchType:GeneralConstants.EventSearch.SearchType) {
        
        let count: Int? = data.array?.count
        
        let error = data["Error"].stringValue
        
        // No events returned
        if data["Error"] == 500 {
            print ("search event error: \(data["Message"])")
            SCLAlertView().showError("Search Error", subTitle: "Error: system is unavailable!")
            noDataFoundIsDisplayed = true
            self.clearSpinner()
        }
            // No events returned
        else if data.isEmpty || count == 0 {
            
            if !noDataFoundIsDisplayed {
                SCLAlertView().showError("Search Results", subTitle: "No events found!")
                noDataFoundIsDisplayed = true
            }
            self.clearSpinner()
            
        }
        else {
            
            // The user location was not showing even though the option was set to true so need to call to set sdk to update for some reason
            setShowUserLocation(locationType, searchType: searchType)
            
            // clear out previous markers
            if let eventCount = count {
                
                if eventCount > 0 {
                    mapView.removeAnnotations(mapView.annotations)
                }
            }
            
            var index = 0
            data.forEach() { _, event in
                index += 1

                let annotation = EventAnnotation(coordinate: CLLocationCoordinate2D(
                    latitude: event["lat"].doubleValue,longitude: event["lon"].doubleValue),
                                                 title:  event["title"].stringValue,
                                                 subtitle: event["category"].stringValue + " - Starts: " + event["date_start"].stringValue,
                                                 eventId: event["id"].stringValue)
                
                mapView.addAnnotation(annotation)
                
                if let eventCount = count {
                    
                    if (index >= eventCount) {
                        
                        self.zoomMapFitAnnotations()
                        mapView.showAnnotations(mapView.annotations, animated: true)
                        
                        self.clearSpinner()
                        
                    }
                }
                
                
            }
        }
    }
    
    func searchEventsFromDeviceLocation() {
        
        var searchZip = ""
        if let zip = deviceZipCode {
            searchZip = zip
        }
        else {
            searchZip = defaultZip
        }
        
        searchEvents(GeneralConstants.EventSearch.LocationType.current, searchType: GeneralConstants.EventSearch.SearchType.location, zipCode: searchZip,
                     fromDate: defaultFromDate, toDate: defaultToDate, eventName: nil, eventState: nil, raceType: nil, radius: defaultRadius)
        
    }
    
    func searchEvents(_ locationType:GeneralConstants.EventSearch.LocationType, searchType:GeneralConstants.EventSearch.SearchType, zipCode:String?, fromDate:Date?, toDate:Date?, eventName:String?, eventState:String?, raceType:GeneralConstants.EventSearch.RaceType?, radius:Int = GeneralConstants.EventSearch.defaultRadius) {
        
        print("EventSearchController ... searchEvents ... top")
        
        if !spinnerIsDisplayed {
            
            let style = SCLAlertView.SCLAppearance(showCloseButton: false)
            let alert = SCLAlertView(appearance: style)
        }
        
        // determine search type
        
        // search by location
        if searchType == GeneralConstants.EventSearch.SearchType.location {
            
            // search by device location or by zip
            if locationType == GeneralConstants.EventSearch.LocationType.current || locationType == GeneralConstants.EventSearch.LocationType.zip {
                
                guard (zipCode != nil && zipCode != "") else {
                    SCLAlertView().showError("Error", subTitle: "Zip code is blank!")
                    clearSpinner()
                    return
                }
                
                if let zip = zipCode {
                    
                    DataAgent.getEventsByZip (zip, withinRadius: radius, betweenDates:fromDate, and:toDate, raceType:raceType)
                    { (data: JSON) in
                        self.processSearchEventsResults(data, locationType: locationType, searchType: searchType)
                    }
                }
                
            }
                // search by state
            else if locationType == GeneralConstants.EventSearch.LocationType.state {
                
                if let state = eventState {
                    
                    DataAgent.getEventsByState (state, withinRadius: radius, betweenDates:fromDate, and:toDate, raceType:raceType)
                    { (data: JSON) in
                        self.processSearchEventsResults(data, locationType: locationType, searchType: searchType)
                    }
                }
                
            }
            
        }
            // search by name
        else if searchType == GeneralConstants.EventSearch.SearchType.name {
            
            if let name = eventName {
                DataAgent.getEventsByName (name)
                { (data: JSON) in
                    self.processSearchEventsResults(data, locationType: locationType, searchType: searchType)
                }
            }
        }
    }
    
    func getDeviceLocationInfo() -> (zip:String, location:CLLocation)? {
        
        if let zip = self.deviceZipCode, let location = self.deviceCurrentLocation {
            return(zip, location)
        }
        else {
            return nil
        }
    }
    
    func getDefaultDates() -> (fromDate:Date, toDate:Date)? {
        
        if let fromDate = self.defaultFromDate, let toDate = defaultToDate {
            return (fromDate, toDate)
        }
        else {
            return nil
        }
        
    }

    func lookupZipcode(_ location:CLLocation) {
        
        print("lookupZipCode top")
        
        CLGeocoder().reverseGeocodeLocation(location) { (placemarks, error) in
            
            if error != nil {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            
            if placemarks!.count > 0 {
                let pm = placemarks![0]
                
                let region = MKCoordinateRegion(
                    center: location.coordinate,
                    span:   MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
                )
                
                self.mapView?.setCenter(location.coordinate, animated: true)
                self.mapView?.setRegion(region, animated: true)
                
                print("zip: \(String(describing: pm.postalCode))")
                
                if let zipCode = pm.postalCode {
                    
                    self.deviceZipCode = zipCode
                    self.deviceCurrentLocation = location

                }
                
            }
            else {
                print("Problem with the data received from geocoder")
            }
            
        }
    }
    
    //
    // MARK: UI Utils
    //
    
    func zoomMapFitAnnotations() {
        
        print("eventsearchmgr ... zoomMapFitAnnotations ... top")
        
        var zoomRect = MKMapRectNull
        for annotation in (mapView.annotations) {
            
            let annotationPoint = MKMapPointForCoordinate(annotation.coordinate)
            
            print("eventsearchmgr ... zoomMapFitAnnotations ... annotationPoint, x: \(annotationPoint.x)")
            
            let pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0)
            
            if (MKMapRectIsNull(zoomRect)) {
                zoomRect = pointRect
            } else {
                zoomRect = MKMapRectUnion(zoomRect, pointRect)
            }
        }
        mapView.setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsetsMake(50, 50, 50, 50), animated: true)
        
    }
    
    func clearSpinner() {
        
        if spinnerIsDisplayed {
            
            if let alert = alertViewResponder {
                alert.close()
            }
        }
    }
    
    func setShowUserLocation(_ locationType:GeneralConstants.EventSearch.LocationType, searchType:GeneralConstants.EventSearch.SearchType) {
        
        if searchType == GeneralConstants.EventSearch.SearchType.location {
            
            if locationType == GeneralConstants.EventSearch.LocationType.current {
        
                mapView.showsUserLocation = true
            }
            else {
                mapView.showsUserLocation = false
            }
        }
        else {
            
            mapView.showsUserLocation = false
        }
    }
}
