//
//  TouchIDAuthentication.swift
//  TouchMeIn
//
//  Created by Marin Benčević on 21/04/2017.
//  Copyright © 2017 iT Guy Technologies. All rights reserved.
//

import Foundation
import LocalAuthentication

class TouchIDAuth {
    
    //
    // MARK: UI methods
    //
  
    let context = LAContext()
  
    func canEvaluatePolicy() -> Bool {
        return context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
    }
    
    //
    // MARK: Touch ID methods
    //
  
    func authenticateUser(completion: @escaping (String?) -> Void) {
    
        let authenticationContext = LAContext()
        var error:NSError?
    
        // Check if the device has a fingerprint sensor
        guard authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
            print("no biometrics detected")
            return
        }
    
        // Check the fingerprint
        authenticationContext.evaluatePolicy (
            .deviceOwnerAuthenticationWithBiometrics,
            localizedReason: "Logging in with Touch ID",
            reply: { [unowned self] (success, error) -> Void in
            
            if( success ) {
                // Fingerprint recognized
                print("touch id successful")
                DispatchQueue.main.async {
                    // User authenticated successfully, take appropriate action
                    completion(nil)
                }
            }else {
                // Check if there is an error
                if let error = error {
                    print("touch id not successful")
                    DispatchQueue.main.async {
                        completion(self.errorMessageForLAErrorCode(error as! LAError))
                    }
                }
            }
        })
    }
    
    func errorMessageForLAErrorCode(_ errorCode:LAError ) -> String {
        
        var message = ""
        
        switch(errorCode) {
        case LAError.authenticationFailed:
            message = "There was a problem verifying your identity."
        case LAError.appCancel:
            message = "Authentication was canceled by application."
        case LAError.userCancel:
            message = "Authentication was canceled by user."
        case LAError.userFallback:
            message = "The user tapped the fallback button."
        case LAError.systemCancel:
            message = "Authentication was canceled by system."
        case LAError.passcodeNotSet:
            message = "Passcode is not set on the device."
        case LAError.touchIDNotEnrolled:
            message = "Touch ID has no enrolled fingers."
        // iOS 9+ functions
        case LAError.invalidContext:
            message = "LAContext passed to this call has been previously invalidated."
        // MARK: IMPORTANT: There are more error states, take a look into the LAError struct
        default:
            message = "Touch ID may not be configured"
            break
        }
        
        return message
        
    }
}
