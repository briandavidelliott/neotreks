//
//  MapManager.swift
 //
// 
//
//  Created by Brian Elliott on 8/16/17.
//   //
// 
//

import Foundation
import MapKit
import CoreLocation

class MapManager:NSObject, CLLocationManagerDelegate {
    
    //
    // MARK: Properties
    //
    
    var locManager:CLLocationManager? = CLLocationManager()
    var currentLocation:CLLocation?
    var completionHandler:((CLLocation)-> ())?
    var didFindLocation:Bool = false
    
    override init() {
        
        super.init()

        locManager?.delegate = self
        locManager?.requestWhenInUseAuthorization()
        locManager?.stopMonitoringSignificantLocationChanges()
        locManager?.distanceFilter = kCLDistanceFilterNone
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if (status == .authorizedWhenInUse) {
            
            if let completion = completionHandler {
                getCurrentLocation(completion: completion)
            }
        }
    }
    
    func getCurrentLocation(completion: @escaping(CLLocation) -> ()) {

        completionHandler = completion
        
        if (CLLocationManager.authorizationStatus() == .authorizedWhenInUse) {
            
            locManager?.startUpdatingLocation()
 
        }
        else {
            
            locManager?.requestWhenInUseAuthorization()
            
        }
    }
    
    func isUserLocationsEnabled() -> Bool {
        
         if (CLLocationManager.authorizationStatus() == .authorizedWhenInUse) {
            return true
        }
         else {
            return false
        }
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        
        //                self.mapView.showAnnotations(self.mapView.annotations, animated: true)
        //                self.mapZoomUpdatedOnce = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        currentLocation = locations[0]
        print("didUpdateLocations currentLoc: lat: \(String(describing: currentLocation?.coordinate.latitude)), long:\(String(describing: currentLocation?.coordinate.longitude))")

        if let loc = currentLocation {
            
            if completionHandler != nil {
                currentLocation = loc
                print("current location: \(loc)")
                locManager?.stopUpdatingLocation()
                locManager = nil
                didFindLocation = true
            
                // call back to caller with current location
                completionHandler!(loc)
            
            }
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if didFindLocation {
            print("Location found **  but Location manager getting current location failed. Error: \(error)")
        }
        else {
            print("- Location manager getting current location failed. Error: \(error)")
        }
    }
}
