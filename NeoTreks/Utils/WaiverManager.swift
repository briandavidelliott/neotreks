//
//  WaiverManager.swift
 //
// 
//
//  Created by Brian Elliott on 2/22/18.
 //
// 
//

import Foundation
import SwiftyJSON

class WaiverManager {
    
    //
    // MARK: Properties
    //
    
    // Can't init is singleton
    private init() { }
    
    // MARK: Shared Instance
    
    static let shared = WaiverManager()
    
    // MARK: Local Variable
    
    var waivers:[Waiver] = []
    var waiverId:Int?
    
    //
    // MARK: Waiver methods
    //

    func checkForWaivers(_ storyboard:UIStoryboard?, nav:UINavigationController?, segueViewController controller:UIViewController?, viewNameForNonSegue viewName:String?, useSegue segueProcessing:Bool) {
    
        // make sure there are licenses
        if AppUtils.hasLicenses() {
        
            // check if wavier needs signing
            if AppUtils.needsWavierSigned() {
            
                // call CCN API to get waiver info to display
                DataAgent.getWaiverInfo(false)
                { (results:JSON) in
                    
                    // errors in getting waivers
                    if self.checkForWaiverErrors(results) {
                        SCLAlertView().showWarning("Waiver Error", subTitle: "Error in obtaining waiver information. Please contact support.")
                        self.goToViewProcessing(segueProcessing, controller:controller, storyboard:storyboard, nav:nav, viewName:viewName)
                        return
                    }
                    
                    // build waivers from JSON results
                    let returnFlag = self.processWaiverResults(results)
                    if returnFlag {
                        if segueProcessing {
                            if let vc = controller {
                                vc.performSegue(withIdentifier: "wavierSignOffSegue", sender: nil)
                            }
                        } else {
                            if let board = storyboard, let nav = nav {
                                AppUtils.navigateToViewController(board, navController:nav, viewName:"Waivers")
                            }
                        }
                        return
                    }
                    else {
                        // don't display waivers
                        self.goToViewProcessing(segueProcessing, controller:controller, storyboard:storyboard, nav:nav, viewName:viewName)
                        return
                    }
                }
            }
            else {
                goToViewProcessing(segueProcessing, controller:controller, storyboard:storyboard, nav:nav, viewName:viewName)
                return
            }
        }
        else {
            goToViewProcessing(segueProcessing, controller:controller, storyboard:storyboard, nav:nav, viewName:viewName)
            return
        }
    }
    
    func goToViewProcessing(_ segueProcessing:Bool, controller:UIViewController?, storyboard:UIStoryboard?, nav:UINavigationController?, viewName:String?) {
        
        if segueProcessing {
            if let vc = controller {
                AppUtils.navigateToView(vc, includeErrorPath: false)
            }
        }
        else {
            if let board = storyboard, let nav = nav, let name = viewName {
                AppUtils.navigateToViewController(board, navController:nav, viewName:name)
            }
        }
    }

    func processWaiverResults(_ data: JSON) -> Bool {
    
        // No results returned
        if data["Error"] == 500 {
            SCLAlertView().showError("Search Error", subTitle: "Error: Could not retreive waiver data!")
            return false
        }
    
        // Look for Junior waviers
        let (isJuniorWavier, parentName, parentEmail) = AppUtils.hasJuniorWaiver(data)
        if isJuniorWavier {
        
            var fullName = " "
        
            if let member = DataAgent.getMemberProfile() {
                fullName = member.full_name
            }
        
            let alert = SCLAlertView()
        
            let htmlHeaderString = "<html>" +
                "<head>" +
                "<style>" +
                "body {" +
                "background-color: rgb(177, 177, 177);" +
                "font-family: 'Arial'; ;font-size: 16px;" +
                "text-decoration:none;" +
                "}" +
                "</style>" +
            "</head>"
        
            var sendToString = "\(parentName) at the email address: \(parentEmail)"
            if parentName.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                sendToString = "the email address: \(parentEmail)"
            }
        
            let bodyString = "Your Parent or Legal Guardian needs to sign the Membership Waiver. This waiver must be completed prior to competing in any sanctioned events. This waiver has already been sent to \(sendToString). If you have questions or need the waiver sent to a different email, you may contact our membership services team at <a href=\"" + GeneralConstants.Waivers.membershipContactEmailAddress + "\">" +
                GeneralConstants.Waivers.membershipContactEmailDisplay +
                "</a> or call <a href=\"" + GeneralConstants.Waivers.membershipContactPhoneNumber + " \">" +
                GeneralConstants.Waivers.membershipContactPhoneNumberDislay + " .</a>"
        
            let htmlString = htmlHeaderString +
                "<body>" + bodyString + "</body></head></html>"
        
            let title = "License for \(fullName) is Pended"
        
            let htmlData = NSString(string: htmlString).data(using: String.Encoding.unicode.rawValue)
        
            let attributedString = try! NSAttributedString(data: htmlData!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
        
            alert.showHTMLInfo(title, subTitle: attributedString)
        
            return false
        }
    
        var successFlag = false
    
        self.createWaivers (data)
            { (status: Bool) in
                if status {
                    successFlag = true
                }
                else {
                    successFlag = false
                }
            }
    
        return successFlag
    }

    func checkForWaiverErrors(_ data:JSON) -> Bool {
    
        if data["Error"] == 500 {
            return true
        }
        else {
            return false
        }
    }

    func createWaivers(_ data:JSON, completion: @escaping (_ results: Bool) -> Void) {
    
        // clean out any previous values
        waivers.removeAll()
    
        waiverId = AppUtils.getWaiverId(data)
    
        // waiver id needs to be defined to make the approval return call
        if waiverId != nil {
        
            for item in data.arrayValue {
            
                var blockCount = 0
            
                for contentBlock in item["waiver_content_blocks"].arrayValue {
                
                    blockCount += 1
                
                    let contentString = contentBlock["content"].stringValue
                    let confirmLabelString = contentBlock["confirm_label"].stringValue
                
                    // if both populated
                    if !contentString.isEmpty && !confirmLabelString.isEmpty {
                        let waiver:Waiver = Waiver(title: nil, content: contentString, consent: confirmLabelString)
                        waivers.append(waiver)
                    }
                    // if only confirmation populated
                    else if contentString.isEmpty && !confirmLabelString.isEmpty {
                    
                        let waiver:Waiver = Waiver(title: nil, content: contentString, consent: confirmLabelString)
                        waivers.append(waiver)
                    }
                }
            }
        
            completion(true)
        }
        // waiver id not found - return error
        else {
            completion(false)
        }
    
    }
}
