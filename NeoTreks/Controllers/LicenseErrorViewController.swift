//
//  LicenseErrorViewController.swift
// 
//
//  Created by Brian Elliott on 9/12/17.
// 
//

import UIKit

class LicenseErrorViewController: UIViewController, NavBarBackProtocol {

    @IBOutlet weak var logoutButton: UIBarButtonItem!
    @IBOutlet weak var licenseYearLabel: UILabel!
    @IBOutlet weak var greetingLabel: UILabel!
    @IBOutlet weak var viewOptionsButton: UIButton!
    private var overlay: LoadingOverlay!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("LicenseErrorViewController viewDidLoad top")

        if !GeneralConstants.FeatureToggles.licensesOnlyFlag {
            if let nav = appDelegate.navController {
                AppUtils.addNavBar("Home", viewController:self, navigationController:nav, delegate:self)
                navigationController?.navigationBar.isTranslucent = false
            }
        }
        else {
            if let nav = appDelegate.navController {
                nav.navigationBar.isHidden = true
            }
        }
        
        logoutButton.setTitleTextAttributes([
            NSFontAttributeName : UIFont(name: "AlternateGothicNo2BT-Regular", size: 26)!,
            NSForegroundColorAttributeName : UIColor.white,
            ], for: .normal)
        
        viewOptionsButton.setTitleColor(AppColors.HomeButtonColors.highlightColor, for: .highlighted)
        
        overlay = LoadingOverlay(addToView: view)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(syncCompleted),
                                               name: NSNotification.Name("SyncComplete"),
                                               object: nil)
        
        if (UIApplication.shared.delegate as! AppDelegate).syncComplete {
            syncCompleted()
        } else {
            overlay.show()
        }

    }
    
    func backPicked() {
        _ = navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func logoutPicked(_ sender: Any) {
        
        DataAgent.reset()
        _ = navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func viewOptionsPicked(_ sender: Any) {
        
        guard let url = URL(string: NetworkingConstants.licenseErrorViewOptionsUrl) else
        {
            return
        }
        
        UIApplication.shared.openURL(url)
    }
    
    func syncCompleted() {
        
        licenseYearLabel.isHidden = true
        
        // only update if member data exists
        // note: there is a condition where if this view controller is displayed and if app is backgrounded
        //       and then foregrounded, the app delegate will call the API to update the licenses but the refresh
        //       token fails, and the member data is reset to nil, and then syncCompleted is set to true, then
        //       this self syncCompleted() method is called but the error message is not relevant because the
        //       refresh session error is displayed and a "could not find member data" error is misleading.
        //       But the syncCompleted is still needed to make sure the spinner view is taken down in the
        //       error condition.
        
        if let member: Member = DataAgent.getMemberProfile() {
            
//            let curDate = Calendar.current.dateComponents([.year], from: Date())
//            licenseYearLabel.text = String(curDate.year!)
            greetingLabel.text = "Hi " + member.first_name + ","
        }
        
        overlay.hide()
    }
    
    override open var shouldAutorotate: Bool {
        return false
    }
}
