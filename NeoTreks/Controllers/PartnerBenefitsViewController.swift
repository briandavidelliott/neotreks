//
//  PartnerBenefitsViewController.swift
// 
//
//  Created by Brian Elliott on 7/25/17.
// 
//

import UIKit

class PartnerBenefitsViewController: UIViewController, UIPopoverPresentationControllerDelegate {

    // MARK: Instance properties
    
    @IBOutlet weak var detailsTextView: UITextView!
    @IBOutlet weak var detailsTextViewHeightConstraint: NSLayoutConstraint!

    public var parentNavController:UINavigationController?
    
    // MARK: Lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()

        detailsTextView.transform = CGAffineTransform(scaleX: 0, y: 0);
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // MARK: - Navigation
    @IBAction func startPicked(_ sender: Any) {
        if ((parentNavController) != nil) {

            dismiss(animated: true, completion: nil)
            
        }
    }


}
