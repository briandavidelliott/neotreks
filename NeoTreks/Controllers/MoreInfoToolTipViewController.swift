//
//  MoreInfoToolTipViewController.swift
// 
//
//  Created by Brian Elliott on 12/12/17.
// 
//

import UIKit

class MoreInfoToolTipViewController: UIViewController, UIPopoverPresentationControllerDelegate  {

    @IBOutlet weak var messageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = AppColors.RiderLookupColors.moreRiderInfoPopupBackgroundFontColor
        self.popoverPresentationController?.backgroundColor = AppColors.RiderLookupColors.moreRiderInfoPopupBackgroundFontColor
    }
    
}
