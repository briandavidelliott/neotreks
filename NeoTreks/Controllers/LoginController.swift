//
//  LoginController.swift
//
//  Brian Elliott revisions based on original by Patrik DeLoughry 
// 
//


import Alamofire
import Kingfisher
import SwiftyJSON
import UIKit
import UserNotifications

// Keychain Configuration
struct KeychainConfiguration {
    static let serviceName = "Unplugged-iOS"
    static let accessGroup: String? = nil
}

class LoginController: UIViewController {
    
    @IBOutlet weak var imgLogo:       UIImageView!
    @IBOutlet weak var txtUsername:   UITextField!
    @IBOutlet weak var txtPassword:   UITextField!
    @IBOutlet weak var btnLogin:      UIButton!
    @IBOutlet weak var btnForgot:     UIButton!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var touchIdButton: UIButton!
    
    let touchIdHandler = TouchIDAuth()
    var waivers:[Waiver] = []
    var waiverId:Int?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Set the last valid username as default
        txtUsername.text = UserDefaults.standard.value(forKey: "username") as? String
        
        touchIdButton.isHidden = !touchIdHandler.canEvaluatePolicy()
        
        // Download (and cache) the login background image
        if DataAgent.getReachabilityStatus() != .notReachable {
            let url = URL(string: "https://s3.amazonaws.com/static/images/LoginBg-1200x1200.png")
            imgBackground.kf.setImage(with: url)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if sender == nil {
            return true
        }
        
        login()
        return false
    }
    
    private func login() {
        
        // Hide the keyboard
        if txtUsername.isEditing {
            txtUsername.endEditing(true)
        } else if txtPassword.isEditing {
            txtPassword.endEditing(true)
        }
        
        // Do we have an internet connection?
        if DataAgent.getReachabilityStatus() == .notReachable {
            SCLAlertView().showError("Network Error", subTitle: "No Internet Connection")
            return
        }
        
        // Is username blank?
        if let userName = txtUsername.text {
            if userName.isEmpty {
                SCLAlertView().showError("Login Error", subTitle: "Username is required")
                return
            }
        }
        
        // Is password blank?
        if let password = txtPassword.text {
            if password.isEmpty {
                SCLAlertView().showError("Login Error", subTitle: "Password is required")
                return
            }
        }
        
        let style = SCLAlertView.SCLAppearance(showCloseButton: false)
        
        // Show login progress popup
        let alert   = SCLAlertView(appearance: style)
        let wndWait = alert.showWait("Login", subTitle: "Logging into NeoTreks", colorStyle: 0x041E42)
        
        // Attempt to authenticate with username and password
        if let userName = txtUsername.text, let password = txtPassword.text {
            
            DataAgent.authenticate(userRole: .Competitor, withUsername: userName, andPassword: password) {
                (_ returnCode: DataAgent.APIReturnValues) in
            
                if returnCode == DataAgent.APIReturnValues.Success {
                    
                    // Store the username as default
                    UserDefaults.standard.set(self.txtUsername.text!, forKey: "username")
                    UserDefaults.standard.synchronize()
                
                    // Register the device for push notifications
                    self.registerPushDevice()
                
                    // Download the member data
                    DataAgent.updateMemberData() { (_ success: Bool) in
                    
                        // Set data sync to complete
                        self.appDelegate.syncComplete = true
                        wndWait.close()
                        
                        if self.touchIdHandler.canEvaluatePolicy() {
                            self.confirmTouchId()
                        }
                        else {
                            self.routeToView()
                        }
                    
                        // Notify the next view that the member data has been loaded
                        NotificationCenter.default.post(name: NSNotification.Name("SyncComplete"), object: success)
                    }
                
                } else {
                    
                    wndWait.close()
                    
                    if returnCode == DataAgent.APIReturnValues.SuccessButNoDataFound ||
                        returnCode == DataAgent.APIReturnValues.InvalidUserNameOrPassword {
                        
                        SCLAlertView().showError("Login Error", subTitle: "Invalid username or password")
                    }
                    else if returnCode == DataAgent.APIReturnValues.NetworkError {
                        
                        SCLAlertView().showError("Network Error", subTitle: "Network error. Please try again.")
                    }
                    else if returnCode == DataAgent.APIReturnValues.APIDown {
                        
                        SCLAlertView().showError("Server Error", subTitle: "Server error. Please try again.")
                    }
                    else {
                        
                        SCLAlertView().showError("Service Error", subTitle: "Service error. Please try again.")
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "wavierSignOffSegue" {
            
            let sharedWavierManager = WaiverManager.shared
            let vc = segue.destination as? WaiverViewController
            vc?.waivers = sharedWavierManager.waivers
            vc?.waiverId = sharedWavierManager.waiverId
        }
    }
    
    func routeToView() {
        
        if GeneralConstants.FeatureToggles.licensesOnlyFlag {
            
            if AppUtils.isLookupUser() {
                // Trigger login segue (to main/home menu)
                self.performSegue(withIdentifier: "LoginSegue", sender: nil)
            }
            else {
                // still in license features toggle mode for non-officals
                if AppUtils.hasLicenses() {

                    // check if wavier needs signing and handle routing
                    let sharedWavierManager = WaiverManager.shared
                    // if false, process without displaying waivers, else let waiver manager handle it
                    sharedWavierManager.checkForWaivers(nil, nav:nil, segueViewController:self, viewNameForNonSegue:nil, useSegue: true)
                    return
                }
                else {
                    self.performSegue(withIdentifier: "LicenseErrorSegue", sender: nil)
                }
            }
        }
        else {
            // Trigger login segue (to main/home menu)
            self.performSegue(withIdentifier: "LoginSegue", sender: nil)
        }
        
    }
    
    @IBAction func forgotPwrd(_ sender: Any?) {
       forgotPasswordAndClearTouchId()
    }
    
    private func registerPushDevice() {
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            UIApplication.shared.registerForRemoteNotifications()
        }
        else if #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        else {
            UIApplication.shared.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
        }
    }
    
    override open var shouldAutorotate: Bool {
        return false
    }
    
    @IBAction func touchIdPicked(_ sender: Any) {
        
        if UserDefaults.standard.bool(forKey: "hasTouchIdLoginSet") {
        
            touchIdHandler.authenticateUser() { message in
                
                if let message = message {
                    // if the completion is not nil show an alert
                    SCLAlertView().showError("Error", subTitle: message)
                } else {
                    
                    // touch id successful
                    if DataAgent.getReachabilityStatus() == .notReachable {
                        DataAgent.unserializeFromDisk()
                        self.routeToView()
                    }
                    // if internet available, login
                    else {
 
                        if let username = UserDefaults.standard.value(forKey: "touchid-username") as? String {

                            do {
                                let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                                    account: username,
                                                                    accessGroup: KeychainConfiguration.accessGroup)
                                let keychainPassword = try passwordItem.readPassword()
                                self.touchIdLogin(username, password:keychainPassword)
                            }
                            catch {
                                SCLAlertView().showWarning("Touch ID Error", subTitle: "Error reading password from keychain - \(error)")
                            }
                        }
                    }
                }
            }
        }
        else {
            SCLAlertView().showWarning("Touch ID", subTitle: "To use Touch ID you must login initially and then register your Touch ID.")
        }
    }
    
    func touchIdLogin(_ userName:String, password:String) {
        
        let style = SCLAlertView.SCLAppearance(showCloseButton: false)
        
        // Show login progress popup
        let alert   = SCLAlertView(appearance: style)
        let wndWait = alert.showWait("Login", subTitle: "Logging into NeoTreks", colorStyle: 0x041E42)
        
        DataAgent.authenticate(userRole: .Competitor, withUsername: userName, andPassword: password) {
            (_ returnCode: DataAgent.APIReturnValues) in
            
             if returnCode == DataAgent.APIReturnValues.Success {
                
                // Download the member data
                DataAgent.updateMemberData() { (_ success: Bool) in
                    
                    // Set data sync to complete
                    self.appDelegate.syncComplete = true
                    wndWait.close()
                    self.routeToView()
                }
                
            } else {
                
                wndWait.close()
                
                if returnCode == DataAgent.APIReturnValues.SuccessButNoDataFound ||
                    returnCode == DataAgent.APIReturnValues.InvalidUserNameOrPassword {
                    
                    SCLAlertView().showError("Login Error", subTitle: "Invalid username or password")
                }
                else if returnCode == DataAgent.APIReturnValues.NetworkError {
                    
                    SCLAlertView().showError("Network Error", subTitle: "Network error. Please try again.")
                }
                else if returnCode == DataAgent.APIReturnValues.APIDown {
                    
                    SCLAlertView().showError("Server Error", subTitle: "Server error. Please try again.")
                }
                else {
                    
                    SCLAlertView().showError("Service Error", subTitle: "Service error. Please try again.")
                }
                                
            }
        }
    }
    
    func confirmTouchId() {
        
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false,
            showCircularIcon: true
        )
        let alertView = SCLAlertView(appearance: appearance)
        alertView.addButton("Yes") {
            self.storeTouchIdInfo()
            self.routeToView()
        }
        alertView.addButton("No") {
            self.routeToView()
        }
        
        let alertViewIcon = UIImage(named: "TouchIcon")
        alertView.showInfo("Register Touch ID", subTitle: "Do you wish to register your Touch ID with the current user?", circleIconImage: alertViewIcon)
        
    }
    
    func forgotPasswordAndClearTouchId() {
        
        // if touch id available and has been registered
        if touchIdHandler.canEvaluatePolicy() && UserDefaults.standard.bool(forKey: "hasTouchIdLoginSet") {
        
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false,
                showCircularIcon: true
            )
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton("Ok") {
                UserDefaults.standard.set(false, forKey: "hasTouchIdLoginSet")
                UserDefaults.standard.set(nil, forKey: "key")
                UserDefaults.standard.synchronize()
                UIApplication.shared.openURL(URL(string: "https://www.unpluggedsystems.com/pwreset.php")!)
            }
            alertView.addButton("Cancel") {
                return
            }
        
            let alertViewIcon = UIImage(named: "TouchIcon")
            alertView.showWarning("Clear Touch ID", subTitle: "Attempting to your reset your password will clear the registration of your Touch ID with the current user. Pick Ok to proceed and or Cancel.", circleIconImage: alertViewIcon)
        }
        else {
            UIApplication.shared.openURL(URL(string: "https://www.unpluggedsystems.com/pwreset.php")!)
        }
        
    }
    
    func storeTouchIdInfo() {
        
        // store password in keychain and user name store in user defaults previously
        if let userName = txtUsername.text, let password = txtPassword.text {
            if !userName.isEmpty && !password.isEmpty {
                do {
            
                    let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                    account: userName,
                                                    accessGroup: KeychainConfiguration.accessGroup)
            
                    // Save the password for the new item.
                    try passwordItem.savePassword(password)
            
                    UserDefaults.standard.set(true, forKey: "hasTouchIdLoginSet")
                    UserDefaults.standard.synchronize()
                    
                    UserDefaults.standard.set(userName, forKey: "touchid-username")
                    UserDefaults.standard.synchronize()
                
                } catch {
                    SCLAlertView().showError("Login Error", subTitle: "Error storing touch id credentials: - \(error)")
                }
            }
        }
    }
    
}

