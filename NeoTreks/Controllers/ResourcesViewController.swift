//
//  ResourcesViewController.swift
// 
//
//  Created by Brian Elliott on 7/25/17.
// 
//

import UIKit

class ResourcesViewController: UITableViewController, NavBarBackProtocol {
    
    // MARK: Instance properties
    
    public var parentNavController:UINavigationController?

    // MARK: - Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let nav = appDelegate.navController {
            AppUtils.addNavBar("Home", viewController:self, navigationController:nav, delegate:self)
        }

    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let row = indexPath.row
        let section = indexPath.section
        print("row: \(row)")
        print("section: \(section)")
        
        if let url = URL(string: "http://www.unpluggedsystems.com/benefits/?l=categories") {
            UIApplication.shared.openURL(url)
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
    func backPicked() {
        _ = navigationController?.popViewController(animated: true)
    }

}
