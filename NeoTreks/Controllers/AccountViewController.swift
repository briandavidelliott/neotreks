//
//  AccountViewController.swift
// 
//
//  Created by Brian Elliott on 7/25/17.
// 
//

import UIKit

class AccountViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    

    var valuesDictionary:[GeneralConstants.AccountViewNames:String] = [:]
    var keysArray:[GeneralConstants.AccountViewNames] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Data loading
    private func loadData() {
        
        if let member = DataAgent.getMemberProfile() {
            
            valuesDictionary = AppUtils.setAccountInfo(member)
        }
        else {
            SCLAlertView().showError("Error", subTitle: "Could not find member data (3)!")
        }
        
        // gets rid of separator lines on table view
        self.tableView.tableFooterView = UIView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // A bug - see: https://www.appcoda.com/self-sizing-cells/
    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 20
        
        loadData()
        
        var tempKeysArray:[GeneralConstants.AccountViewNames] = []
        
        for key in valuesDictionary.keys {
            tempKeysArray.append(key)
        }
        
        // sort in display order
        let unsortDict = GeneralConstants.accountViewNamesDisplayOrder
        tempKeysArray = Array(unsortDict.keys).sorted(by: {unsortDict[$0]! < unsortDict[$1]!})
    
        for key in tempKeysArray {
            if let isOn = GeneralConstants.accountViewNamesDisplayOn[key] {
                if isOn {
                    keysArray.append(key)
                }
            }
        }

    }
    
    func indexToKeyLookup(_ index:Int) -> String {
        
        var count = -1
        for _ in keysArray {
            count += 1
            if count == index {
                return keysArray[count].rawValue
            }
        }
        
        return ""
    }
    
    func indexToKeyLookup(_ index:Int) -> GeneralConstants.AccountViewNames {
        
        var count = -1
        for key in keysArray {
            count += 1
            if count == index {
                return key
            }
        }
        
        return GeneralConstants.AccountViewNames.Name
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return keysArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AccountCell", for: indexPath) as? AccountTableViewCell else {
            fatalError("The dequeued cell is not an instance of AccountTableViewCell")
        }
        
        cell.nameField.text = indexToKeyLookup(indexPath.row) + ":"
        if let value = valuesDictionary[indexToKeyLookup(indexPath.row)] {
            cell.valueField.text = value
        }
        
        return cell
    }
    
    @IBAction func updateAccountPicked(_ sender: Any) {
        
        if let member = DataAgent.getMemberProfile() {
            
            let baseAddress = NetworkingConstants.productionUrl
            let address = NetworkingConstants.updateAccountUrl + "&compid=\(member.comp_id)"
            let finalAddress = baseAddress + address
                
            if let url = URL(string: finalAddress) {
                UIApplication.shared.openURL(url)
            }

        }

    }

}
