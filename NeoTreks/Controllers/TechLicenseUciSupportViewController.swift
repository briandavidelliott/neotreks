//
//  TechLicenseUciSupportViewController.swift
// 
//
//  Created by Brian Elliott on 11/15/17.
// 
//

import UIKit

class TechLicenseUciSupportViewController: UIViewController {
    
    @IBOutlet weak var proTeamNameStackView: UIStackView!
    @IBOutlet weak var proTeamNameLabel: UILabel!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var genericStackView: UIStackView!
    @IBOutlet weak var licenseTypeTitleLabel: UILabel!
    @IBOutlet weak var subtitleLicenseLabel: UILabel!
    @IBOutlet weak var suspendedOrPendingImageView: UIImageView!
    
    @IBOutlet weak var uciRolesLabel: UILabel!
    @IBOutlet weak var topLevelStackView: UIStackView!
    
    public var license:License?
    
    private lazy var popupBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = GeneralConstants.RiderLookup.defaultCornerRadius
        return view
    }()
    
    private func pinBackground(_ view: UIView, to stackView: UIStackView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        stackView.insertSubview(view, at: 0)
        view.pin(to: stackView)
    }
    
    private lazy var rolesBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = AppColors.RiderLookupColors.sectionBackgroundColor
        view.layer.cornerRadius = GeneralConstants.RiderLookup.defaultCornerRadius
        return view
    }()
    
    private lazy var teamBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = AppColors.RiderLookupColors.sectionBackgroundColor
        view.layer.cornerRadius = GeneralConstants.RiderLookup.defaultCornerRadius
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        pinBackground(popupBackgroundView, to: topLevelStackView)
        topLevelStackView.layoutMargins = UIEdgeInsets(top: GeneralConstants.RiderLookup.popupMargin, left: GeneralConstants.RiderLookup.popupMargin, bottom: GeneralConstants.RiderLookup.popupMargin, right: GeneralConstants.RiderLookup.popupMargin)
        topLevelStackView.isLayoutMarginsRelativeArrangement = true
        
        popupView.layer.cornerRadius = 10.0
        popupView.layer.borderWidth = 4.0
        popupView.layer.borderColor = (AppColors.RiderLookupColors.borderColor).cgColor
        popupView.layer.masksToBounds = true
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(popupViewPicked(_:)))
        singleTap.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(singleTap)
        
        AppUtils.pinBackground(rolesBackgroundView, to: genericStackView)
        genericStackView.layoutMargins = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        genericStackView.isLayoutMarginsRelativeArrangement = true
        
        AppUtils.pinBackground(teamBackgroundView, to: proTeamNameStackView)
        proTeamNameStackView.layoutMargins = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        proTeamNameStackView.isLayoutMarginsRelativeArrangement = true
        
        // Testing code
//                            let testDict: [String:String] = [
//                                "CHAUFFEUR" : "true",
//                                "MECHANIC" : "true",
//                                "DRIVER" : "true",
//                                "DIRECTOR_SPORTIF_ADJOINT" : "true",
//                                "SOIGNEUR_TRAINER" : "true",
//                                "PARAMEDIC" : "true"
//                            ]

        if let license = self.license {
            
            setLicenseStatusImage()
            
            licenseTypeTitleLabel.adjustsFontSizeToFitWidth = true
            licenseTypeTitleLabel.adjustsFontSizeToFitWidth = true
            
            if let extra = license.extra {
                proTeamNameLabel.text = extra["pro_team_name"]
            }
            
            if let extra = license.extra {
            
                // for testing - let extra = testDict
            
                let sortedKeys = (extra.keys).sorted{ $0.lowercased() < $1.lowercased() }
                let totalKeys = sortedKeys.count
                var count = 0
                var rolesString = ""
                for key in sortedKeys {
                    count += 1
                    if key.contains("pro_team_name") {
                        continue
                    }
                    if let value = extra[key] {
                        if value == "true" {
                            let newString = key.replacingOccurrences(of: "_", with: " ")
                            rolesString += newString
                            if count != totalKeys {
                                rolesString += "\n"
                            }
                        }
                    }
                }
                uciRolesLabel.text = rolesString
            }
        }
    }
    
    func setLicenseStatusImage() {
        
        if let licenseStatus = license?.license_status {
            
            // Check if license is suspended
            if licenseStatus.uppercased() == "S" {
                suspendedOrPendingImageView.isHidden = false
                let image = UIImage(named: "Suspended")
                suspendedOrPendingImageView.image = image
            }
                // Check if license is pending
            else if licenseStatus.uppercased() == "P" {
                suspendedOrPendingImageView.isHidden = false
                let image = UIImage(named: "Pending")
                suspendedOrPendingImageView.image = image
            }
            else if let expirationDate = license?.expiration_date {
                if AppUtils.isDateExpired(expirationDate) {
                    suspendedOrPendingImageView.isHidden = false
                    let image = UIImage(named: "Expired")
                    suspendedOrPendingImageView.image = image
                }
                else {
                    suspendedOrPendingImageView.isHidden = true
                }
            }
            else {
                suspendedOrPendingImageView.isHidden = true
            }
        }
        else {
            suspendedOrPendingImageView.isHidden = true
        }
    }
    
    @IBAction func xPicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closePicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func popupViewPicked(_ touch: UITapGestureRecognizer) {
        
        let touchPoint = touch.location(in: self.view)
        let popupRect = self.view.convert(self.popupView.frame, from: self.view)
        
        if !popupRect.contains(touchPoint) {
            dismiss(animated: true, completion: nil)
        }
    }

}
