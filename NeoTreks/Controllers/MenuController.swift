//
//  MenuController.swift
//
//

import UIKit


class MenuController: UIViewController {
    
    @IBOutlet weak var lblWelcome: UILabel!
    @IBOutlet weak var btnLicense: UIButton!
    @IBOutlet weak var btnResults: IconButton!
    @IBOutlet weak var btnEvents: UIButton!
    @IBOutlet weak var btnAccount: UIButton!
    @IBOutlet weak var btnBenefits: IconButton!
    @IBOutlet weak var btnRegulations: IconButton!
    @IBOutlet weak var btnRiderLookup: IconButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewContainer: UIView!
    @IBOutlet weak var versionLabel: UILabel!

    private var overlay: LoadingOverlay!
    var network:NetworkManager?
    
    public var parentNavController:UINavigationController?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        versionLabel.text = AppUtils.appVersion()
        
        if let image = UIImage(named: "LinkArrow") {
            
            btnResults.addLeadingImage(image)
            btnResults.setImageSize(size: CGSize(width: GeneralConstants.iconButton.iconButtonImageHeightWidth, height: GeneralConstants.iconButton.iconButtonImageHeightWidth))
            
        }
        
        overlay = LoadingOverlay(addToView: view)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(syncCompleted),
                                               name: NSNotification.Name("SyncComplete"),
                                               object: nil)
        
        if (UIApplication.shared.delegate as! AppDelegate).syncComplete {
            syncCompleted()
             overlay.hide()
        } else {
            overlay.show()
        }
        
        network = NetworkManager()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.specialInfoPicked(_:)))
        tapGesture.numberOfTapsRequired = 5;
        versionLabel.addGestureRecognizer(tapGesture)
        
        self.navigationItem.title = "Home"
        
        btnLicense.setTitleColor(AppColors.HomeButtonColors.highlightColor, for: .highlighted)
        btnResults.setTitleColor(AppColors.HomeButtonColors.highlightColor, for: .highlighted)
        btnAccount.setTitleColor(AppColors.HomeButtonColors.highlightColor, for: .highlighted)
        btnEvents.setTitleColor(AppColors.HomeButtonColors.highlightColor, for: .highlighted)
        btnRiderLookup.setTitleColor(AppColors.HomeButtonColors.highlightColor, for: .highlighted)
        
        setFeatureToggledHomeButtonOptions()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (UIApplication.shared.delegate as! AppDelegate).syncComplete == false {
            overlay.show()
        }
        
    }
    
    func syncCompleted() {
        
        var welcomeMessage = "Welcome"
        if let firstName = DataAgent.getMemberProfile()?.first_name {
            welcomeMessage += ", \(firstName)"
        }
        else {
            welcomeMessage += "!"
        }
        lblWelcome.text = welcomeMessage
        
        if AppUtils.hasLicenses() {
            
            btnLicense.isEnabled = true
            btnLicense.alpha     = 1.0
        }
        else {
            btnLicense.isEnabled = false
            btnLicense.alpha     = 0.5
        }
        overlay.hide()
    }
    
    @IBAction func showResults(_ sender: Any?) {
        if let compID = DataAgent.getMemberProfile()?.comp_id {
            UIApplication.shared.openURL(URL(string: "https://www.unpluggedsystems.com/results/index.php?compid=\(compID)&all=1&access_token=\(DataAgent.getAccessToken())")!)
        }
    }
    
    @IBAction func logOut(_ sender: Any?) {
        DataAgent.reset()
        _ = navigationController?.popViewController(animated: true)
    }
    
    func showViewController(_ name:String) {
        
        if let nav = appDelegate.navController {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier:name)
            
            nav.pushViewController(viewController, animated: true)
            
        }
    }
    
    //
    // MARK: Home utilities
    //
    
    func specialInfoPicked(_ sender: UIButton) {

        DataAgent.refreshToken() { (success: DataAgent.APIReturnValues) in
            if success == DataAgent.APIReturnValues.Success {
                print("success")
            } else {
                 print("not success")
            }
        }
        
        // test
        
        if let current = network?.apiUrl {
            
            let currentString = "Current \(current)"
            let optionMenuController = UIAlertController(title: "Environment", message: currentString, preferredStyle: .actionSheet)

            let cancelAction = UIAlertAction(title: "Done", style: .cancel, handler: {
                (alert: UIAlertAction!) -> Void in
            })
        
            optionMenuController.addAction(cancelAction)
        
            // Present UIAlertController with Action Sheet
        
            self.present(optionMenuController, animated: true, completion: nil)
        }
    }
    
    func setFeatureToggledHomeButtonOptions() {
        
        if GeneralConstants.FeatureToggles.officialsVersionFlag {
            
            btnLicense.isHidden = false
            btnResults.isHidden = true
            btnAccount.isHidden = true
            btnEvents.isHidden = true
            btnBenefits.isHidden = true
            btnRegulations.isHidden = true
            btnRiderLookup.isHidden = false
        }
        else {
            
            btnLicense.isHidden = false
            btnResults.isHidden = false
            btnAccount.isHidden = false
            btnEvents.isHidden = false
            btnBenefits.isHidden = false
            btnRegulations.isHidden = false
            btnRiderLookup.isHidden = false
        }
        
    }
    
}
