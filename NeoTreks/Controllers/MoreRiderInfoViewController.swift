//
//  MoreRiderInfoViewController.swift
// 
//
//  Created by Brian Elliott on 12/11/17.
// 
//

import UIKit

class MoreRiderInfoViewController: UIViewController, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var homePhoneLabel: UILabel!
    @IBOutlet weak var workPhoneLabel: UILabel!
    @IBOutlet weak var topStackView: UIStackView!
    
    var yOffset:CGFloat = 0.0
    var emailAddress = ""
    var homePhone = ""
    var workPhone = ""
    
    private lazy var popupBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 5.0
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailLabel.text = emailAddress
        emailLabel.adjustsFontSizeToFitWidth = true
        homePhoneLabel.text = homePhone
        homePhoneLabel.adjustsFontSizeToFitWidth = true
        workPhoneLabel.text = workPhone
        workPhoneLabel.adjustsFontSizeToFitWidth = true
        
        AppUtils.pinBackground(popupBackgroundView, to: topStackView)
        topStackView.layoutMargins = UIEdgeInsets(top: GeneralConstants.RiderLookup.popupMargin, left: GeneralConstants.RiderLookup.popupMargin, bottom: GeneralConstants.RiderLookup.popupMargin, right: GeneralConstants.RiderLookup.popupMargin)
        topStackView.isLayoutMarginsRelativeArrangement = true
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(popupViewPicked(_:)))
        singleTap.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(singleTap)
        
        emailLabel.adjustsFontSizeToFitWidth = true

    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.modalPresentationStyle = .custom
        self.transitioningDelegate = self
        
    }
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        
        return PopupPresentation(presentedViewController: presented, presenting: presenting, delta:yOffset)
        
    }
    
    @IBAction func closePicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func popupViewPicked(_ touch: UITapGestureRecognizer) {
        
        let touchPoint = touch.location(in: self.view)
        let popupRect = self.view.convert(self.topStackView.frame, from: self.view)
        
        if !popupRect.contains(touchPoint) {
            dismiss(animated: true, completion: nil)
        }
    }
    
}
