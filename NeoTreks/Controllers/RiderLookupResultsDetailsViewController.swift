//
//  RiderLookupResultsDetailsViewController.swift
// 
//
//  Created by Brian Elliott on 10/9/17.
// 
//

import UIKit

class RiderLookupResultsDetailsViewController: UIViewController, NavBarBackProtocol, UITableViewDelegate, UITableViewDataSource, UIPopoverPresentationControllerDelegate {
    
    @IBOutlet weak var topContainerView: UIView!
    @IBOutlet weak var moreRiderInfoButton: UIButton!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var licenseNumberLabel: UILabel!
    @IBOutlet weak var racingAgeLabel: UILabel!
    @IBOutlet weak var birthdateLabel: UILabel!
    @IBOutlet weak var sexLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var uciIDLabel: UILabel!
    @IBOutlet weak var nationalityLabel: UILabel!
    @IBOutlet weak var citizenshipLabel: UILabel!
    @IBOutlet weak var localAssociationLabel: UILabel!
    @IBOutlet weak var emergencyContactName: UILabel!
    @IBOutlet weak var emergencyContactPhone: UILabel!
    @IBOutlet weak var laActiveStatusLabel: UILabel!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var eContactContainerView: UIView!
    @IBOutlet weak var moreInfoButton: UIButton!
    
    var searchManager:RiderLookupSearchManager?
    var rider:Member?
    var activeLicenses:[License] = []
    var expiredLicenses:[License] = []
    
    let licenseSectionHeaders = ["Active", "Expired"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if AppUtils.isUserAnOfficial() {
        
            moreRiderInfoButton.isHidden = false
            moreRiderInfoButton.setTitleColor(AppColors.HomeButtonColors.highlightColor, for: .highlighted)
            moreRiderInfoButton.layer.cornerRadius = 0.5 * moreRiderInfoButton.bounds.size.width
            moreRiderInfoButton.clipsToBounds = true
            
            if !UserDefaults.standard.bool(forKey: "hasMoreRiderInfoBeenSet") {
                self.performSegue(withIdentifier: "MoreInfoToolTipSegue", sender: nil)
                UserDefaults.standard.set(true, forKey: "hasMoreRiderInfoBeenSet")
                UserDefaults.standard.synchronize()
            }
        }
        else {
            moreRiderInfoButton.isHidden = true
        }

        if let nav = appDelegate.navController {
            AppUtils.addNavBar("Back", viewController:self, navigationController:nav, delegate:self)
            navigationController?.navigationBar.isTranslucent = false
        }
        
        searchManager = RiderLookupSearchManager(self)
        
        if let member = self.rider {
            
            fullNameLabel.text = member.full_name
            licenseNumberLabel.text = String(member.comp_id)
            
            // flag to indicate a bonus date from the API - a zero date
            if AppUtils.isDateInTheFuture(member.dob) {
                racingAgeLabel.text  = ""
                birthdateLabel.text = ""
            }
            else {
                birthdateLabel.text = member.birth_date
                if let racingAge = AppUtils.calcRacingAgeInYear(member) {
                    racingAgeLabel.text = String(racingAge)
                }
                else {
                    racingAgeLabel.text = ""
                }
            }

            if member.gender.rawValue == "MALE" {
                sexLabel.text = "M"
            }
            else if member.gender.rawValue == "FEMALE" {
                sexLabel.text = "F"
            }
            else {
                sexLabel.text = "U"
            }
            stateLabel.text = member.state
            
            if let id = member.uci_id {
                uciIDLabel.text = AppUtils.formatUciId(id)
            }
            else {
                uciIDLabel.text = " "
            }

            nationalityLabel.text = member.nationality
            if let citizen = member.citizenship {
                citizenshipLabel.text = " " + citizen
            }
            localAssociationLabel.text = member.la
            emergencyContactName.text = member.emergency_name
            
            if let phone = member.emergency_phone {
                emergencyContactPhone.text = AppUtils.formatPhone(phone)
            }
            else {
                emergencyContactPhone.text = " "
            }
            
            // set local association status indicator
            if let association = member.la, let status = member.la_active {
                AppUtils.setLocalAssociationStatusIndicator(laActiveStatusLabel, localAssociation:association, status:status)
            }
            else {
                AppUtils.setLocalAssociationStatusIndicator(laActiveStatusLabel, localAssociation:"", status:"")
            }
            
            // load license data
            loadLicenseData()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func backPicked() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func moreRiderInfoPicked(_ sender: Any) {
        
        self.performSegue(withIdentifier: "MoreRiderInfoSegue", sender: nil)
    }
    
    func loadLicenseData() {
        
        // first check if token is active
        if let nav = self.navigationController {
            
            AppUtils.checkAccess(nav, displayErrors:true, forceLoginOnError:true, displayLoginError:true, accessReturnCode:
            { success in
                if success == AppUtils.CheckAccessReturnValues.Success {
                    
                    if let compid = self.rider?.comp_id, let manager = self.searchManager {
                        
                        manager.getLicenses(String(compid))
                        { (licenseArray: [License]) in
                            
                            if licenseArray.count > 0 {
                                for license in licenseArray {
                                    
                                    if AppUtils.isDateExpired(license.expiration_date) {
                                        self.expiredLicenses.append(license)
                                    }
                                    else {
                                        self.activeLicenses.append(license)
                                    }
                                }
                                self.tableview.reloadData()
                            }
                        }
                    }
                }
                else {
                    print("RiderLookupResultsDetailsVC ..... check access failure")
                }
            })
        }
        else {
            SCLAlertView().showError("Error", subTitle: "System error - Nav controller not found!")
        }
        
    }
    
    //MARK: Tableview Methods
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return licenseSectionHeaders.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // active
        if section == 0 {
            if self.activeLicenses.count > 0 {
                return (self.activeLicenses.count)
            }
            else {
                // placeholder for zero license messages
                return 1
            }
        }
        // expired
        else if (section == 1) {
            if self.expiredLicenses.count > 0 {
                return (self.expiredLicenses.count)
            }
            else {
                // placeholder for zero license messages
                return 1
            }
            
        }
        else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = Bundle.main.loadNibNamed("LookupLicenseHeaderViewCell", owner: self, options: nil)?.first as! LookupLicenseHeaderViewCell
        
        if section == 0 {
            headerView.headerLabel.text = "ACTIVE"
        }
        else {
            headerView.headerLabel.text = "EXPIRED"
        }
        
        return headerView
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RiderLookupLicenseCell", for: indexPath) as? RiderLookupLicenseTableViewCell else {
            fatalError("The dequeued cell is not an instance of RiderLookupLicenseTableViewCell")
        }
        
        // active
        if indexPath.section == 0 {
            if self.activeLicenses.count > 0 {
                cell.licenseTypeTitleLabel.text = AppUtils.getLookupSection(self.activeLicenses[indexPath.row])
                cell.licenseTypeStatus.text = AppUtils.getLicenseStatus(self.activeLicenses[indexPath.row])
                cell.licenseTypeTitleLabel.textColor = AppColors.RiderLookupColors.normalFontColor
                cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                cell.isUserInteractionEnabled = true
                cell.selectionStyle = .gray
            }
            else {
                cell.licenseTypeTitleLabel.text = "No active licenses"
                cell.licenseTypeStatus.text = ""
                cell.licenseTypeTitleLabel.textColor = AppColors.RiderLookupColors.errorMessageFontColor
                cell.accessoryType = UITableViewCellAccessoryType.none
                cell.selectionStyle = .none
                cell.isUserInteractionEnabled = false
            }
        }
        // expired
        else if (indexPath.section == 1) {
            
            if self.expiredLicenses.count > 0 {
                cell.licenseTypeTitleLabel.text = AppUtils.getLookupSection(self.expiredLicenses[indexPath.row])
                cell.licenseTypeStatus.text = AppUtils.getLicenseStatus(self.expiredLicenses[indexPath.row])
                cell.licenseTypeTitleLabel.textColor = AppColors.RiderLookupColors.normalFontColor
                cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                cell.selectionStyle = .gray
                cell.isUserInteractionEnabled = true
            }
            else {
                cell.licenseTypeTitleLabel.text = "No expired licenses"
                cell.licenseTypeStatus.text = ""
                cell.licenseTypeTitleLabel.textColor = AppColors.RiderLookupColors.errorMessageFontColor
                cell.accessoryType = UITableViewCellAccessoryType.none
                cell.selectionStyle = .none
                cell.isUserInteractionEnabled = false
            }
        }
        else {
            cell.textLabel?.text = " "
            cell.licenseTypeTitleLabel.textColor = AppColors.RiderLookupColors.normalFontColor
            cell.accessoryType = UITableViewCellAccessoryType.none
            cell.selectionStyle = .none
            cell.isUserInteractionEnabled = false
        }
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if indexPath.section == 0 {
            
            if indexPath.row >= 0 && indexPath.row < self.activeLicenses.count {
                displayLicenseDetails(self.activeLicenses[indexPath.row])
            }
        }
        // expired
        else if (indexPath.section == 1) {
            
            if indexPath.row >= 0 && indexPath.row < self.expiredLicenses.count {
                displayLicenseDetails(self.expiredLicenses[indexPath.row])
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    //MARK: License Drilldown Display Methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "intlLicense" {
            
            let vc = segue.destination as? IntlLicenseViewController
            vc?.popoverPresentationController?.delegate = self
            
            if let indexPath = tableview.indexPathForSelectedRow {
                if indexPath.section == 0 {
                    vc?.license = self.activeLicenses[indexPath.row]
                }
                    // expired
                else if (indexPath.section == 1) {
                    vc?.license = self.expiredLicenses[indexPath.row]
                }
            }
        }
        else if segue.identifier == "domesticLicense" {
            
            let vc = segue.destination as? DomesticLicenseViewController
            vc?.popoverPresentationController?.delegate = self
            
            if let indexPath = tableview.indexPathForSelectedRow {
                if indexPath.section == 0 {
                    vc?.license = self.activeLicenses[indexPath.row]
                }
                    // expired
                else if (indexPath.section == 1) {
                    vc?.license = self.expiredLicenses[indexPath.row]
                }
            }
        }
        else if segue.identifier == "collegiateLicense" {
            
            let vc = segue.destination as? CollegiateLicenseViewController
            vc?.popoverPresentationController?.delegate = self
            
            if let indexPath = tableview.indexPathForSelectedRow {
                if indexPath.section == 0 {
                    vc?.license = self.activeLicenses[indexPath.row]
                }
                    // expired
                else if (indexPath.section == 1) {
                    vc?.license = self.expiredLicenses[indexPath.row]
                }
            }
        }
        else if segue.identifier == "proLicense" {
            
            let vc = segue.destination as? ProLicenseViewController
            vc?.popoverPresentationController?.delegate = self
            
            if let indexPath = tableview.indexPathForSelectedRow {
                if indexPath.section == 0 {
                    vc?.license = self.activeLicenses[indexPath.row]
                }
                    // expired
                else if (indexPath.section == 1) {
                    vc?.license = self.expiredLicenses[indexPath.row]
                }
            }
        }
        else if segue.identifier == "officialLicense" {
            
            let vc = segue.destination as? TechLicenseOfficialViewController
            vc?.popoverPresentationController?.delegate = self
            
            if let indexPath = tableview.indexPathForSelectedRow {
                if indexPath.section == 0 {
                    vc?.license = self.activeLicenses[indexPath.row]
                }
                    // expired
                else if (indexPath.section == 1) {
                    vc?.license = self.expiredLicenses[indexPath.row]
                }
            }
        }
        else if segue.identifier == "techDomesticGeneric" {
            
            let vc = segue.destination as? TechLicenseDomesticViewController
            vc?.popoverPresentationController?.delegate = self
            
            if let indexPath = tableview.indexPathForSelectedRow {
                if indexPath.section == 0 {
                    vc?.license = self.activeLicenses[indexPath.row]
                }
                    // expired
                else if (indexPath.section == 1) {
                    vc?.license = self.expiredLicenses[indexPath.row]
                }
            }
        }
        else if segue.identifier == "uciLicense" {
            
            let vc = segue.destination as? TechLicenseUciSupportViewController
            vc?.popoverPresentationController?.delegate = self
            
            if let indexPath = tableview.indexPathForSelectedRow {
                if indexPath.section == 0 {
                    vc?.license = self.activeLicenses[indexPath.row]
                }
                    // expired
                else if (indexPath.section == 1) {
                    vc?.license = self.expiredLicenses[indexPath.row]
                }
            }
        }
        else if segue.identifier == "MoreRiderInfoSegue" {
            let vc = segue.destination as? MoreRiderInfoViewController
            let delta = (self.view.frame.height / 2) - topContainerView.center.y
            vc?.yOffset = delta
            
            if let member = self.rider {
                
                if let home = member.home_phone{
                    vc?.homePhone = AppUtils.formatPhone(home)
                }
                else {
                    vc?.homePhone = ""
                }
                
                if let work = member.work_phone {
                    vc?.workPhone = AppUtils.formatPhone(work)
                }
                else {
                    vc?.workPhone = ""
                }

                vc?.emailAddress = member.email
            }
        }
        else if segue.identifier == "MoreInfoToolTipSegue" {

            let vc = segue.destination as? MoreInfoToolTipViewController
            vc?.modalPresentationStyle = UIModalPresentationStyle.popover
            vc?.popoverPresentationController?.delegate = self
            
            vc?.popoverPresentationController?.sourceRect = CGRect(x: moreInfoButton.bounds.origin.x, y: moreInfoButton.bounds.origin.y, width: moreInfoButton.bounds.width, height: moreInfoButton.bounds.height)
        }
    }
    
    func displayLicenseDetails(_ license:License) {
        
        if license.section_header == "INTERNATIONAL" {
            self.performSegue(withIdentifier: "intlLicense", sender: nil)
        }
        else if license.section_header == "PRO" {
            self.performSegue(withIdentifier: "proLicense", sender: nil)
        }
        else if license.section_header == "DOMESTIC" {
            self.performSegue(withIdentifier: "domesticLicense", sender: nil)
        }
        else if license.section_header == "COLLEGIATE" {
            self.performSegue(withIdentifier: "collegiateLicense", sender: nil)
        }
        
        // official license
        if license.section_header?.caseInsensitiveCompare("TECHNICAL") == .orderedSame {
            
            // Official
            if license.license_type == "O" {
                self.performSegue(withIdentifier: "officialLicense", sender: nil)
            }
            // Coach
            else if license.license_type == "C" {
                self.performSegue(withIdentifier: "techDomesticGeneric", sender: nil)
            }
            // Domestic Mechanic
            else if license.license_type == "M" {
                self.performSegue(withIdentifier: "techDomesticGeneric", sender: nil)
            }
            // Domestic Race Director
            else if license.license_type == "D" {
                self.performSegue(withIdentifier: "techDomesticGeneric", sender: nil)
            }
            // Domestic Drive - Vehicle
            else if license.license_type == "V" {
                self.performSegue(withIdentifier: "techDomesticGeneric", sender: nil)
            }
            // Pro / UCI Support
            else if license.section_subtitle?.caseInsensitiveCompare("UCI Support") == .orderedSame {
                self.performSegue(withIdentifier: "uciLicense", sender: nil)
            }
            
        }
        
    }
    
    func adaptivePresentationStyle(for controller:UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }

    
}
