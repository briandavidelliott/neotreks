//
//  IntlLicenseViewController.swift
// 
//
//  Created by Brian Elliott on 11/15/17.
// 
//

import UIKit

class IntlLicenseViewController: UIViewController {
        
    @IBOutlet weak var suspendedOrPendingImageView: UIImageView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var catStackView: UIStackView!
    @IBOutlet weak var clubsStackView: UIStackView!
    @IBOutlet weak var topLevelStackView: UIStackView!
    
    @IBOutlet weak var rdCatValueLabel: UILabel!
    @IBOutlet weak var trCatValueLabel: UILabel!
    @IBOutlet weak var otCatValueLabel: UILabel!
    @IBOutlet weak var mxCatValueLabel: UILabel!
    @IBOutlet weak var xcCatValueLabel: UILabel!
    @IBOutlet weak var cxCatValueLabel: UILabel!
    @IBOutlet weak var dhCatValueLabel: UILabel!
    
    @IBOutlet weak var firstTeamValueLabel: UILabel!
    @IBOutlet weak var secondTeamValueLabel: UILabel!
    @IBOutlet weak var thirdTeamValueLabel: UILabel!
    @IBOutlet weak var fourthTeamValueLabel: UILabel!
    
    @IBOutlet weak var intlTeamNameLabel: UILabel!
    
    public var license:License?
    
    private lazy var popupBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = GeneralConstants.RiderLookup.defaultCornerRadius
        return view
    }()
    
    private func pinBackground(_ view: UIView, to stackView: UIStackView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        stackView.insertSubview(view, at: 0)
        view.pin(to: stackView)
    }
    
    private lazy var catBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = AppColors.RiderLookupColors.sectionBackgroundColor
        view.layer.cornerRadius = GeneralConstants.RiderLookup.defaultCornerRadius
        return view
    }()
    
    private lazy var clubBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = AppColors.RiderLookupColors.sectionBackgroundColor
        view.layer.cornerRadius = GeneralConstants.RiderLookup.defaultCornerRadius
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pinBackground(popupBackgroundView, to: topLevelStackView)
        topLevelStackView.layoutMargins = UIEdgeInsets(top: GeneralConstants.RiderLookup.popupMargin, left: GeneralConstants.RiderLookup.popupMargin, bottom: GeneralConstants.RiderLookup.popupMargin, right: GeneralConstants.RiderLookup.popupMargin)
        topLevelStackView.isLayoutMarginsRelativeArrangement = true
        
        popupView.layer.cornerRadius = 10.0
        popupView.layer.borderWidth = 4.0
        popupView.layer.borderColor = (AppColors.RiderLookupColors.borderColor).cgColor
        popupView.layer.masksToBounds = true
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(popupViewPicked(_:)))
        singleTap.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(singleTap)
        
        AppUtils.pinBackground(catBackgroundView, to: catStackView)
        catStackView.layoutMargins = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        catStackView.isLayoutMarginsRelativeArrangement = true
        
        AppUtils.pinBackground(clubBackgroundView, to: clubsStackView)
        clubsStackView.layoutMargins = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        clubsStackView.isLayoutMarginsRelativeArrangement = true
        
        if let license = self.license {
            
            setLicenseStatusImage()
            
            if let extra = license.extra {
                intlTeamNameLabel.text = extra["intl_team_name"]
            }
        
            if let cat = license.categories {
                rdCatValueLabel.text = cat["RD"]
                trCatValueLabel.text = cat["TR"]
                otCatValueLabel.text = cat["OT"]
                mxCatValueLabel.text = cat["MX"]
                xcCatValueLabel.text = cat["XC"]
                cxCatValueLabel.text = cat["CX"]
                dhCatValueLabel.text = cat["DH"]
            }
            
            firstTeamValueLabel.adjustsFontSizeToFitWidth = true
            secondTeamValueLabel.adjustsFontSizeToFitWidth = true
            thirdTeamValueLabel.adjustsFontSizeToFitWidth = true
            fourthTeamValueLabel.adjustsFontSizeToFitWidth = true
            
            if let teams_clubs = license.clubs {
            
                var count = 0
                let sortedKeys = (teams_clubs.keys).sorted{ $0.lowercased() < $1.lowercased() }
                for key in sortedKeys {

                    count += 1
                    if count == 1 {
                        if let value = teams_clubs[key] {
                            firstTeamValueLabel.text = key + " : " + value
                        }
                    }
                    else if count == 2 {
                        if let value = teams_clubs[key] {
                            secondTeamValueLabel.text = key + " : " + value
                        }
                    }
                    else if count == 3 {
                        if let value = teams_clubs[key] {
                            thirdTeamValueLabel.text = key + " : " + value
                        }
                    }
                    else if count == 4 {
                        if let value = teams_clubs[key] {
                            fourthTeamValueLabel.text = key + " : " + value
                        }
                    }
                }
            }
        }
    }
    
    func setLicenseStatusImage() {
        
        if let licenseStatus = license?.license_status {
            
            // Check if license is suspended
            if licenseStatus.uppercased() == "S" {
                suspendedOrPendingImageView.isHidden = false
                let image = UIImage(named: "Suspended")
                suspendedOrPendingImageView.image = image
            }
                // Check if license is pending
            else if licenseStatus.uppercased() == "P" {
                suspendedOrPendingImageView.isHidden = false
                let image = UIImage(named: "Pending")
                suspendedOrPendingImageView.image = image
            }
            else if let expirationDate = license?.expiration_date {
                if AppUtils.isDateExpired(expirationDate) {
                    suspendedOrPendingImageView.isHidden = false
                    let image = UIImage(named: "Expired")
                    suspendedOrPendingImageView.image = image
                }
                else {
                    suspendedOrPendingImageView.isHidden = true
                }
            }
            else {
                suspendedOrPendingImageView.isHidden = true
            }
        }
        else {
            suspendedOrPendingImageView.isHidden = true
        }
    }
    @IBAction func xPicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func closePicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func popupViewPicked(_ touch: UITapGestureRecognizer) {
        
        let touchPoint = touch.location(in: self.view)
        let popupRect = self.view.convert(self.popupView.frame, from: self.view)
        
        if !popupRect.contains(touchPoint) {
            dismiss(animated: true, completion: nil)
        }
    }

}
