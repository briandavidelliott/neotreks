//
//  TechLicenseDomesticViewController.swift
// 
//
//  Created by Brian Elliott on 11/15/17.
//
// 
//

import UIKit

class TechLicenseDomesticViewController: UIViewController {
    
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var licenseTypeTitleLabel: UILabel!
    @IBOutlet weak var subtitleLicenseLabel: UILabel!
    
    @IBOutlet weak var genericUseStackView: UIStackView!
    @IBOutlet weak var genericUseTitleLabel: UILabel!
    @IBOutlet weak var genericUseValueLabel: UILabel!
    @IBOutlet weak var topLevelStackView: UIStackView!
    
    @IBOutlet weak var suspendedOrPendingImageView: UIImageView!
    public var license:License?
    
    private lazy var popupBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = GeneralConstants.RiderLookup.defaultCornerRadius
        return view
    }()
    
    private func pinBackground(_ view: UIView, to stackView: UIStackView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        stackView.insertSubview(view, at: 0)
        view.pin(to: stackView)
    }
    
    private lazy var genericBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = AppColors.RiderLookupColors.sectionBackgroundColor
        view.layer.cornerRadius = GeneralConstants.RiderLookup.defaultCornerRadius
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        pinBackground(popupBackgroundView, to: topLevelStackView)
        topLevelStackView.layoutMargins = UIEdgeInsets(top: GeneralConstants.RiderLookup.popupMargin, left: GeneralConstants.RiderLookup.popupMargin, bottom: GeneralConstants.RiderLookup.popupMargin, right: GeneralConstants.RiderLookup.popupMargin)
        topLevelStackView.isLayoutMarginsRelativeArrangement = true
        
        popupView.layer.cornerRadius = 10.0
        popupView.layer.borderWidth = 4.0
        popupView.layer.borderColor = (AppColors.RiderLookupColors.borderColor).cgColor
        popupView.layer.masksToBounds = true
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(popupViewPicked(_:)))
        singleTap.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(singleTap)
        
        AppUtils.pinBackground(genericBackgroundView, to: genericUseStackView)
        genericUseStackView.layoutMargins = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        genericUseStackView.isLayoutMarginsRelativeArrangement = true

        if let license = self.license {
            
            setLicenseStatusImage()
            
            licenseTypeTitleLabel.text = license.section_header
            licenseTypeTitleLabel.adjustsFontSizeToFitWidth = true
            subtitleLicenseLabel.text = license.section_subtitle
            licenseTypeTitleLabel.adjustsFontSizeToFitWidth = true
            
            // Coach
            if license.license_type == "C" {
                
                genericUseTitleLabel.isHidden = true
                        
                if let extra = license.extra {
                    
                    var coachInfoString = ""
                    
                    if let level = extra["coaching_level"] {
                        coachInfoString += "LEVEL: " + level + "\n"
                    }

                    if let isExpired = extra["bckcheck_is_expired"]  {
                        if isExpired == "true" {
                            coachInfoString += "CRIMINAL BG CHECK EXPIRES : Expired! \n"
                        }
                        else {
                            if let checkDate = extra["bckcheck_expire_date"] {
                                coachInfoString += "CRIMINAL BG CHECK EXPIRES : " + checkDate + " \n"
                            }
                            else {
                                coachInfoString += "CRIMINAL BG CHECK EXPIRES : \n"
                            }
                        }
                    }
                    else {
                        coachInfoString += "CRIMINAL BG CHECK EXPIRES : \n"
                    }
                
                    if let licenseExpires = AppUtils.formatDate(license.expiration_date) {
                        coachInfoString += "LICENSE EXPIRES : " + licenseExpires + "\n"
                    }
                    
                     genericUseValueLabel.text = coachInfoString
                }
            
            }
            // Domestic Mechanic
            else if license.license_type == "M" {
                
                genericUseTitleLabel.isHidden = false
                
                genericUseTitleLabel.text = "Category"
            
                if let extra = license.extra {
                    genericUseValueLabel.text = extra["mechanic_category"]
                }
            
            }
            // Domestic Race Director
            else if license.license_type == "D" {
                
                genericUseTitleLabel.isHidden = false

                genericUseTitleLabel.text = "Info"
            
                if let extra = license.extra {
                    if let value = extra["is_club"] {
                        if value == "true" {
                            genericUseValueLabel.text = "CLUB SPONSORED"
                        }
                        else {
                            genericUseValueLabel.text = "PAID"
                        }
                    }
                }
            
            }
            // Domestic Drive - Vehicle
            else if license.license_type == "V" {
                genericUseTitleLabel.isHidden = false
                
                genericUseTitleLabel.text = "Info"
                genericUseValueLabel.text = "None"
                
                subtitleLicenseLabel.text = "Domestic Drive"
            }
        }
    }
    
    func setLicenseStatusImage() {
        
        if let licenseStatus = license?.license_status {
            
            // Check if license is suspended
            if licenseStatus.uppercased() == "S" {
                suspendedOrPendingImageView.isHidden = false
                let image = UIImage(named: "Suspended")
                suspendedOrPendingImageView.image = image
            }
                // Check if license is pending
            else if licenseStatus.uppercased() == "P" {
                suspendedOrPendingImageView.isHidden = false
                let image = UIImage(named: "Pending")
                suspendedOrPendingImageView.image = image
            }
            else if let expirationDate = license?.expiration_date {
                if AppUtils.isDateExpired(expirationDate) {
                    suspendedOrPendingImageView.isHidden = false
                    let image = UIImage(named: "Expired")
                    suspendedOrPendingImageView.image = image
                }
                else {
                    suspendedOrPendingImageView.isHidden = true
                }
            }
            else {
                suspendedOrPendingImageView.isHidden = true
            }
        }
        else {
            suspendedOrPendingImageView.isHidden = true
        }
    }
    
    @IBAction func xPicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closePicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func popupViewPicked(_ touch: UITapGestureRecognizer) {
        
        let touchPoint = touch.location(in: self.view)
        let popupRect = self.view.convert(self.popupView.frame, from: self.view)
        
        if !popupRect.contains(touchPoint) {
            dismiss(animated: true, completion: nil)
        }
    }

}
