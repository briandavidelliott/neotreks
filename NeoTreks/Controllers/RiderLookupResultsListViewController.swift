//
//  RiderLookupResultsListViewController.swift
// 
//
//  Created by Brian Elliott on 10/9/17.
// 
//

import UIKit

class RiderLookupResultsListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NavBarBackProtocol {
    
    @IBOutlet weak var tableview: UITableView!
    
    var riderLookupArray:[Member] = []
    var totalRidersFound:Int = 0
    var searchManager:RiderLookupSearchManager?
    var searchValues:String = ""
    var pageNumber = 1
    var moreIsAvailable = false

    override func viewDidLoad() {
        super.viewDidLoad()

        if let nav = appDelegate.navController {
            AppUtils.addNavBar("Back", viewController:self, navigationController:nav, delegate:self)
        }
        
        if totalRidersFound > GeneralConstants.RiderLookup.searchItemsPerPage {
            moreIsAvailable = true
        }
        else {
            moreIsAvailable = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func backPicked() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if moreIsAvailable {
            // add one to the items per page
            return riderLookupArray.count + 2
        }
        else {
            // add one to account for header row
            return riderLookupArray.count + 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RiderLookupCell", for: indexPath) as? RiderLookupListTableViewCell else {
            fatalError("The dequeued cell is not an instance of RiderLookupListTableViewCell")
        }
        
        // add header to first row
        if indexPath.row == 0 {
            cell.moreRidersLabel.isHidden = true
            cell.nameLabel.text = "Name"
            cell.licenseLabel.text = "License #"
            cell.birthdateLabel.text = "Birthdate "
            cell.stateLabel.text = "State"
            cell.accessoryType = .none
            cell.backgroundColor = AppColors.RiderLookupColors.sectionBackgroundColor
            //cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.disclosureIndictorImageView.alpha = 0.1
            cell.stateLabelTrailingConstraint?.constant = 16
            cell.stateLabelWidthConstraint.constant = 32
            cell.disclosureIndictorImageView.image = UIImage(named:"Blank")

        }
        // add cell for loadng more riders
        else if moreIsAvailable && indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
            
            cell.moreRidersLabel.isHidden = false
            cell.backgroundColor = AppColors.RiderLookupColors.sectionBackgroundColor
            cell.moreRidersLabel.backgroundColor = AppColors.RiderLookupColors.sectionBackgroundColor
            cell.accessoryType = .none
            cell.tag = GeneralConstants.RiderLookup.searchMoreViewTag
            
            // blank out other labels because the selection shows remnants from the recycled cell
            cell.nameLabel.text = ""
            cell.licenseLabel.text = " "
            cell.birthdateLabel.text = " "
            cell.stateLabel.text = " "
            cell.disclosureIndictorImageView.isHidden = true
            cell.disclosureIndictorImageView.image = UIImage(named:"Blank")
        }
        else {
        
            let rider = riderLookupArray[indexPath.row-1]
            cell.moreRidersLabel.isHidden = true
            cell.nameLabel.text = rider.first_name + " " + rider.last_name
            cell.licenseLabel.text = String(rider.comp_id)
            
            let dob = AppUtils.getBirthdate(rider.dob)
            if dob == "N/A" {
                cell.birthdateLabel.text = ""
            }
            else {
                cell.birthdateLabel.text = dob
            }
            
            // cell.birthdateLabel.text = AppUtils.getBirthdate(rider.dob)
            cell.stateLabel.text = rider.state.uppercased()
            //cell.accessoryType = .disclosureIndicator
            cell.backgroundColor = AppColors.RiderLookupColors.normalCellBackgroundColor
            cell.disclosureIndictorImageView.isHidden = false
            cell.disclosureIndictorImageView.image = UIImage(named:"RightArrow")
            // cell.disclosureIndictorImageView.image = UIImage(named:"ExpandArrow")
            cell.stateLabelTrailingConstraint?.constant = 32
            cell.stateLabelWidthConstraint.constant = 15
            
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath) as? RiderLookupListTableViewCell {
            
//            let selectionColor = cell.selectedBackgroundView?.backgroundColor
//
//            if let color = selectionColor {
//                cell.birthdateLabel.textColor = color
//            }
//
            // more riders row selected
            if cell.tag == GeneralConstants.RiderLookup.searchMoreViewTag {
                getMoreResults(cell)
            }
            // individual rider selected
            else {
               if indexPath.row > 0 {
                    let rider = riderLookupArray[indexPath.row-1]
                    let storyboard = UIStoryboard(name: "RiderLookupController", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "RiderLookupResultsDetailsViewController") as! RiderLookupResultsDetailsViewController
                
                    vc.rider = rider
                    self.navigationController?.pushViewController(vc, animated: true)
               }
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func setMoreAvailableFlag(_ cell:RiderLookupListTableViewCell, thereIsMore:Bool) {
        
        self.moreIsAvailable = thereIsMore
        if thereIsMore {
            cell.moreRidersLabel.isHidden = false
        }
        else {
            cell.moreRidersLabel.isHidden = true
        }
    }
    
    func getMoreResults(_ cell:RiderLookupListTableViewCell) {
        
        // first check if token is active
        if let nav = self.navigationController {
            AppUtils.checkAccess(nav, displayErrors:true, forceLoginOnError:true, displayLoginError:true, accessReturnCode:
                { success in
                    if success == AppUtils.CheckAccessReturnValues.Success {
                        
                        self.pageNumber += 1
                        if  let manager = self.searchManager {
                            manager.searchRiders(self.searchValues, pageNumber:self.pageNumber)
                            { (riderArray: [Member], totalCount:Int) in
                                
                                // Go to results list view if there is results (error alerts will be displayed previously)
                                if riderArray.count > 0 {

                                    // add new results to previous found rider array
                                    // self.riderLookupArray += riderArray
                                    self.riderLookupArray.append(contentsOf: riderArray)
                                    self.totalRidersFound = totalCount
                                    let currentNumberRiders = self.riderLookupArray.count
                                    let deltaLeft = self.totalRidersFound - currentNumberRiders
                                    
                                    // if next query would exceed max, don't allow it
                                    if currentNumberRiders + GeneralConstants.RiderLookup.searchItemsPerPage >
                                        GeneralConstants.RiderLookup.searchMaxAllowed {
                                        
                                        self.setMoreAvailableFlag(cell, thereIsMore:false)
                                    }
                                    else if deltaLeft > 0 {
                                        self.setMoreAvailableFlag(cell, thereIsMore:true)
                                    }
                                    else {
                                       self.setMoreAvailableFlag(cell, thereIsMore:false)
                                    }
                                    
                                    self.tableview.reloadData()

                                }
                                else {
                                   SCLAlertView().showError("Error", subTitle: "No more riders found!")
                                }
                            }
                        }
                        else {
                            SCLAlertView().showError("Error", subTitle: "No more riders found!")
                        }
                    }
            })
        }
        else {
            SCLAlertView().showError("Error", subTitle: "System error - Nav controller not found!")
        }
    }
    
}
