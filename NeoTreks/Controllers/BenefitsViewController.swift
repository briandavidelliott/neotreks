//
//  BenefitsViewController.swift
// 
//
//  Created by Brian Elliott on 8/11/17.
// 
//

import UIKit
import SwiftyJSON

class BenefitsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NavBarBackProtocol {
    
    @IBOutlet weak var memberUnderlineView: UIView!
    @IBOutlet weak var partnerUnderlineView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var membershipButton: UIButton!
    @IBOutlet weak var partnerButton: UIButton!
    @IBOutlet weak var explainationLabel: UILabel!
    @IBOutlet weak var partnersView: UIView!
    @IBOutlet weak var partnersStackView: UIStackView!

    var isMemberTabStateOn = true
    var currentMembership:GeneralConstants.Benefits.MembershipTypes = GeneralConstants.Benefits.MembershipTypes.Ride
    
    var benefitsArray:[Benefit] = []
    var partnersArray:[Partner] = []
    var partnersIndexMapArray:[Int:Partner] = [:]
    var expandedCellPaths = Set<IndexPath>()
    
    let collapsedCellImage = UIImage(named:GeneralConstants.Benefits.collaspedCellIcon)
    let expandedCellImage = UIImage(named:GeneralConstants.Benefits.expandedCellIcon)
    let externalLinkCellImage = UIImage(named:GeneralConstants.Benefits.externalLinkCellIcon)
    let internalLinkCellImage = UIImage(named:GeneralConstants.Benefits.internalLinkCellIcon)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let nav = appDelegate.navController {
            AppUtils.addNavBar("Home", viewController:self, navigationController:nav, delegate:self)
        }
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 250
        
        currentMembership = AppUtils.getMembership()
        
        // update tab button with membership type
        setMembershipTabTitle()

        loadData()
        
        buildPartnerGrid()
        
        partnerButton.setTitleColor(AppColors.BenefitColors.tabOffTextColor, for: .normal)
        partnerButton.setTitleColor(AppColors.BenefitColors.tabSelectedTextColor, for: .selected)
        partnerButton.setTitleColor(AppColors.BenefitColors.tabOnTextColor, for: .highlighted)
        
        membershipButton.setTitleColor(AppColors.BenefitColors.tabOffTextColor, for: .normal)
        membershipButton.setTitleColor(AppColors.BenefitColors.tabSelectedTextColor, for: .selected)
        membershipButton.setTitleColor(AppColors.BenefitColors.tabOnTextColor, for: .highlighted)
        
        // add top separator
        let px = 1 / UIScreen.main.scale
        let frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: px)
        let line = UIView(frame: frame)
        self.tableView.tableHeaderView = line
        line.backgroundColor = self.tableView.separatorColor
        
        setTabState(true)
        
        displayMembershipPicker()
    }
    
    func backPicked() {
        _ = navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Data loading
    private func loadData() {
        
        // membership data
        if let json = AppUtils.readJson(GeneralConstants.Benefits.benefitsJsonFile) {
            
            let benefits = json["benefits"]
            
            for (_, object) in benefits {
                
                let title = object["title"].stringValue
                let description = object["description"].stringValue
                let inAppLink = object["in-app-link"].stringValue
                let externalLink = object["external-link"].stringValue
                let offeredToArray:[String] = object["offeredTo"].arrayValue.map { $0.stringValue}
                
                let benefit = Benefit(title: title, description: description, offeredTo: offeredToArray, inAppLink: inAppLink, externalLink: externalLink)
                
                //benefitsArray.append(benefit)
                
                if AppUtils.isBenefitApplicable(offeredToArray, currentMembershipLevel: currentMembership) {
                    benefitsArray.append(benefit)
                }

            }

        }
        else {
            SCLAlertView().showError("Error", subTitle: "Could not find benefits data!")
        }
        
        // partner data
        if let json = AppUtils.readJson(GeneralConstants.Benefits.partnerJsonFile) {
            
            let partners = json["partners"]
            
            for (_, object) in partners {
                
                let partnerName = object["partner"].stringValue
                let imageName = object["image"].stringValue
                let details = object["details"].stringValue
                let offeredToArray:[String] = object["offeredTo"].arrayValue.map { $0.stringValue}
                
                let partner = Partner(partner: partnerName, imageName: imageName, details:details, offeredTo: offeredToArray)
                
                if AppUtils.isBenefitApplicable(offeredToArray, currentMembershipLevel: currentMembership) {
                    partnersArray.append(partner)
                }
                // partnersArray.append(partner)
                
            }
            
        }
        else {
            SCLAlertView().showError("Error", subTitle: "Could not find benefits data!")
        }
        
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return keysArray.count
        return benefitsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BenefitsCell", for: indexPath) as? BenefitsTableViewCell else {
            fatalError("The dequeued cell is not an instance of BenefitsTableViewCell")
        }
        
        let benefit = benefitsArray[indexPath.row]
        
        cell.titleLabel.text = benefit.title
        
        if let externalLink = benefit.externalLink, !externalLink.isEmpty  {
            print("external link: \(externalLink)")
            cell.descriptionTextView.isHidden = true
            cell.expandIconImageView.image = externalLinkCellImage
        }
        else if let inAppLink = benefit.inAppLink, !inAppLink.isEmpty {
            
            print("in-app link: \(inAppLink)")
            cell.descriptionTextView.isHidden = true
            cell.expandIconImageView.image = internalLinkCellImage
            
        }
        else {
            
            print("both external & in-app no value: \(benefit.title ?? "brian ")")
        
            // cell.descriptionTextView.isHidden = false
            cell.descriptionTextView.isHidden = !expandedCellPaths.contains(indexPath)
            
            if let description = benefit.description {

                let htmlHeaderString = "<html>" +
                    "<head>" +
                    "<style>" +
                    "body {" +
                    "background-color: rgb(177, 177, 177);" +
                    "font-family: 'Arial'; ;font-size: 16px;" +
                    "text-decoration:none;" +
                    "}" +
                    "</style>" +
                    "</head>"
        
                let htmlString = htmlHeaderString +
                    "<body>" + description + "</body></head></html>"
        
                let htmlData = NSString(string: htmlString).data(using: String.Encoding.unicode.rawValue)
        
                let attributedString = try! NSAttributedString(data: htmlData!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
        
                cell.descriptionTextView.attributedText = attributedString
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath) as? BenefitsTableViewCell {
            
            let benefit = benefitsArray[indexPath.row]
            
            if let externalLink = benefit.externalLink, !externalLink.isEmpty {
                
                if let url = URL(string: externalLink) {
                    UIApplication.shared.openURL(url)
                }
            }
            else if let inAppLink = benefit.inAppLink, !inAppLink.isEmpty {
                setTabState(false)
            }
            else {
                
                cell.descriptionTextView.isHidden = !cell.descriptionTextView.isHidden
                if cell.descriptionTextView.isHidden {
                    expandedCellPaths.remove(indexPath)
                } else {
                    expandedCellPaths.insert(indexPath)
                }
            
                updateExpandIcon(cell.descriptionTextView.isHidden, imageView:cell.expandIconImageView)
            
                tableView.beginUpdates()
                tableView.endUpdates()
                tableView.deselectRow(at: indexPath, animated: true)
            }
        }
    }
    
    func updateExpandIcon(_ isHIdden:Bool, imageView:UIImageView) {
        
        if isHIdden {
            
            UIView.transition(with: imageView,
                              duration: 0.5,
                              options: .transitionCrossDissolve,
                              animations: {
                                imageView.image = self.expandedCellImage
            },
                              completion: nil)
        }
        else {
            
            UIView.transition(with: imageView,
                              duration: 0.5,
                              options: .transitionCrossDissolve,
                              animations: {
                                imageView.image = self.collapsedCellImage
            },
                              completion: nil)
            
        }
        

    }
    
    @IBAction func membershipTabPicked(_ sender: Any) {
        print("membership picked")
        setTabState(true)
        
    }
    
    @IBAction func partnerTabPicked(_ sender: Any) {
        print("partnerTabPicked picked")
        setTabState(false)
    }
    
    
    func setTabState(_ memberState:Bool) {
        
        if memberState {
            
            membershipButton.isSelected = true
            partnerButton.isSelected = false
            
            membershipButton.backgroundColor = AppColors.BenefitColors.tabOnBackgroundColor
            partnerButton.backgroundColor = AppColors.BenefitColors.tabOffBackgroundColor
            
            memberUnderlineView.isHidden = false
            partnerUnderlineView.isHidden = true
            
            tableView.isHidden = false
            partnersView.isHidden = true
            
            setExplanation()
            
        }
        // partner state
        else {
            
            membershipButton.isSelected = false
            partnerButton.isSelected = true
            
            membershipButton.backgroundColor = AppColors.BenefitColors.tabOffBackgroundColor
            partnerButton.backgroundColor = AppColors.BenefitColors.tabOnBackgroundColor
            
            memberUnderlineView.isHidden = true
            partnerUnderlineView.isHidden = false
            
            tableView.isHidden = true
            partnersView.isHidden = false
            
            explainationLabel.text = GeneralConstants.Benefits.partnerExplaination
            
        }
        
        isMemberTabStateOn = memberState
    }
    
    func setMembershipTabTitle() {
        membershipButton.setTitle(currentMembership.rawValue + " Benefits", for: .normal)
    }
    
    func setExplanation() {
        let membershipString = GeneralConstants.Benefits.membershipExplaination
        explainationLabel.text = membershipString.replacingOccurrences(of: "%membership_type%", with: currentMembership.rawValue)
    }
    
    func processMenuPick() {
        
        self.setMembershipTabTitle()
        self.setExplanation()
        
        benefitsArray.removeAll()
        partnersArray.removeAll()
        partnersIndexMapArray.removeAll()
        expandedCellPaths.removeAll()
        
        self.loadData()
        
        for view in partnersStackView.arrangedSubviews {
            partnersStackView.removeArrangedSubview(view)
        }

        self.buildPartnerGrid()
        
        self.tableView.reloadData()
    }
    
    // This is temporary and until membership level is roled out in API.
    func displayMembershipPicker() {
        
        let optionMenuController = UIAlertController(title: "Testing Membership", message: "Pick membership:", preferredStyle: .actionSheet)
        let rideAction = UIAlertAction(title: "Ride", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.currentMembership = GeneralConstants.Benefits.MembershipTypes.Ride
            self.processMenuPick()

        })
        
        let ridePlusAction = UIAlertAction(title: "Ride Plus", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.currentMembership = GeneralConstants.Benefits.MembershipTypes.RidePlus
            self.processMenuPick()
            
        })
        
        let raceAction = UIAlertAction(title: "Race", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.currentMembership = GeneralConstants.Benefits.MembershipTypes.Race
            self.processMenuPick()

        })
        
        let racePodiumAction = UIAlertAction(title: "Race Podium", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.currentMembership = GeneralConstants.Benefits.MembershipTypes.RacePodium
            self.processMenuPick()

        })
        
        let racePodiumPlusAction = UIAlertAction(title: "Race Podium Plus", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.currentMembership = GeneralConstants.Benefits.MembershipTypes.RacePodiumPlus
            self.processMenuPick()

        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenuController.addAction(rideAction)
        optionMenuController.addAction(ridePlusAction)
        optionMenuController.addAction(raceAction)
        optionMenuController.addAction(racePodiumAction)
        optionMenuController.addAction(racePodiumPlusAction)        
        optionMenuController.addAction(cancelAction)

        self.present(optionMenuController, animated: true, completion: nil)

    }
    
    func imagePicked(_ sender: UITapGestureRecognizer) {
        
        if let view = sender.view {
            print("Imageview Clicked tag: \(view.tag)")
            
            if let details = partnersIndexMapArray[view.tag]?.details, let partner = partnersIndexMapArray[view.tag]?.partner {
                print("details: \(details)")
                
                // let alert = PartnerBenefitsAlertView()
                
                let alert = SCLAlertView()

                // let textView = alert.addTextView()
                
                let htmlHeaderString = "<html>" +
                    "<head>" +
                    "<style>" +
                    "body {" +
                    "background-color: rgb(177, 177, 177);" +
                    "font-family: 'Arial'; ;font-size: 16px;" +
                    "text-decoration:none;" +
                    "}" +
                    "</style>" +
                "</head>"
                
                let htmlString = htmlHeaderString +
                    "<body>" + details + "</body></head></html>"
                
                let htmlData = NSString(string: htmlString).data(using: String.Encoding.unicode.rawValue)
                
                let attributedString = try! NSAttributedString(data: htmlData!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
                
                alert.showHTMLInfo(partner, subTitle: attributedString)
                
            }
        }

    }
    
    private func popupDetails(_ text:String, tappedView: UIView) {
        
        let htmlHeaderString = "<html>" +
            "<head>" +
            "<style>" +
            "body {" +
            "background-color: rgb(177, 177, 177);" +
            "font-family: 'Arial'; ;font-size: 16px;" +
            "text-decoration:none;" +
            "}" +
            "</style>" +
        "</head>"
        
        let htmlString = htmlHeaderString +
            "<body>" + text + "</body></head></html>"
        
        let htmlData = NSString(string: htmlString).data(using: String.Encoding.unicode.rawValue)
        
        let attributedString = try! NSAttributedString(data: htmlData!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
        
        partnersView.isUserInteractionEnabled = false

        
    }
    
    func buildPartnerGrid() {
        
        var imagePerRowCount = 0
        
        var imageCount = 0
        
        var twoColumStackView:UIStackView?
        
        for partner in partnersArray {
            
            if let name = partner.imageName {
                
                imageCount += 1
                
                imagePerRowCount += 1
                
                let image = UIImage(named: name)
                let imageView = UIImageView(image: image)
                
                let singleTap = UITapGestureRecognizer(target: self, action: #selector(imagePicked(_:)))
                singleTap.numberOfTapsRequired = 1
                imageView.isUserInteractionEnabled = true
                imageView.addGestureRecognizer(singleTap)
                imageView.tag = imageCount
                
                partnersIndexMapArray[imageCount] = partner
            
                if imagePerRowCount == 1 {
                    
                    twoColumStackView = UIStackView(frame: CGRect(x: 0, y: 0, width: GeneralConstants.Benefits.partnerImageWidth, height: GeneralConstants.Benefits.partnerImageHeight))
                    twoColumStackView?.axis = .horizontal
                    twoColumStackView?.distribution = .equalCentering
                    twoColumStackView?.alignment = .center
                    twoColumStackView?.spacing = GeneralConstants.Benefits.partnerImageSpacing
                }
                
                if let stackView = twoColumStackView {
                    stackView.addArrangedSubview(imageView)

                    
                    if imagePerRowCount == 2 {
                        partnersStackView.addArrangedSubview(stackView)
                        imagePerRowCount = 0
                    }
                }
                
            }
            
        }
        
    }

}
