//
//  CustomSegueWithAnimation.swift
// 
//
//  Created by Brian Elliott on 1/15/18.
// 
//

import Foundation

class CustomSegueWithAnimation: UIStoryboardSegue {
    
    override func perform() {
        
        let sourceVC = source as UIViewController
        let destinationVC = destination as UIViewController
        let window = UIApplication.shared.keyWindow!
        
        destination.view.alpha = 0.0
        window.insertSubview(destinationVC.view, belowSubview: sourceVC.view)
        
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            sourceVC.view.alpha = 0.0
            destinationVC.view.alpha = 1.0
        }) { (finished) -> Void in
            sourceVC.view.alpha = 1.0
            sourceVC.present(destinationVC, animated: false, completion: nil)
        }
    }
}
