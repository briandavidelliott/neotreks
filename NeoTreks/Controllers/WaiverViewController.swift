//
//  WaiverViewController.swift
// 
//
//  Created by Brian Elliott on 1/26/18.
// 
//

import Foundation
import SwiftyJSON

struct ExpandableNames {
    var isExpanded: Bool
    let names: Waiver
    let numberRows: Int
    let id: Int
    var openCloseImageView: UIImageView?
    var openCloseLabel:UILabel?
    var signedOff: Bool
}

struct WaiverTitleStruct {
    let searchString:String
    let resolvesTo:String
}

class WaiverViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UITextViewDelegate {
    
    let waiverTitles:[WaiverTitleStruct] = [
        WaiverTitleStruct(searchString: GeneralConstants.Waivers.adultWaiverSearchString, resolvesTo: GeneralConstants.Waivers.adultWaiverTitle),
        WaiverTitleStruct(searchString: GeneralConstants.Waivers.childWaiverSearchString, resolvesTo: GeneralConstants.Waivers.childWaiverTitle),
        WaiverTitleStruct(searchString: GeneralConstants.Waivers.liabilityWaiverSearchString, resolvesTo: GeneralConstants.Waivers.liabilityWaiverTitle),
        WaiverTitleStruct(searchString: GeneralConstants.Waivers.mediaGrantWaiverSearchString, resolvesTo: GeneralConstants.Waivers.mediaGrantWaiverTitle),
        WaiverTitleStruct(searchString: GeneralConstants.Waivers.codeOfConductWaiverSearchString, resolvesTo: GeneralConstants.Waivers.codeOfConductWaiverTitle)
    ]
    
    @IBOutlet weak var waiverTableView: UITableView!
    @IBOutlet weak var fullNameTextView: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    
    var twoDimensionalArray:[ExpandableNames] = []
    var waivers:[Waiver] = []
    var waiverId:Int?
    var expandedCellPaths = Set<IndexPath>()
    
    let collapsedCellImage = UIImage(named:GeneralConstants.Waivers.collaspedCellIcon)
    let expandedCellImage = UIImage(named:GeneralConstants.Waivers.expandedCellIcon)
    let greenCheckBoxCellImage = UIImage(named:GeneralConstants.Waivers.greenCheckBoxIcon)
    
    // MARK: - Lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()
        
        var count = -1
        for waiver in waivers {
            count += 1
            let expandableName:ExpandableNames = ExpandableNames(isExpanded: false, names: waiver, numberRows:getSectionRows(waiver), id:count, openCloseImageView:nil, openCloseLabel:nil, signedOff:false)
            twoDimensionalArray.append(expandableName)
        }
        
        waiverTableView.rowHeight = UITableViewAutomaticDimension
        waiverTableView.sectionHeaderHeight = UITableViewAutomaticDimension
        
        waiverTableView.estimatedRowHeight = 250
        waiverTableView.estimatedSectionHeaderHeight = GeneralConstants.Waivers.sectionHeaderHeight
        waiverTableView.estimatedSectionFooterHeight = 0
        
        self.waiverTableView.tableFooterView = UIView()
        
        submitButton.backgroundColor = AppColors.WaiverColors.submitButtonInactiveColor
        
        let nib = UINib(nibName: "WaiverTableSectionHeader", bundle: nil)
        waiverTableView.register(nib, forHeaderFooterViewReuseIdentifier: "WaiverTableSectionHeader")

    }
    
    // MARK: - Pick processing
    
    @objc func handleExpandClose(button: UIButton) {
        
        let section = button.tag
        
        if let title = twoDimensionalArray[section].openCloseLabel {
            title.textColor = AppColors.WaiverColors.sectionTitleNormalColor
        }
        
        processExpandClose(section)
        
    }
    
    func processExpandClose(_ section: Int) {
        
        var indexPaths = [IndexPath]()
        for row in 0 ... twoDimensionalArray[section].numberRows-1 {
            let indexPath = IndexPath(row: row, section: section)
            indexPaths.append(indexPath)
        }
        
        let isExpanded = !twoDimensionalArray[section].isExpanded
        twoDimensionalArray[section].isExpanded = isExpanded
        
        if isExpanded {
            waiverTableView.insertRows(at: indexPaths, with: .fade)
        } else {
            waiverTableView.deleteRows(at: indexPaths, with: .fade)
        }
        
        // do this for close and expanded states in case they are open on one and try opening another
        for i in (0...twoDimensionalArray.count - 1) {
            if self.twoDimensionalArray[i].id != section {
                twoDimensionalArray[i].isExpanded = false
                waiverTableView.reloadData()
            }
        }
        
        // this is a safe guard where I have seen the closing of all the sections when hitting the last checkbox, it turns gray and
        // does not collapse but I can't repeat
        waiverTableView.setNeedsLayout()
        
        if let imageView = twoDimensionalArray[section].openCloseImageView {
            
            if self.twoDimensionalArray[section].signedOff {
                self.twoDimensionalArray[section].openCloseImageView?.image = greenCheckBoxCellImage
            }
            else {
                imageView.image = isExpanded ? collapsedCellImage : expandedCellImage
            }
        }
        
    }
    
    func signOffButtonClicked(sender:UIButton) {
        
        let section = sender.tag
        if sender.isSelected {
            self.twoDimensionalArray[section].signedOff = true
            checkForReadyToSubmit()
            self.processExpandClose(section)
        }
        else {
            self.twoDimensionalArray[section].signedOff = false
            waiverTableView.reloadData()
        }
    }
    
    @IBAction func fullNameDidEndEditing(_ textView: UITextView) {
        
        print(textView.text);
        checkForReadyToSubmit()

    }
    
    func completeSignOff(completion: @escaping (_ completion: Bool) -> Void) {
        
        if let nav = self.navigationController {
            
            AppUtils.checkAccess(nav, displayErrors:true, forceLoginOnError: true, displayLoginError:true) {
                (returnCode: AppUtils.CheckAccessReturnValues ) in
                if returnCode == AppUtils.CheckAccessReturnValues.Success {
                    
                   
                    // make waiver api call
                    if let id = self.waiverId, let name = self.fullNameTextView.text {
                        DataAgent.acceptWaiver(false, waiverId:id, signature: name)
                        { (results:JSON) in
                            
                            if results["Error"] == 400 {
                                completion(false)
                                return
                            }
                            else {
                                
                                completion(true)
                                return
                            }
                        }
                    }
                    else {
                        completion(false)
                        return
                    }
                }
                else {
                    completion(false)
                    return
                }
            }
        }
    }
    
    func displayWaiverCompletionMessage() {
        
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        alertView.addButton("Ok") {
                
            // refresh licenses when Ok picked
            if let nav = self.appDelegate.navController {

                self.appDelegate.refreshMemberLicenseData(nav) {
                    (_ returnCode: AppDelegate.RefreshReturnValues) in

                    if returnCode != AppDelegate.RefreshReturnValues.Success {
                        SCLAlertView().showError("Refresh", subTitle: " There was a problem refreshing your license display. You may need to logout and login to see the pending status removed from your license.")
                    }
                    
                    return
                }
            }
        }
        
        // display alert and go to license controller
        alertView.showSuccess("Submitted", subTitle: "Thank you for submitting your membership waiver.")
        _ = self.navigationController?.popViewController(animated: true)
        self.performSegue(withIdentifier: "WaiverToLicenseSegue", sender: nil)
    }
    
    @IBAction func submitButtonPicked(_ sender: Any) {

        self.completeSignOff()
            { (completion:Bool) in

                // succcessful approval
                if completion {
                    self.displayWaiverCompletionMessage()
                }
                // error in approval
                else {
                    SCLAlertView().showError("Waiver Error", subTitle: "Error in approving your waiver information. Please contact support.")
                    _ = self.navigationController?.popViewController(animated: true)
                    self.performSegue(withIdentifier: "WaiverToLicenseSegue", sender: nil)
                }
        }
    }
    
    // MARK: - Display utilities
    
    func checkForReadyToSubmit() {
        
        for i in (0...twoDimensionalArray.count - 1) {
            if !self.twoDimensionalArray[i].signedOff  {
                return
            }
        }
        
        if let text = fullNameTextView.text {
            if text.count == 0 {
                return
            }
            else {
                submitButton.isEnabled = true
                submitButton.backgroundColor = AppColors.WaiverColors.submitButtonActiveColor
            }
        }
        
    }
    
    func getWaiverTitle(_ waiver:Waiver, count:Int) -> String {
        
        var returnString = "WAIVER: " + String(count)
        
        if let content = waiver.content {
            
            if (content.isEmpty) {
                // look in consent
                if let consent = waiver.consent {
                    for waiverTitleStructs in waiverTitles {
                        if consent.uppercased().contains(waiverTitleStructs.searchString.uppercased()) {
                            returnString = waiverTitleStructs.resolvesTo
                            break
                        }
                    }
                }
            }
            else {
                // look in content
                for waiverTitleStructs in waiverTitles {
                    if content.uppercased().contains(waiverTitleStructs.searchString.uppercased()) {
                        returnString = waiverTitleStructs.resolvesTo
                        break
                    }
                }
            }
        }
        
        return returnString
    }
    
    @objc func handleButtonHighlight(button: UIButton) {

        let section = button.tag

        if let title = twoDimensionalArray[section].openCloseLabel {
            title.textColor = AppColors.WaiverColors.sectionTitleHighlightColor
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return waivers.count
    }
    
    func getSectionRows(_ waiver:Waiver) -> Int {
        
        if let content = waiver.content {
            if (content.isEmpty) {
                return 1
            }
            else {
                return 2
            }
        }
        return 1
        
    }
    
    // MARK: - Table view methods
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        // Dequeue with the reuse identifier
        let cell = waiverTableView.dequeueReusableHeaderFooterView(withIdentifier: "WaiverTableSectionHeader")
        let headerView = cell as! WaiverTableSectionHeader
        
        let wavier = waivers[section]
        headerView.openCloseLabel.text = getWaiverTitle(wavier, count:section)
        
        headerView.openCloseButton.addTarget(self, action: #selector(handleButtonHighlight), for: .touchDown)
        headerView.openCloseButton.addTarget(self, action: #selector(handleExpandClose), for: .touchUpInside)
        
        headerView.openCloseButton.tag = section
        headerView.openCloseButton.isUserInteractionEnabled = true
        
        twoDimensionalArray[section].openCloseImageView = headerView.openCloseImageView
        headerView.openCloseLabel.textColor = AppColors.WaiverColors.sectionTitleNormalColor
        twoDimensionalArray[section].openCloseLabel = headerView.openCloseLabel
        
        headerView.openCloseButton.setTitleColor(AppColors.HomeButtonColors.highlightColor, for: .highlighted)
        
        twoDimensionalArray[section].openCloseImageView?.image = expandedCellImage
        
        if let imageView = twoDimensionalArray[section].openCloseImageView {
            
            if self.twoDimensionalArray[section].signedOff {
                self.twoDimensionalArray[section].openCloseImageView?.image = greenCheckBoxCellImage
            }
            else {
                imageView.image = twoDimensionalArray[section].isExpanded ? collapsedCellImage : expandedCellImage
            }
            
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if !twoDimensionalArray[section].isExpanded {
            return 0
        }
        
        return getSectionRows(waivers[section])
        
    }
    
    func configConsentCell(_ consent:String, indexPath: IndexPath, tableView:UITableView, signedOffState:Bool) -> WaiverConsentTableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "WaiverConsentTableViewCell", for: indexPath) as? WaiverConsentTableViewCell else {
            fatalError("The dequeued cell is not an instance of WaiverConsentTableViewCell")
        }
        
        let consentHtmlHeaderString = "<html>" +
            "<head>" +
            "<style>" +
            "body {" +
            "font-family: 'Arial'; ;font-size: 16px;" +
            "text-decoration:none;" +
            "color:white;" +
            "}" +
            "</style>" +
            "</head>"
            
        let consentHtmlString = consentHtmlHeaderString + "<body>" + consent + "</body></head></html>"
            
        let htmlConsentData = NSString(string: consentHtmlString).data(using: String.Encoding.unicode.rawValue)
            
        if let consentHtml = htmlConsentData {
            let consentAttributedString = try! NSAttributedString(data: consentHtml, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
                cell.consentTextView.attributedText = consentAttributedString
                
        }
        
        cell.consentContainerView.layer.cornerRadius = GeneralConstants.Waivers.cornerRadius
        cell.consentButton.isSelected = signedOffState
        cell.consentButton.tag = indexPath.section
        
        cell.consentButton.addTarget(self, action: #selector(signOffButtonClicked), for: .touchUpInside)
        
        return cell
    }
    
    func configContentCell(_ content:String, indexPath: IndexPath, tableView:UITableView) -> WaiverContentTableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "WaiverContentTableViewCell", for: indexPath) as? WaiverContentTableViewCell else {
            fatalError("The dequeued cell is not an instance of WaiverContentTableViewCell")
        }
            
        let htmlHeaderString = "<html>" +
            "<head>" +
            "<style>" +
            "body {" +
            "font-family: 'Arial'; ;font-size: 16px;" +
            "text-decoration:none;" +
            "color:black;" +
            "}" +
            "</style>" +
            "</head>"
            
        let htmlString = htmlHeaderString +
                "<body>" + AppUtils.prepareHtmlWaiver(content) + "</body></head></html>"
            
        let htmlData = NSString(string: htmlString).data(using: String.Encoding.unicode.rawValue)
            
        if let html = htmlData {
            let attributedString = try! NSAttributedString(data: html, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
            cell.contentTextView.attributedText = attributedString
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let wavier = waivers[indexPath.section]
        
        if let content = wavier.content {
            
            // case where row is 1 and content is blank and consent is only row
            if content.isEmpty {
                
                var consent = ""
                if let text = wavier.consent {
                    consent = text
                    
                }
                
                let isSignedOff = twoDimensionalArray[indexPath.section].signedOff
                return configConsentCell(consent, indexPath: indexPath, tableView:tableView, signedOffState:isSignedOff)
            }
            // content is not blank
            else {
                
                let isSignedOff = twoDimensionalArray[indexPath.section].signedOff
                    
                // content row
                if indexPath.row == 0 {
                    return configContentCell(content, indexPath: indexPath, tableView:tableView)
                }
                // second row
                else if indexPath.row == 1 {
                    
                    var consent = ""
                    if let text = wavier.consent {
                        consent = text
                        
                    }
                    return configConsentCell(consent, indexPath: indexPath, tableView:tableView, signedOffState:isSignedOff)
                }
            
                // default if nothing - blank
                return tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            }
        }
        // default if nothing - blank
        return tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
    }

}

