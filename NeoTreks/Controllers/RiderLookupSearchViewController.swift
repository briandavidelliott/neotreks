//
//  RiderLookupSearchViewController.swift
// 
//
//  Created by Brian Elliott on 10/9/17.
//  
// 
//

import UIKit


class RiderLookupSearchViewController: UIViewController, NavBarBackProtocol {

    @IBOutlet weak var searchTermField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var errorMessageLabel: UILabel!
    
    var searchManager:RiderLookupSearchManager?
    var searchTerms:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchManager = RiderLookupSearchManager(self)

        // this is needed to for the nav bar to be displayed even though also set in viewwillappear
        self.navigationController?.isNavigationBarHidden = false
        
        // handle for custom button
        submitButton.setTitleColor(AppColors.HomeButtonColors.highlightColor, for: .highlighted)
        self.errorMessageLabel.isHidden = true
        
        searchTermField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let nav = appDelegate.navController {
            AppUtils.addNavBar("Home", viewController:self, navigationController:nav, delegate:self)
            navigationController?.navigationBar.isTranslucent = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        clearErrorMessage()
    }
    
    func clearErrorMessage() {
        self.errorMessageLabel.isHidden = true
        self.errorMessageLabel.text = ""
    }
    
    func noResultsErrorMessage() {
        self.errorMessageLabel.isHidden = false
        self.errorMessageLabel.text = "No results found!"
    }
    
    @IBAction func submitPicked(_ sender: Any) {
        
        clearErrorMessage()
        
        // Hide the keyboard
        if searchTermField.isEditing {
            searchTermField.endEditing(true)
        }
        
        self.searchTerms = searchTermField.text
        
        // first check if token is active
        if let nav = self.navigationController {
            AppUtils.checkAccess(nav, displayErrors:true, forceLoginOnError:true, displayLoginError:true, accessReturnCode:
                { success in
                    if success == AppUtils.CheckAccessReturnValues.Success {
                
                        if let searchValues = self.searchTerms, let manager = self.searchManager {
                    
                            manager.searchRiders(searchValues, pageNumber:1)
                            { (riderArray: [Member], totalCount:Int) in
                        
                                // Go to results list view if there is results (error alerts will be displayed previously)
                                if riderArray.count > 0 {
                            
                                    let storyboard = UIStoryboard(name: "RiderLookupController", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "RiderLookupResultsListViewController") as! RiderLookupResultsListViewController
                            
                                    vc.riderLookupArray = riderArray
                                    vc.totalRidersFound = totalCount
                                    vc.searchValues = searchValues
                                    vc.searchManager = self.searchManager
                                    nav.pushViewController(vc, animated: true)
                                }
                                else {
                                    self.noResultsErrorMessage()
                                }
                            }
                        }
                        else {
                            self.noResultsErrorMessage()
                        }
                    }

            })
        }
        else {
            SCLAlertView().showError("Error", subTitle: "System error - Nav controller not found!")
        }
    }
    
    func backPicked() {
        _ = navigationController?.popViewController(animated: true)
    }

}
