//
//  BenefitsTableViewCell.swift
//
//  Created by Brian Elliott on 8/12/17.
//

import UIKit

class BenefitsTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var expandIconImageView: UIImageView!

    @IBAction func closeButtonPicked(_ sender: Any) {
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
