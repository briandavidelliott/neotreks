//
//  RiderLookupListItem.swift 
//
//  Created by Brian Elliott on 10/10/17.
//

import Foundation

struct RiderLookupListItem {
    
    var compId:String?
    var firstName:String?
    var lastName:String?
    var dob:Date?
    var state:String?
}
