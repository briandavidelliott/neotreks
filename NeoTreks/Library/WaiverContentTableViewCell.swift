//
//  WaiverTableViewCell.swift
// 
//
//  Created by Brian Elliott on 1/29/18.
//

import UIKit

class WaiverContentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var contentTextView: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
