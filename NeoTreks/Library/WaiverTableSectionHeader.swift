//
//  WaiverTableSectionHeader.swift
//
//  Created by Brian Elliott on 2/7/18.
//

import UIKit

class WaiverTableSectionHeader: UITableViewHeaderFooterView {
    
    @IBOutlet weak var openCloseLabel: UILabel!
    @IBOutlet weak var openCloseImageView: UIImageView!
    @IBOutlet weak var openCloseButton: UIButton!
    
}
