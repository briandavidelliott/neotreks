//
//  EventSearchValues.swift
//
//  Created by Brian Elliott on 9/8/17.
//

import Foundation

struct EventSearchValues {
    
    var locationType:GeneralConstants.EventSearch.LocationType?
    var searchType:GeneralConstants.EventSearch.SearchType?
    var zipCode:String?
    var fromDate:Date?
    var toDate:Date?
    var eventName:String?
    var eventState:String?
    var raceType:GeneralConstants.EventSearch.RaceType?
    var radius:Int?

}
