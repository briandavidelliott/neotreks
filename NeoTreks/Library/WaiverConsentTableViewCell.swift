//
//  WaiverConsentTableViewCell.swift
// 
//
//  Created by Brian Elliott on 2/3/18.
//

import UIKit
import UICheckbox_Swift

class WaiverConsentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var consentTextView: UITextView!
    @IBOutlet weak var consentButton: UICheckbox!
    
    @IBOutlet weak var consentContainerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
