//
//  IconBujtton.swift 
//
//  Created by Brian Elliott on 7/17/17.
//

import UIKit

class IconButton: UIButton {

    var iconImageView:UIImageView?
    var instrinsicSizeRect:CGSize = CGSize.zero
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.isUserInteractionEnabled = true
        self.setTitleColor(AppColors.HomeButtonColors.highlightColor, for: UIControlState.highlighted)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.isUserInteractionEnabled = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setImageSize(size:CGSize) {
        
        if let iconImageView = iconImageView {
            
            self.translatesAutoresizingMaskIntoConstraints = false
            iconImageView.translatesAutoresizingMaskIntoConstraints = false
            
            iconImageView.addConstraint(NSLayoutConstraint(item: iconImageView, attribute: .height, relatedBy: .equal,
                                                      toItem: nil, attribute: .notAnAttribute,
                                                      multiplier: 1.0, constant: size.height))
            iconImageView.addConstraint(NSLayoutConstraint(item: iconImageView, attribute: .width, relatedBy: .equal,
                                                      toItem: nil, attribute: .notAnAttribute,
                                                      multiplier: 1.0, constant: size.width))
        }
    }
    
    func addLeadingImage(_ image:UIImage) {
        
        iconImageView = UIImageView(image: image)
        
        if let view = iconImageView {
            
            addSubview(view)
            
            self.translatesAutoresizingMaskIntoConstraints = false
            view.translatesAutoresizingMaskIntoConstraints = false
            
            self.addConstraint(NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: GeneralConstants.iconButton.iconButtonPadding))
                        
            self.addConstraint(NSLayoutConstraint(item: view,
                                                  attribute: NSLayoutAttribute.centerY,
                                                  relatedBy: NSLayoutRelation.equal,
                                                  toItem: self,
                                                  attribute: NSLayoutAttribute.centerY,
                                                  multiplier: 1.0,
                                                  constant: 0))
        }

    }
    
    func addTrailingImage(_ image:UIImage) {
        
        iconImageView = UIImageView(image: image)
        
        if let view = iconImageView {
            
            addSubview(view)
            
            self.translatesAutoresizingMaskIntoConstraints = false
            view.translatesAutoresizingMaskIntoConstraints = false
            
            self.addConstraint(NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: GeneralConstants.iconButton.iconButtonPadding))
            
            self.addConstraint(NSLayoutConstraint(item: view,
                                                  attribute: NSLayoutAttribute.centerY,
                                                  relatedBy: NSLayoutRelation.equal,
                                                  toItem: self,
                                                  attribute: NSLayoutAttribute.centerY,
                                                  multiplier: 1.0,
                                                  constant: 0))
        }
        
    }
    
    func setIntrinsicSize(nodeSize:CGSize) {
        instrinsicSizeRect = nodeSize
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: instrinsicSizeRect.width, height: instrinsicSizeRect.height)
    }


}
