//
//  Date.swift 
//
//  Created by Brian Elliott on 7/27/17.
//

import Foundation

extension Date {
    
    //An integer representation of age from the date object (read-only).
    var age: Int {
        get {
            let now = Date()
            let calendar = Calendar.current
            
            let ageComponents = calendar.dateComponents([.year], from: self, to: now)
            let age = ageComponents.year!
            return age
        }
    }
    
    // The racing age is the age of the rider at the end of the year
    var racingAge: Int? {
        get {
            if let now = endOfYear() {
                let calendar = Calendar.current
            
                let ageComponents = calendar.dateComponents([.year], from: self, to: now)
                let age = ageComponents.year!
                return age
            }
            else {
                return nil
            }
        }
    }
    
    init(year: Int, month: Int, day: Int) {
        var dc = DateComponents()
        dc.year = year
        dc.month = month
        dc.day = day
        
        var calendar = Calendar(identifier: .gregorian)
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        if let date = calendar.date(from: dc) {
            self.init(timeInterval: 0, since: date)
        } else {
            fatalError("Date component values were invalid.")
        }
    }
    
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
    
    func startOfYear() -> Date? {
        
        let components = Calendar.current.dateComponents([.year], from: Date())
        if let startDateOfYear = Calendar.current.date(from: components) {
            return startDateOfYear
        }
        
        return nil
    }
    
    func endOfYear() -> Date? {
        
        var components = Calendar.current.dateComponents([.year], from: Date())
        if let startDateOfYear = Calendar.current.date(from: components) {
            components.year = 1
            components.day = -1
            return Calendar.current.date(byAdding: components, to: startDateOfYear)
        }
        
        return nil
    }
    
    func aYearFromNow() -> Date? {
        
        return Calendar.current.date(byAdding: .year, value: 1, to: Date())
    }
    
    func yearsFromNow(when:Int) -> Date? {
        
        return Calendar.current.date(byAdding: .year, value: when, to: Date())
    }
    
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: self)!
    }
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: self)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return tomorrow.month != month
    }
    
}
