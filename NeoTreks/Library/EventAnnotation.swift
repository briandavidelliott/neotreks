//
//  EventAnnotation.swift 
//
//  Created by Brian Elliott on 9/5/17.
//

import UIKit
import MapKit

class EventAnnotation: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var eventId: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String, eventId: String) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
        self.eventId = eventId
    }

}
