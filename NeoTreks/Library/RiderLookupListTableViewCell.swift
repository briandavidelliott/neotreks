//
//  RiderLookupListTableViewCell.swift
//
//  Created by Brian Elliott on 10/10/17.
//

import UIKit

class RiderLookupListTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var licenseLabel: UILabel!
    @IBOutlet weak var birthdateLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var moreRidersLabel: UILabel!
    @IBOutlet weak var disclosureIndictorImageView: UIImageView!
    
    @IBOutlet weak var stateLabelTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var stateLabelWidthConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

