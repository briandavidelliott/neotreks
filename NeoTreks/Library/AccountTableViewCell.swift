//
//  AccountTableViewCell.swift
//  Created by Brian Elliott on 7/25/17. 
//

import UIKit

class AccountTableViewCell: UITableViewCell {

    @IBOutlet weak var nameField: UILabel!
    @IBOutlet weak var valueField: UILabel!

}
