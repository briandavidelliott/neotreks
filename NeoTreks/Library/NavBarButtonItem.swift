//
//  NavBarButtonItem.swift
//
//  Created by Brian Elliott on 7/26/17.
//

import Foundation

class BackBarButtonItem: UIBarButtonItem {
    
    var delegate:NavBarBackProtocol?
    
    private var actionHandler: ((Void) -> Void)?
    
    convenience init(title: String?, style: UIBarButtonItemStyle, delegate:NavBarBackProtocol) {
        self.init(title: title, style: style, target: nil, action: #selector(barButtonItemPressed))
        self.target = self
        self.delegate = delegate
    }
    
    func barButtonItemPressed(sender: UIBarButtonItem) {
        delegate?.backPicked()
    }
    
}
