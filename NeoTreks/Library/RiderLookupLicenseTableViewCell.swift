//
//  RiderLookupLicenseTableViewCell.swift
//
//  Created by Brian Elliott on 10/20/17.
//

import UIKit

class RiderLookupLicenseTableViewCell: UITableViewCell {
    
    @IBOutlet weak var licenseTypeTitleLabel: UILabel!
    @IBOutlet weak var licenseTypeStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
